package com.security.audit.shipsafe.sqllite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.security.audit.shipsafe.model.AppLogin;
import com.security.audit.shipsafe.model.CCategoryDetails;
import com.security.audit.shipsafe.model.ChecklistTypeDetails;
import com.security.audit.shipsafe.model.InspectionTypeDTO;
import com.security.audit.shipsafe.model.ObservationDepartmentDTO;
import com.security.audit.shipsafe.model.ObservationInfoDTO;
import com.security.audit.shipsafe.model.ObservationTypeDTO;
import com.security.audit.shipsafe.model.PChecklistCategoryDTO;
import com.security.audit.shipsafe.model.PChecklistQuestion;
import com.security.audit.shipsafe.model.PChecklistTypeDTO;
import com.security.audit.shipsafe.model.PDesignationListDTO;
import com.security.audit.shipsafe.model.PNationalityDTO;
import com.security.audit.shipsafe.model.PShipSideDTO;
import com.security.audit.shipsafe.model.PVesselListDTO;
import com.security.audit.shipsafe.model.PortListDTO;
import com.security.audit.shipsafe.model.PortListDeatils;
import com.security.audit.shipsafe.model.QuestionCheckList;
import com.security.audit.shipsafe.model.QuestionName;
import com.security.audit.shipsafe.model.SaveObservationDTO;
import com.security.audit.shipsafe.model.SaveStaffinfo;
import com.security.audit.shipsafe.model.SaveVessel;
import com.security.audit.shipsafe.model.SaveVesselChecklist;
import com.security.audit.shipsafe.model.ShipSafe;
import com.security.audit.shipsafe.model.ShipSideCheckList;
import com.security.audit.shipsafe.model.ShipsideName;
import com.security.audit.shipsafe.model.UserInfo;
import com.security.audit.shipsafe.model.UserLoginDTO;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelperAdmin extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "shipsafe.db";

    private static final String TABLE_APP_PASSWORD = "app_password";

    private static final String COLUMN_T_ID = "id";
    private static final String COLUMN_STATUS = "status";
    private static final String COLUMN_MESSAGE = "message";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_P_COMPANY_ID = "company_id";

    private String CREATE_APP_PASSWORD = "CREATE TABLE " + TABLE_APP_PASSWORD + "("
            + COLUMN_T_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_STATUS + " TEXT," + COLUMN_MESSAGE + " TEXT," + COLUMN_PASSWORD + " TEXT," + COLUMN_P_COMPANY_ID + " TEXT" + ")";
    // drop table sql query
    private String DROP_APP_PASSWORD = "DROP TABLE IF EXISTS " + TABLE_APP_PASSWORD;


    private static final String TABLE_APP_LOGIN = "app_login";

    private static final String COLUMN_LOGIN_MSG_STATUS = "status";
    private static final String COLUMN_LOGIN_MESSAGE = "message";
    private static final String COLUMN_LOGIN_AUTH_TOKEN = "token";
    private static final String COLUMN_LOGIN_LAST_NAME = "lastname";
    private static final String COLUMN_LOGIN_USER_ID = "userid";
    private static final String COLUMN_LOGIN_EMAIL = "email";
    private static final String COLUMN_LOGIN_FIRST_NAME = "firstname";
    private static final String COLUMN_LOGIN_USER_PASSWORD = "password";
    private static final String COLUMN_LOGIN_COMPANY_ID = "company_id";


    private String CREATE_APP_LOGIN = "CREATE TABLE " + TABLE_APP_LOGIN + "("
            + COLUMN_LOGIN_MSG_STATUS + " TEXT," + COLUMN_LOGIN_MESSAGE + " TEXT," + COLUMN_LOGIN_AUTH_TOKEN + " TEXT," + COLUMN_LOGIN_LAST_NAME + " TEXT," + COLUMN_LOGIN_USER_ID + " TEXT," + COLUMN_LOGIN_EMAIL + " TEXT," + COLUMN_LOGIN_USER_PASSWORD + " TEXT," + COLUMN_LOGIN_FIRST_NAME + " TEXT," + COLUMN_LOGIN_COMPANY_ID + " TEXT" + ")";
    // drop table sql query
    private String DROP_APP_LOGIN = "DROP TABLE IF EXISTS " + TABLE_APP_LOGIN;


    private static final String TABLE_CHECKLIST_TYPE = "checklist_type";

    private static final String COLUMN_CHECKLIST_ID = "id";
    private static final String COLUMN_IS_INTERNAL = "is_internal";
    private static final String COLUMN_CHECKLIST_NAME = "checklist_name";


    private String CREATE_CHECKLIST_TYPE = "CREATE TABLE " + TABLE_CHECKLIST_TYPE + "("
            + COLUMN_CHECKLIST_ID + " TEXT," + COLUMN_IS_INTERNAL + " TEXT," + COLUMN_CHECKLIST_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_CHECKLIST_TYPE = "DROP TABLE IF EXISTS " + TABLE_CHECKLIST_TYPE;


    private static final String TABLE_VESSEL_LIST = "vessel_list";

    private static final String COLUMN_VESSEL_USER_NAME = "user_name";
    private static final String COLUMN_VESSEL_EMAIL = "email";
    private static final String COLUMN_VESSEL_VMID = "vmid";
    private static final String COLUMN_VESSEL_ID = "vessel_id";
    private static final String COLUMN_VESSEL_NAME = "vessel_name";


    private String CREATE_VESSEL_LIST = "CREATE TABLE " + TABLE_VESSEL_LIST + "("
            + COLUMN_VESSEL_USER_NAME + " TEXT," + COLUMN_VESSEL_EMAIL + " TEXT," + COLUMN_VESSEL_VMID + " TEXT," + COLUMN_VESSEL_ID + " TEXT," + COLUMN_VESSEL_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_VESSEL_LIST = "DROP TABLE IF EXISTS " + TABLE_VESSEL_LIST;


    private static final String TABLE_PORT_LIST = "port_list";

    private static final String COLUMN_PORT_NAME = "port_name";
    private static final String COLUMN_COUNTRY_NAME = "country_name";
    private static final String COLUMN_COUNTRY_ID = "country_id";
    private static final String COLUMN_COUNTRY_WITH_PORT = "country_port";
    private static final String COLUMN_PORT_CODE = "port_code";
    private static final String COLUMN_PORT_ID = "port_id";
    private static final String COLUMN_LOCATION_CODE = "location_code";


    private String CREATE_PORT_LIST = "CREATE TABLE " + TABLE_PORT_LIST + "("
            + COLUMN_PORT_NAME + " TEXT," + COLUMN_COUNTRY_NAME + " TEXT," + COLUMN_COUNTRY_ID + " TEXT," + COLUMN_COUNTRY_WITH_PORT + " TEXT," + COLUMN_PORT_CODE + " TEXT," + COLUMN_PORT_ID + " TEXT," + COLUMN_LOCATION_CODE + " TEXT" + ")";
    // drop table sql query
    private String DROP_PORT_LIST = "DROP TABLE IF EXISTS " + TABLE_PORT_LIST;


    private static final String TABLE_DESIGNATION = "designation";

    private static final String COLUMN_DESIGNATION_ID = "id";
    private static final String COLUMN_DESIGNATION_NAME = "rank_name";


    private String CREATE_DESIGNATION = "CREATE TABLE " + TABLE_DESIGNATION + "("
            + COLUMN_DESIGNATION_ID + " TEXT," + COLUMN_DESIGNATION_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_DESIGNATION = "DROP TABLE IF EXISTS " + TABLE_DESIGNATION;


    private static final String TABLE_NATIONALITY = "nationality";

    private static final String COLUMN_NATIONALITY_ID = "id";
    private static final String COLUMN_NATIONALITY_NAME = "nationality_name";

    private String CREATE_NATIONALITY = "CREATE TABLE " + TABLE_NATIONALITY + "("
            + COLUMN_NATIONALITY_ID + " TEXT," + COLUMN_NATIONALITY_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_NATIONALITY = "DROP TABLE IF EXISTS " + TABLE_NATIONALITY;


    private static final String TABLE_P_CHECKLIST_CATEGORY = "checklist_category";


    private static final String COLUMN_CATEGORY_TYPE_ID = "type_id";
    private static final String COLUMN_CATEGORY_TYPE_NAME = "type_name";
    private static final String COLUMN_CATEGORY_ORDER = "c_order";
    private static final String COLUMN_CATEGORY_ID = "category_id";
    private static final String COLUMN_CATEGORY_NAME = "category_name";


    private String CREATE_P_CHECKLIST_CATEGORY = "CREATE TABLE " + TABLE_P_CHECKLIST_CATEGORY + "("
            + COLUMN_CATEGORY_TYPE_ID + " TEXT," + COLUMN_CATEGORY_TYPE_NAME + " TEXT," + COLUMN_CATEGORY_ORDER + " TEXT," + COLUMN_CATEGORY_ID + " TEXT," + COLUMN_CATEGORY_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_P_CHECKLIST_CATEGORY = "DROP TABLE IF EXISTS " + TABLE_P_CHECKLIST_CATEGORY;


    private static final String TABLE_CHECKLIST_QUESTION = "Question";

    private static final String COLUMN_QUESTION_INFO = "info";
    private static final String COLUMN_QUESTION_CATEGORY_TYPE_ID = "category_type_id";
    private static final String COLUMN_QUESTION_CATEGORY_TYPE_NAME = "category_type_name";
    private static final String COLUMN__QUESTION = "question";
    private static final String COLUMN__QUESTION_CATEGORY_ID = "category_id";
    private static final String COLUMN_QUESTION_ORDER = "order_type";
    private static final String COLUMN__QUESTION_CATEGORY_NAME = "category_name";
    private static final String COLUMN_QUESTION_ID = "id";


    private String CREATE_CHECKLIST_QUESTION = "CREATE TABLE " + TABLE_CHECKLIST_QUESTION + "("
            + COLUMN_QUESTION_INFO + " TEXT," + COLUMN_QUESTION_CATEGORY_TYPE_ID
            + " TEXT," + COLUMN_QUESTION_CATEGORY_TYPE_NAME + " TEXT," + COLUMN__QUESTION + " TEXT," + COLUMN__QUESTION_CATEGORY_ID + " TEXT," + COLUMN_QUESTION_ORDER + " TEXT,"
            + COLUMN__QUESTION_CATEGORY_NAME + " TEXT," + COLUMN_QUESTION_ID + " TEXT" + ")";
    // drop table sql query
    private String DROP_CHECKLIST_QUESTION = "DROP TABLE IF EXISTS " + TABLE_CHECKLIST_QUESTION;


    private static final String COLUMN_QUESTION_INFO_CHECK_LIST = "info_check_list";
    private static final String COLUMN_QUESTION_CHECK_LIST_ID = "id";
    private static final String COLUMN_QUESTION_CHECK_LIST_UUID = "checklist_uuid";
    private static final String COLUMN_QUESTION_CHECK_LIST_QUESTION_ID = "question_id";
    private static final String COLUMN_QUESTION_CHECK_LIST_REMARK = "remark";
    private static final String COLUMN_QUESTION_CHECK_LIST_SELECT = "question_select";
    private static final String COLUMN_QUESTION_CHECK_LIST_DATE = "date";


    private String CREATE_QUESTION_INFO_CHECK_LIST = "CREATE TABLE " + COLUMN_QUESTION_INFO_CHECK_LIST + "("
            + COLUMN_QUESTION_CHECK_LIST_ID + " TEXT," + COLUMN_QUESTION_CHECK_LIST_UUID + " TEXT,"
            + COLUMN_QUESTION_CHECK_LIST_QUESTION_ID + " TEXT,"
            + COLUMN_QUESTION_CHECK_LIST_REMARK + " TEXT," + COLUMN_QUESTION_CHECK_LIST_SELECT + " TEXT,"
            + COLUMN_QUESTION_CHECK_LIST_DATE + " TEXT" + ")";

    // drop table sql query
    private String DROP_QUESTION_INFO_CHECK_LIST = "DROP TABLE IF EXISTS " + COLUMN_QUESTION_INFO_CHECK_LIST;

    private static final String TABLE_SHIPSAFE = "ShipSafe";

    private static final String COLUMN_SHIPSAFE_CHECKLIST_TYPE_ID = "checklist_type_id";
    private static final String COLUMN_SHIPSAFE_VASSEL_ID = "vassel_id";
    private static final String COLUMN_SHIPSAFE_REMARK = "remark";
    private static final String COLUMN_SHIPSAFE_SIGNATURES = "signatures";
    private static final String COLUMN_SHIPSAFE_INSPECTOR = "inspector";

    private String CREATE_SHIPSAFE = "CREATE TABLE " + TABLE_SHIPSAFE + "("
            + COLUMN_SHIPSAFE_CHECKLIST_TYPE_ID + " TEXT," + COLUMN_SHIPSAFE_VASSEL_ID
            + " TEXT," + COLUMN_SHIPSAFE_REMARK + " TEXT," + COLUMN_SHIPSAFE_SIGNATURES + " TEXT,"
            + COLUMN_SHIPSAFE_INSPECTOR + " TEXT" + ")";
    // drop table sql query
    private String DROP_SHIPSAFE = "DROP TABLE IF EXISTS " + TABLE_SHIPSAFE;


    private static final String TABLE_SHIPSIDE = "ShipSide";

    private static final String COLUMN_SHIPSIDE_CHECKLIST_TYPE_ID = "checklist_type_id";
    private static final String COLUMN_SHIPSIDE_ORDER = "shipside_order";
    private static final String COLUMN_SHIPSIDE_CHECKLIST_TYPE_NAME = "checklist_name";
    private static final String COLUMN_SHIPSIDE_ID = "shipside_id";
    private static final String COLUMN_SHIPSIDE_NAME = "shipside_name";


    private String CREATE_SHIPSIDE = "CREATE TABLE " + TABLE_SHIPSIDE + "("
            + COLUMN_SHIPSIDE_CHECKLIST_TYPE_ID + " TEXT," + COLUMN_SHIPSIDE_ORDER
            + " TEXT," + COLUMN_SHIPSIDE_CHECKLIST_TYPE_NAME + " TEXT," + COLUMN_SHIPSIDE_ID + " TEXT,"
            + COLUMN_SHIPSIDE_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_SHIPSIDE = "DROP TABLE IF EXISTS " + TABLE_SHIPSIDE;


    private static final String TABLE_SHIPSIDE_CHECK_LIST = "ShipSideChackList";
    private static final String TABLE_SHIPSIDE_CHECK_LIST_ID = "id";
    private static final String COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID = "shipside_id";
    private static final String COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID = "checklist_id";
    private static final String COLUMN_SHIPSIDE_CHECK_LIST_REMARKS = "remarks";

    private String CREATE_SHIPSIDE_CHECK_LIST = "CREATE TABLE "
            + TABLE_SHIPSIDE_CHECK_LIST + "(" + TABLE_SHIPSIDE_CHECK_LIST_ID + " TEXT,"
            + COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID + " TEXT," + COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID + " TEXT," + COLUMN_SHIPSIDE_CHECK_LIST_REMARKS
            + " TEXT" + ")";
    // drop table sql query
    private String DROP_SHIPSIDE_CHECK_LIST = "DROP TABLE IF EXISTS " + TABLE_SHIPSIDE_CHECK_LIST;

    private static final String TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE = "photo_report_check_list_image";

    private static final String COLUMN_PHOTO_REPORT_CHECK_LIST_ID = "id";
    private static final String COLUMN_PHOTO_REPORT_CHECK_LIST_UUID = "checklist_uuid";
    private static final String COLUMN_PHOTO_REPORT_CHECK_LIST_IMAGE = "file";


    private String CREATE_PHOTO_REPORT_CHECK_LIST_IMAGE = "CREATE TABLE " + TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE + "(" + COLUMN_PHOTO_REPORT_CHECK_LIST_ID + " TEXT," + COLUMN_PHOTO_REPORT_CHECK_LIST_IMAGE + " TEXT,"
            + COLUMN_PHOTO_REPORT_CHECK_LIST_UUID + " TEXT" +
            ")";

    // drop table sql query
    private String DROP_SHIPSIDE_CHECK_LIST_IMAGE = "DROP TABLE IF EXISTS " + TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE;

    private static final String TABLE_INSPECTION_TYPE = "InsType";


    private static final String COLUMN_INSPECTION_ID = "inspection_id";
    private static final String COLUMN_INSPECTION_NAME = "inspection_name";


    private String CREATE_INSPECTION_TYPE = "CREATE TABLE " + TABLE_INSPECTION_TYPE + "(" + COLUMN_INSPECTION_ID + " TEXT," + COLUMN_INSPECTION_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_INSPECTION_TYPE = "DROP TABLE IF EXISTS " + TABLE_INSPECTION_TYPE;


    private static final String TABLE_OBSERVATION_TYPE = "ObsType";

    private static final String COLUMN_OBSERVATION_ID = "observation_id";
    private static final String COLUMN_OBSERVATION_NAME = "observation_name";


    private String CREATE_OBSERVATION_TYPE = "CREATE TABLE " + TABLE_OBSERVATION_TYPE + "(" + COLUMN_OBSERVATION_ID + " TEXT," + COLUMN_OBSERVATION_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_OBSERVATION_TYPE = "DROP TABLE IF EXISTS " + TABLE_OBSERVATION_TYPE;


    private static final String TABLE_SAVE_VESSEL_INFO = "SaveVesselInfo";

    private static final String COLUMN_SAVE_VESSEL_ID = "vessel_id";
    private static final String COLUMN_SAVE_VESSEL_START_DATE = "startdate";
    private static final String COLUMN_SAVE_VESSEL_END_DATE = "enddate";
    private static final String COLUMN_SAVE_VESSEL_PORT_FROM = "port_from";
    private static final String COLUMN_SAVE_VESSEL_PORT_TO = "port_to";
    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_UUID = "checklist_uuid";
    private static final String COLUMN_SAVE__VESSEL_CHECKLIST_TYPE = "checklist_type";
    private static final String COLUMN_SAVE_VESSEL_STATUS = "status";

    private String CREATE_SAVE_VESSEL_INFO = "CREATE TABLE " + TABLE_SAVE_VESSEL_INFO + "("
            + COLUMN_SAVE_VESSEL_ID + " TEXT," + COLUMN_SAVE_VESSEL_START_DATE
            + " TEXT," + COLUMN_SAVE_VESSEL_END_DATE + " TEXT," + COLUMN_SAVE_VESSEL_PORT_FROM + " TEXT," + COLUMN_SAVE_VESSEL_PORT_TO + " TEXT," + COLUMN_SAVE_VESSEL_CHECKLIST_UUID + " TEXT,"
            + COLUMN_SAVE__VESSEL_CHECKLIST_TYPE + " TEXT," + COLUMN_SAVE_VESSEL_STATUS + " TEXT" + ")";
    // drop table sql query
    private String DROP_SAVE_VESSEL_INFO = "DROP TABLE IF EXISTS " + TABLE_SAVE_VESSEL_INFO;


    private static final String TABLE_SAVE_STAFF_INFORMATION = "SaveStaffInfo";

    private static final String COLUMN_SAVE_STAFF_RANK = "rank";
    private static final String COLUMN_SAVE_STAFF_NAME = "staff_name";
    private static final String COLUMN_SAVE_YEAR_IN_COMPANY = "year_in_company";
    private static final String COLUMN_SAVE_MONTH_IN_COMPANY = "month_in_company";
    private static final String COLUMN_SAVE_STAFF_NATIONALITY = "nationality";
    private static final String COLUMN_SAVE_STAFF_CHECKLIST_UUID = "checklist_uuid";

    private String CREATE_SAVE_STAFF_INFO = "CREATE TABLE " + TABLE_SAVE_STAFF_INFORMATION + "("
            + COLUMN_SAVE_STAFF_RANK + " TEXT," + COLUMN_SAVE_STAFF_NAME
            + " TEXT," + COLUMN_SAVE_YEAR_IN_COMPANY + " TEXT," + COLUMN_SAVE_MONTH_IN_COMPANY + " TEXT," + COLUMN_SAVE_STAFF_NATIONALITY + " TEXT,"
            + COLUMN_SAVE_STAFF_CHECKLIST_UUID + " TEXT" + ")";
    // drop table sql query
    private String DROP_SAVE_STAFF_INFO = "DROP TABLE IF EXISTS " + TABLE_SAVE_STAFF_INFORMATION;


    private static final String TABLE_OBSERVATION_DEPARTMENT = "Observation_Department";

    private static final String COLUMN_OD_CHECKLIST_TYPE_ID = "type_id";
    private static final String COLUMN_OD_CHECKLIST_TYPE_NAME = "type_name";
    private static final String COLUMN_OD_ID = "od_id";
    private static final String COLUMN_OD_NAME = "od_name";


    private String CREATE_OBSERVATION_DEPARTMENT = "CREATE TABLE " + TABLE_OBSERVATION_DEPARTMENT + "(" + COLUMN_OD_CHECKLIST_TYPE_ID + " TEXT," + COLUMN_OD_CHECKLIST_TYPE_NAME + " TEXT,"
            + COLUMN_OD_ID + " TEXT," + COLUMN_OD_NAME + " TEXT" + ")";
    // drop table sql query
    private String DROP_OBSERVATION_DEPARTMENT = "DROP TABLE IF EXISTS " + TABLE_OBSERVATION_DEPARTMENT;


    private static final String TABLE_QUESTION_IMAGE = "image";

    private static final String COLUMN_IMGE_QUESTION_ID = "id";
    private static final String COLUMN_IMAGE = "file";


    private String CREATE_QUESTION_IMAGE = "CREATE TABLE " + TABLE_QUESTION_IMAGE + "(" + COLUMN_IMGE_QUESTION_ID + " TEXT," + COLUMN_IMAGE + " TEXT" + ")";
    // drop table sql query
    private String DROP_QUESTION_IMAGE = "DROP TABLE IF EXISTS " + TABLE_QUESTION_IMAGE;


    private static final String TABLE_SAVE_OBSERVATION = "Observation";
    private static final String COLUMN_SAVE_OBSERVATION_ID = "id";
    private static final String COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID = "departament_id";
    private static final String COLUMN_SAVE_OBSERVATION_OBSERVATION = "observation";
    private static final String COLUMN_SAVE_OBSERVATION_TARGET_DATE = "targetdate";
    private static final String COLUMN_SAVE_OBSERVATION_CLOSE_DATE = "closedate";
    private static final String COLUMN_SAVE_OBSERVATION_CHECKLISTUUID = "checklistUUID";
    private static final String COLUMN_SAVE_OBSERVATION_TYPE = "observation_type";
    private static final String COLUMN_SAVE_INSPECTION_TYPE = "inspection_type";
    private static final String COLUMN_SAVE_OBSERVATION_REMARK = "remark";


    private String CREATE_SAVE_OBSERVATION = "CREATE TABLE " + TABLE_SAVE_OBSERVATION + "(" + COLUMN_SAVE_OBSERVATION_ID + " TEXT," + COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID + " TEXT," + COLUMN_SAVE_OBSERVATION_OBSERVATION + " TEXT," + COLUMN_SAVE_OBSERVATION_TARGET_DATE + " TEXT," + COLUMN_SAVE_OBSERVATION_CLOSE_DATE + " TEXT," + COLUMN_SAVE_OBSERVATION_CHECKLISTUUID + " TEXT," + COLUMN_SAVE_OBSERVATION_TYPE + " TEXT," + COLUMN_SAVE_INSPECTION_TYPE + " TEXT," + COLUMN_SAVE_OBSERVATION_REMARK + " TEXT" + ")";
    // drop table sql query
    private String DROP_SAVE_OBSERVATION = "DROP TABLE IF EXISTS " + TABLE_SAVE_OBSERVATION;


    private static final String TABLE_SAVE_OBSERVATION_INFO = "Observation_info";

    private static final String COLUMN_SAVE_OBSERVATION_INFO_VESSEL_ID = "id";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_PORT_PLACE = "port_place";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_REPORT_DATE = "report_date";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_REPORT_TIME = "report_time";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_DATE = "auditor_date";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_TIME = "auditor_time";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_OPENING_HELD_AT = "held_at";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_OPENING_ATTENDED_BY = "attended_by";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_CLOSING_HELD_AT = "closing_held_at";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_CLOSING_ATTENDED_BY = "closing_attended_by";
    private static final String COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID = "checklist_UUID";


    private String CREATE_SAVE_OBSERVATION_INFO = "CREATE TABLE " + TABLE_SAVE_OBSERVATION_INFO + "(" + COLUMN_SAVE_OBSERVATION_INFO_VESSEL_ID + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_PORT_PLACE + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_REPORT_DATE + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_REPORT_TIME + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_DATE + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_TIME + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_OPENING_HELD_AT + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_OPENING_ATTENDED_BY + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_CLOSING_HELD_AT + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_CLOSING_ATTENDED_BY + " TEXT,"
            + COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID + " TEXT" + ")";
    // drop table sql query
    private String DROP_SAVE_OBSERVATION_INFO = "DROP TABLE IF EXISTS " + TABLE_SAVE_OBSERVATION_INFO;


    private static final String TABLE_SAVE_VESSEL_CHECKLIST = "save_vessel_checklist";

    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_QUESTION_ID = "quetion_id";
    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_DATE = "date";
    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_RESPONSE = "response";
    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID = "checklistUUID";
    private static final String COLUMN_SAVE_VESSEL_CHECKLIST_REMARK = "remark";


    private String CREATE_SAVE_VESSEL_CHECKLIST = "CREATE TABLE " + TABLE_SAVE_VESSEL_CHECKLIST + "(" + COLUMN_SAVE_VESSEL_CHECKLIST_QUESTION_ID + " TEXT," + COLUMN_SAVE_VESSEL_CHECKLIST_DATE + " TEXT," + COLUMN_SAVE_VESSEL_CHECKLIST_RESPONSE + " TEXT," + COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID + " TEXT," + COLUMN_SAVE_VESSEL_CHECKLIST_REMARK + " TEXT" + ")";
    // drop table sql query
    private String DROP_SAVE_VESSEL_CHECKLIST = "DROP TABLE IF EXISTS " + TABLE_SAVE_VESSEL_CHECKLIST;


    public DatabaseHelperAdmin(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_APP_PASSWORD);
        db.execSQL(CREATE_APP_LOGIN);
        db.execSQL(CREATE_CHECKLIST_TYPE);
        db.execSQL(CREATE_VESSEL_LIST);
        db.execSQL(CREATE_PORT_LIST);
        db.execSQL(CREATE_NATIONALITY);
        db.execSQL(CREATE_DESIGNATION);
        db.execSQL(CREATE_P_CHECKLIST_CATEGORY);
        db.execSQL(CREATE_CHECKLIST_QUESTION);
        db.execSQL(CREATE_SHIPSAFE);
        db.execSQL(CREATE_SHIPSIDE);
        db.execSQL(CREATE_SHIPSIDE_CHECK_LIST);
        db.execSQL(CREATE_INSPECTION_TYPE);
        db.execSQL(CREATE_OBSERVATION_TYPE);
        db.execSQL(CREATE_SAVE_VESSEL_INFO);
        db.execSQL(CREATE_SAVE_STAFF_INFO);
        db.execSQL(CREATE_OBSERVATION_DEPARTMENT);
        db.execSQL(CREATE_QUESTION_IMAGE);
        db.execSQL(CREATE_PHOTO_REPORT_CHECK_LIST_IMAGE);
        db.execSQL(CREATE_SAVE_OBSERVATION);
        db.execSQL(CREATE_SAVE_OBSERVATION_INFO);
        db.execSQL(CREATE_SAVE_VESSEL_CHECKLIST);
        db.execSQL(CREATE_QUESTION_INFO_CHECK_LIST);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_APP_PASSWORD);
        db.execSQL(DROP_APP_LOGIN);
        db.execSQL(DROP_CHECKLIST_TYPE);
        db.execSQL(DROP_VESSEL_LIST);
        db.execSQL(DROP_PORT_LIST);
        db.execSQL(DROP_NATIONALITY);
        db.execSQL(DROP_DESIGNATION);
        db.execSQL(DROP_P_CHECKLIST_CATEGORY);
        db.execSQL(DROP_CHECKLIST_QUESTION);
        db.execSQL(DROP_SHIPSAFE);
        db.execSQL(DROP_SHIPSIDE);
        db.execSQL(DROP_SHIPSIDE_CHECK_LIST);
        db.execSQL(DROP_INSPECTION_TYPE);
        db.execSQL(DROP_OBSERVATION_TYPE);
        db.execSQL(DROP_SAVE_VESSEL_INFO);
        db.execSQL(DROP_SAVE_STAFF_INFO);
        db.execSQL(DROP_OBSERVATION_DEPARTMENT);
        db.execSQL(DROP_QUESTION_IMAGE);
        db.execSQL(DROP_INSPECTION_TYPE);
        db.execSQL(DROP_SAVE_OBSERVATION);
        db.execSQL(DROP_SAVE_OBSERVATION_INFO);
        db.execSQL(DROP_SAVE_VESSEL_CHECKLIST);
        db.execSQL(DROP_QUESTION_INFO_CHECK_LIST);
        // Create tables again
        onCreate(db);

    }


    public void add_app_user(List<UserLoginDTO> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (UserLoginDTO item : list) {
                values.put(COLUMN_LOGIN_MSG_STATUS, item.getStatus());
                values.put(COLUMN_LOGIN_MESSAGE, item.getMessage());
                values.put(COLUMN_LOGIN_AUTH_TOKEN, item.getData().getAuthToken());
                values.put(COLUMN_LOGIN_LAST_NAME, item.getData().getLastName());
                values.put(COLUMN_LOGIN_USER_ID, item.getData().getUserId());
                values.put(COLUMN_LOGIN_EMAIL, item.getData().getEmail());
                values.put(COLUMN_LOGIN_FIRST_NAME, item.getData().getFirstName());
                values.put(COLUMN_LOGIN_FIRST_NAME, item.getData().getFirstName());
                values.put(COLUMN_LOGIN_USER_PASSWORD, item.getPassword());
                values.put(COLUMN_LOGIN_COMPANY_ID, item.getData().getCompanyId());
                db.insert(TABLE_APP_LOGIN, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void add_Observation_Department(List<ObservationDepartmentDTO.OData.ObservationDepartment> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (ObservationDepartmentDTO.OData.ObservationDepartment item : list) {
                values.put(COLUMN_OD_CHECKLIST_TYPE_ID, item.getChecklist_type__id());
                values.put(COLUMN_OD_CHECKLIST_TYPE_NAME, item.getChecklist_type__name());
                values.put(COLUMN_OD_ID, item.getId());
                values.put(COLUMN_OD_NAME, item.getName());
                db.insert(TABLE_OBSERVATION_DEPARTMENT, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void addShipSideCheckList(List<ShipSideCheckList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (ShipSideCheckList item : list) {
                values.put(TABLE_SHIPSIDE_CHECK_LIST_ID, item.getId());
                values.put(COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID, item.getShipsideId());
                values.put(COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID, item.getCheckListId());
                values.put(COLUMN_SHIPSIDE_CHECK_LIST_REMARKS, item.getRemarks());
                db.insert(TABLE_SHIPSIDE_CHECK_LIST, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void addPhotoReportCheckListImage(String id, String uuid, Uri image) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_PHOTO_REPORT_CHECK_LIST_ID, id);
            values.put(COLUMN_PHOTO_REPORT_CHECK_LIST_UUID, uuid);
            values.put(COLUMN_PHOTO_REPORT_CHECK_LIST_IMAGE, image.toString());
            db.insert(TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void removePhotoReportCheckListImage(String id, String uuid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE,
                    COLUMN_PHOTO_REPORT_CHECK_LIST_ID + " = ? AND " + COLUMN_PHOTO_REPORT_CHECK_LIST_UUID + " = ? ",
                    new String[]{id, uuid});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void add_app_password(List<AppLogin> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (AppLogin item : list) {
                values.put(COLUMN_STATUS, item.getStatus());
                values.put(COLUMN_MESSAGE, item.getMessage());
                values.put(COLUMN_P_COMPANY_ID, item.getData().getCompanyId());
                values.put(COLUMN_PASSWORD, item.getData().getPassword());

                db.insert(TABLE_APP_PASSWORD, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void addShipSafe(ShipSafe shipSafe) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_SHIPSAFE_CHECKLIST_TYPE_ID, shipSafe.getChecklistUUID());
            values.put(COLUMN_SHIPSAFE_VASSEL_ID, shipSafe.getVasselId());
            values.put(COLUMN_SHIPSAFE_REMARK, shipSafe.getRemark());
            values.put(COLUMN_SHIPSAFE_SIGNATURES, shipSafe.getSignatures());
            values.put(COLUMN_SHIPSAFE_INSPECTOR, shipSafe.getInspector());
            db.insert(TABLE_SHIPSAFE, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void add_shipside(List<PShipSideDTO.SSide.ShipSide> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PShipSideDTO.SSide.ShipSide item : list) {
                values.put(COLUMN_SHIPSIDE_CHECKLIST_TYPE_ID, item.getChecklist_type__id());
                values.put(COLUMN_SHIPSIDE_ORDER, item.getOrder());
                values.put(COLUMN_SHIPSIDE_CHECKLIST_TYPE_NAME, item.getChecklist_type__name());
                values.put(COLUMN_SHIPSIDE_ID, item.getId());
                values.put(COLUMN_SHIPSIDE_NAME, item.getName());
                db.insert(TABLE_SHIPSIDE, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void add_Inspection_Type(List<InspectionTypeDTO.InsData.InsTypeList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (InspectionTypeDTO.InsData.InsTypeList item : list) {
                values.put(COLUMN_INSPECTION_ID, item.getId());
                values.put(COLUMN_INSPECTION_NAME, item.getName());
                db.insert(TABLE_INSPECTION_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void add_Observation_Type(List<ObservationTypeDTO.ObsData.ObsTypeList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (ObservationTypeDTO.ObsData.ObsTypeList item : list) {
                values.put(COLUMN_OBSERVATION_ID, item.getId());
                values.put(COLUMN_OBSERVATION_NAME, item.getName());
                db.insert(TABLE_OBSERVATION_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void addSaveObservationInfo(ObservationInfoDTO info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SAVE_OBSERVATION_INFO_VESSEL_ID, info.getVesselId());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_PORT_PLACE, info.getPortPlace());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_REPORT_DATE, info.getReportDate());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_REPORT_TIME, info.getReportTime());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_DATE, info.getAuditorDate());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_TIME, info.getAuditorTime());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_OPENING_HELD_AT, info.getOpeningHeldAt());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_OPENING_ATTENDED_BY, info.getOpeningAttendedBy());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_CLOSING_HELD_AT, info.getClosingHeldAt());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_CLOSING_ATTENDED_BY, info.getClosingAttendedBy());
        values.put(COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID, info.getChecklistUUID());

        db.insert(TABLE_SAVE_OBSERVATION_INFO, null, values);

        db.close();
    }


    public void addSaveVesselChecklist(SaveVesselChecklist info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_QUESTION_ID, info.getQuestionId());
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_DATE, info.getDate());
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_RESPONSE, info.getResponse());
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID, info.getChecklistUUID());
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_REMARK, info.getRemark());

        db.insert(TABLE_SAVE_VESSEL_CHECKLIST, null, values);

        db.close();
    }

    public void addQuestionCheckList(QuestionCheckList questionCheckList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_QUESTION_CHECK_LIST_ID, questionCheckList.getId());
        values.put(COLUMN_QUESTION_CHECK_LIST_QUESTION_ID, questionCheckList.getQuestionId());
        values.put(COLUMN_QUESTION_CHECK_LIST_UUID, questionCheckList.getChecklistUUID());
        values.put(COLUMN_QUESTION_CHECK_LIST_REMARK, questionCheckList.getRemark());
        values.put(COLUMN_QUESTION_CHECK_LIST_SELECT, questionCheckList.getSelect());
        values.put(COLUMN_QUESTION_CHECK_LIST_DATE, questionCheckList.getDate());

        db.insert(COLUMN_QUESTION_INFO_CHECK_LIST, null, values);

        db.close();
    }

    public void addSaveObservation(SaveObservationDTO info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SAVE_OBSERVATION_ID, info.getId());
        values.put(COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID, info.getDepartmentId());
        values.put(COLUMN_SAVE_OBSERVATION_OBSERVATION, info.getObservation());
        values.put(COLUMN_SAVE_OBSERVATION_TARGET_DATE, info.getTargetDate());
        values.put(COLUMN_SAVE_OBSERVATION_CLOSE_DATE, info.getClosedDate());
        values.put(COLUMN_SAVE_OBSERVATION_CHECKLISTUUID, info.getChecklistUUID());
        values.put(COLUMN_SAVE_OBSERVATION_TYPE, info.getObservationType());
        values.put(COLUMN_SAVE_INSPECTION_TYPE, info.getInspectionType());
        values.put(COLUMN_SAVE_OBSERVATION_REMARK, info.getRemark());

        db.insert(TABLE_SAVE_OBSERVATION, null, values);

        db.close();
    }


    public void addSaveVesselInfo(SaveVessel info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SAVE_VESSEL_ID, info.getVesselId());
        values.put(COLUMN_SAVE_VESSEL_START_DATE, info.getStartDate());
        values.put(COLUMN_SAVE_VESSEL_END_DATE, info.getEndDate());
        values.put(COLUMN_SAVE_VESSEL_PORT_FROM, info.getPortFrom());
        values.put(COLUMN_SAVE_VESSEL_PORT_TO, info.getPortTo());
        values.put(COLUMN_SAVE_VESSEL_CHECKLIST_UUID, info.getChecklistUUID());
        values.put(COLUMN_SAVE__VESSEL_CHECKLIST_TYPE, info.getChecklistType());
        values.put(COLUMN_SAVE_VESSEL_STATUS, info.getStatus());

        db.insert(TABLE_SAVE_VESSEL_INFO, null, values);

        db.close();
    }


    public void addSaveStaffInfo(SaveStaffinfo info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SAVE_STAFF_RANK, info.getRank());
        values.put(COLUMN_SAVE_STAFF_NAME, info.getName());
        values.put(COLUMN_SAVE_YEAR_IN_COMPANY, info.getYearInCompany());
        values.put(COLUMN_SAVE_MONTH_IN_COMPANY, info.getMonthInCompany());
        values.put(COLUMN_SAVE_STAFF_NATIONALITY, info.getNationality());
        values.put(COLUMN_SAVE_STAFF_CHECKLIST_UUID, info.getChecklistUUID());
        db.insert(TABLE_SAVE_STAFF_INFORMATION, null, values);

        db.close();
    }

    public void addImage(int id, byte[] image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_IMGE_QUESTION_ID, id);
        values.put(COLUMN_IMAGE, image);
        db.insert(TABLE_QUESTION_IMAGE, null, values);

        db.close();
    }

    public void add_checklist_category(List<PChecklistCategoryDTO.CCat.Checklistcategories> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PChecklistCategoryDTO.CCat.Checklistcategories name : list) {
                values.put(COLUMN_CATEGORY_TYPE_ID, name.getType__id());
                values.put(COLUMN_CATEGORY_TYPE_NAME, name.getType__name());
                values.put(COLUMN_CATEGORY_ORDER, name.getOrder());
                values.put(COLUMN_CATEGORY_ID, name.getId());
                values.put(COLUMN_CATEGORY_NAME, name.getName());
                db.insert(TABLE_P_CHECKLIST_CATEGORY, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void add_Designation(List<PDesignationListDTO.DList.DesignationList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PDesignationListDTO.DList.DesignationList item : list) {
                values.put(COLUMN_DESIGNATION_ID, item.getId());
                values.put(COLUMN_DESIGNATION_NAME, item.getName());
                db.insert(TABLE_DESIGNATION, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }

    public boolean isObservationDepartmentEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM Observation_Department";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }

    public boolean isQuestionEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM Question";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }


    public boolean isObservationTypeEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM ObsType";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }


    public boolean isAppPasswordEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM app_password";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }


    public boolean isShipSideEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM ShipSide";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }

    public boolean isInspectionTypeEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM InsType";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }


    public boolean isCategoryEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM checklist_category";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }

    public boolean isNationalityEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM nationality";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }

    public boolean isRankEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM designation";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;

        else
            return false;
    }


    public void add_Nationality(List<PNationalityDTO.NList.NationalityList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PNationalityDTO.NList.NationalityList item : list) {
                values.put(COLUMN_NATIONALITY_ID, item.getId());
                values.put(COLUMN_NATIONALITY_NAME, item.getName());
                db.insert(TABLE_NATIONALITY, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }

    public void add_Questions(List<PChecklistQuestion.CQData.ChecklistQuestions> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PChecklistQuestion.CQData.ChecklistQuestions item : list) {
                values.put(COLUMN_QUESTION_INFO, item.getInfo());
                values.put(COLUMN_QUESTION_CATEGORY_TYPE_ID, item.getCategory__id());
                values.put(COLUMN_QUESTION_CATEGORY_TYPE_NAME, item.getCategory__type__name());
                values.put(COLUMN__QUESTION, item.getQuestion());
                values.put(COLUMN__QUESTION_CATEGORY_ID, item.getCategory__id());
                values.put(COLUMN_QUESTION_ORDER, item.getOrder());
                values.put(COLUMN__QUESTION_CATEGORY_NAME, item.getCategory__name());
                values.put(COLUMN_QUESTION_ID, item.getId());
                db.insert(TABLE_CHECKLIST_QUESTION, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public void add_Port(List<PortListDTO.PList.PortList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PortListDTO.PList.PortList name : list) {
                values.put(COLUMN_PORT_NAME, name.getName());
                values.put(COLUMN_COUNTRY_NAME, name.getCountry__name());
                values.put(COLUMN_COUNTRY_ID, name.getCountry__id());
                values.put(COLUMN_COUNTRY_WITH_PORT, name.getFull_name());
                values.put(COLUMN_PORT_CODE, name.getPort_code());
                values.put(COLUMN_PORT_ID, name.getId());
                values.put(COLUMN_LOCATION_CODE, name.getLocode());
                db.insert(TABLE_PORT_LIST, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }


    public void add_Vessel(List<PVesselListDTO.Datum.VesselList> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PVesselListDTO.Datum.VesselList name : list) {
                values.put(COLUMN_VESSEL_USER_NAME, name.getUsername());
                values.put(COLUMN_VESSEL_EMAIL, name.getEmail());
                values.put(COLUMN_VESSEL_VMID, name.getVmid());
                values.put(COLUMN_VESSEL_ID, name.getId());
                values.put(COLUMN_VESSEL_NAME, name.getName());
                db.insert(TABLE_VESSEL_LIST, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }


    public void addPort(PortListDeatils name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PORT_NAME, name.getName());
        values.put(COLUMN_COUNTRY_NAME, name.getCountry__name());
        values.put(COLUMN_COUNTRY_ID, name.getCountry__id());
        values.put(COLUMN_COUNTRY_WITH_PORT, name.getFull_name());
        values.put(COLUMN_PORT_CODE, name.getPort_code());
        values.put(COLUMN_PORT_ID, name.getId());
        values.put(COLUMN_LOCATION_CODE, name.getLocode());
        db.insert(TABLE_PORT_LIST, null, values);
        db.close();
    }

    public void addCType(ChecklistTypeDetails name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CHECKLIST_ID, name.getId());
        values.put(COLUMN_CHECKLIST_NAME, name.getName());
        values.put(COLUMN_IS_INTERNAL, name.getIs_internal());
        db.insert(TABLE_CHECKLIST_TYPE, null, values);
        db.close();
    }


    public void add_CType(List<PChecklistTypeDTO.CType.ChecklistTypeData> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PChecklistTypeDTO.CType.ChecklistTypeData name : list) {
                values.put(COLUMN_CHECKLIST_ID, name.getId());
                values.put(COLUMN_CHECKLIST_NAME, name.getName());
                values.put(COLUMN_IS_INTERNAL, name.isIs_internal());
                db.insert(TABLE_CHECKLIST_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }


    public boolean isChecklistTypeEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM checklist_type";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }


    public boolean isPortListEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM port_list";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }


    public boolean isCompanyNameEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM company_login";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isUserNameEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM user_login";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isVesselNameEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM vessel_list";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public List<PortListDeatils> getAllCountryWithPort() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_PORT_NAME,
                COLUMN_COUNTRY_NAME,
                COLUMN_COUNTRY_ID,
                COLUMN_COUNTRY_WITH_PORT,
                COLUMN_PORT_CODE,
                COLUMN_PORT_ID,
                COLUMN_LOCATION_CODE
        };
        // sorting orders
        String sortOrder =
                COLUMN_PORT_ID + " ASC";
        List<PortListDeatils> roles = new ArrayList<PortListDeatils>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PORT_LIST, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PortListDeatils role = new PortListDeatils();
                role.setName(cursor.getString(cursor.getColumnIndex(COLUMN_PORT_NAME)));
                role.setCountry__name(cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY_NAME)));
                role.setCountry__id(cursor.getInt(cursor.getColumnIndex(COLUMN_COUNTRY_ID)));
                role.setFull_name(cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY_WITH_PORT)));
                role.setPort_code(cursor.getString(cursor.getColumnIndex(COLUMN_PORT_CODE)));
                role.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_PORT_ID)));
                role.setLocode(cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION_CODE)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public ArrayList<String> getRank() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_DESIGNATION_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_DESIGNATION, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_DESIGNATION_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public ArrayList<String> getVessels() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_VESSEL_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_VESSEL_LIST, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_VESSEL_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public ArrayList<String> getNationality() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_NATIONALITY_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_NATIONALITY, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_NATIONALITY_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public List<String> getPhotoReportCheckListImage(String id, String checkListUUID) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_PHOTO_REPORT_CHECK_LIST_IMAGE,
        };

        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        // selection arguments
        String[] selectionArgs = {id, checkListUUID};
        String selection = COLUMN_PHOTO_REPORT_CHECK_LIST_ID + " = ? AND " + COLUMN_PHOTO_REPORT_CHECK_LIST_UUID + " = ? ";

        Cursor cursor = db.query(TABLE_PHOTO_REPORT_CHECK_LIST_IMAGE, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);
        //The sort order
        List<String> images = new ArrayList<>();
        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                images.add(cursor.getString(cursor.getColumnIndex(COLUMN_PHOTO_REPORT_CHECK_LIST_IMAGE)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return images;
    }


    public QuestionCheckList getQuestionInfoCheckListByQuestionID(String questionId, String checkListUUID) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_QUESTION_CHECK_LIST_ID,
                COLUMN_QUESTION_CHECK_LIST_REMARK,
                COLUMN_QUESTION_CHECK_LIST_SELECT,
                COLUMN_QUESTION_CHECK_LIST_DATE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        // selection arguments
        String[] selectionArgs = {questionId, checkListUUID};
        String selection = COLUMN_QUESTION_CHECK_LIST_QUESTION_ID + " = ? AND " + COLUMN_QUESTION_CHECK_LIST_UUID + " = ? ";

        Cursor cursor = db.query(COLUMN_QUESTION_INFO_CHECK_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);
        //The sort order
        QuestionCheckList item = new QuestionCheckList();
        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                item.setId(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_CHECK_LIST_ID)));
                item.setRemark(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_CHECK_LIST_REMARK)));
                item.setSelect(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_CHECK_LIST_SELECT)));
                item.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_CHECK_LIST_DATE)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return item;
    }

    public List<ShipSideCheckList> getAllShipsideCheckList() {
        // array of columns to fetch
        String[] columns = {
                TABLE_SHIPSIDE_CHECK_LIST_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_REMARKS
        };
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHIPSIDE_CHECK_LIST, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<ShipSideCheckList> roles = new ArrayList<>();
        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ShipSideCheckList role = new ShipSideCheckList();
                role.setId(cursor.getString(cursor.getColumnIndex(TABLE_SHIPSIDE_CHECK_LIST_ID)));
                role.setCheckListId(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID)));
                role.setShipsideId(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID)));
                role.setRemarks(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_REMARKS)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public List<ShipSideCheckList> getAllShipsideCheckList(String checklisttypeid) {
        // array of columns to fetch
        String[] columns = {
                TABLE_SHIPSIDE_CHECK_LIST_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID,
                COLUMN_SHIPSIDE_CHECK_LIST_REMARKS
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        // selection arguments
        String[] selectionArgs = {checklisttypeid};
        String selection = COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID + " = ?";

        Cursor cursor = db.query(TABLE_SHIPSIDE_CHECK_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<ShipSideCheckList> roles = new ArrayList<>();
        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ShipSideCheckList role = new ShipSideCheckList();
                role.setId(cursor.getString(cursor.getColumnIndex(TABLE_SHIPSIDE_CHECK_LIST_ID)));
                role.setCheckListId(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_CHECKLIST_ID)));
                role.setShipsideId(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_SHIPSIDE_ID)));
                role.setRemarks(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECK_LIST_REMARKS)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public List<ShipsideName> getAllShipSide() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SHIPSIDE_CHECKLIST_TYPE_ID,
                COLUMN_SHIPSIDE_ORDER,
                COLUMN_SHIPSIDE_CHECKLIST_TYPE_NAME,
                COLUMN_SHIPSIDE_ID,
                COLUMN_SHIPSIDE_NAME
        };
        // sorting orders
        String sortOrder =
                COLUMN_SHIPSIDE_ID + " ASC";
        List<ShipsideName> roles = new ArrayList<ShipsideName>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHIPSIDE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ShipsideName role = new ShipsideName();
                role.setChecklist_type__id(cursor.getInt(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECKLIST_TYPE_ID)));
                role.setOrder(cursor.getInt(cursor.getColumnIndex(COLUMN_SHIPSIDE_ORDER)));
                role.setChecklist_type__name(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_CHECKLIST_TYPE_NAME)));
                role.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_SHIPSIDE_ID)));
                role.setName(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSIDE_NAME)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public ArrayList<String> getInspections() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_INSPECTION_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_INSPECTION_TYPE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_INSPECTION_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public ArrayList<String> getObservations() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_OBSERVATION_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_OBSERVATION_TYPE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_OBSERVATION_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public ArrayList<String> getObservationDepartment() {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_OD_NAME,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_OBSERVATION_DEPARTMENT, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        //The sort order
        ArrayList<String> data = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(cursor.getColumnIndex(COLUMN_OD_NAME)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_LOGIN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_LOGIN_EMAIL + " = ?" + " AND " + COLUMN_LOGIN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        Cursor cursor = db.query(TABLE_APP_LOGIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public boolean checkAppPassword(String pass) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_T_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {pass};

        Cursor cursor = db.query(TABLE_APP_PASSWORD, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public List<CCategoryDetails> getDataFromtype(String checklisttypeid) {

        //  final String TABLE_NAME = "name of table";
        // array of columns to fetch
        String[] columns = {
                COLUMN_CATEGORY_TYPE_ID,
                COLUMN_CATEGORY_TYPE_NAME,
                COLUMN_CATEGORY_ORDER,
                COLUMN_CATEGORY_ID,
                COLUMN_CATEGORY_NAME
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_CATEGORY_TYPE_ID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklisttypeid};

        Cursor cursor = db.query(TABLE_P_CHECKLIST_CATEGORY, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<CCategoryDetails> data = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                CCategoryDetails role = new CCategoryDetails();
                role.setType__id(cursor.getInt(cursor.getColumnIndex(COLUMN_CATEGORY_TYPE_ID)));
                role.setType__name(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_TYPE_NAME)));
                role.setOrder(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_ORDER)));
                role.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_CATEGORY_ID)));
                role.setName(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_NAME)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public SaveVessel getSaveVesselByID(int vesselId) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_VESSEL_ID,
                COLUMN_SAVE_VESSEL_START_DATE,
                COLUMN_SAVE_VESSEL_END_DATE,
                COLUMN_SAVE_VESSEL_PORT_FROM,
                COLUMN_SAVE_VESSEL_PORT_TO,
                COLUMN_SAVE_VESSEL_CHECKLIST_UUID,
                COLUMN_SAVE__VESSEL_CHECKLIST_TYPE
        };
        // sorting orders
        String selection = COLUMN_SAVE_VESSEL_ID + " = ?";

        // selection arguments
        String[] selectionArgs = {String.valueOf(vesselId)};

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_VESSEL_INFO, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order
        SaveVessel role = null;
        if (cursor.moveToFirst()) {
            do {
                role = new SaveVessel();
                role.setVesselId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_ID)));
                role.setStartDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_START_DATE)));
                role.setEndDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_END_DATE)));
                role.setPortFrom(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_FROM)));
                role.setPortTo(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_TO)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_UUID)));
                role.setChecklistType(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_CHECKLIST_TYPE)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return role;
    }

    public ArrayList<SaveVessel> getSaveVesselbtChecklistId(String checklistid) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_VESSEL_ID,
                COLUMN_SAVE_VESSEL_START_DATE,
                COLUMN_SAVE_VESSEL_END_DATE,
                COLUMN_SAVE_VESSEL_PORT_FROM,
                COLUMN_SAVE_VESSEL_PORT_TO,
                COLUMN_SAVE_VESSEL_CHECKLIST_UUID,
                COLUMN_SAVE__VESSEL_CHECKLIST_TYPE
        };
        // sorting orders
        String selection = COLUMN_SAVE_VESSEL_CHECKLIST_UUID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklistid};
        ArrayList<SaveVessel> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_VESSEL_INFO, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SaveVessel role = new SaveVessel();
                role.setVesselId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_ID)));
                role.setStartDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_START_DATE)));
                role.setEndDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_END_DATE)));
                role.setPortFrom(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_FROM)));
                role.setPortTo(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_TO)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_UUID)));
                role.setChecklistType(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_CHECKLIST_TYPE)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public ArrayList<SaveStaffinfo> getSaveStaffinfoByChecklistUUID(String checklistuuid) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_STAFF_RANK,
                COLUMN_SAVE_STAFF_NAME,
                COLUMN_SAVE_YEAR_IN_COMPANY,
                COLUMN_SAVE_MONTH_IN_COMPANY,
                COLUMN_SAVE_STAFF_NATIONALITY,
                COLUMN_SAVE_STAFF_CHECKLIST_UUID
        };
        // sorting orders
        String selection = COLUMN_SAVE_STAFF_CHECKLIST_UUID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklistuuid};
        ArrayList<SaveStaffinfo> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_STAFF_INFORMATION, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SaveStaffinfo role = new SaveStaffinfo();
                role.setRank(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_STAFF_RANK)));
                role.setName(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_STAFF_NAME)));
                role.setYearInCompany(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_YEAR_IN_COMPANY)));
                role.setMonthInCompany(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_MONTH_IN_COMPANY)));
                role.setNationality(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_STAFF_NATIONALITY)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_STAFF_CHECKLIST_UUID)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public void removeObservationByCheckListUUID(String checkListUUID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_SAVE_OBSERVATION,
                    COLUMN_SAVE_OBSERVATION_CHECKLISTUUID + " = ? ",
                    new String[]{String.valueOf(checkListUUID)});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void removeObservationById(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_SAVE_OBSERVATION,
                    COLUMN_SAVE_OBSERVATION_ID + " = ? ",
                    new String[]{String.valueOf(id)});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<SaveObservationDTO> getAllObservation() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_OBSERVATION_ID,
                COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID,
                COLUMN_SAVE_OBSERVATION_OBSERVATION,
                COLUMN_SAVE_OBSERVATION_TARGET_DATE,
                COLUMN_SAVE_OBSERVATION_CLOSE_DATE,
                COLUMN_SAVE_OBSERVATION_CHECKLISTUUID,
                COLUMN_SAVE_OBSERVATION_TYPE,
                COLUMN_SAVE_INSPECTION_TYPE,
                COLUMN_SAVE_OBSERVATION_REMARK
        };
        // selection arguments
        ArrayList<SaveObservationDTO> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_OBSERVATION, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SaveObservationDTO role = new SaveObservationDTO();
                role.setId(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_ID)));
                role.setDepartmentId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID)));
                role.setObservation(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_OBSERVATION)));
                role.setTargetDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_TARGET_DATE)));
                role.setClosedDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_CLOSE_DATE)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_CHECKLISTUUID)));
                role.setObservationType(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_TYPE)));
                role.setInspectionType(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_INSPECTION_TYPE)));
                role.setRemark(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_REMARK)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public ArrayList<SaveObservationDTO> getObservation(String checklistuuid) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_OBSERVATION_ID,
                COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID,
                COLUMN_SAVE_OBSERVATION_OBSERVATION,
                COLUMN_SAVE_OBSERVATION_TARGET_DATE,
                COLUMN_SAVE_OBSERVATION_CLOSE_DATE,
                COLUMN_SAVE_OBSERVATION_CHECKLISTUUID,
                COLUMN_SAVE_OBSERVATION_TYPE,
                COLUMN_SAVE_INSPECTION_TYPE,
                COLUMN_SAVE_OBSERVATION_REMARK
        };
        // sorting orders
        String selection = COLUMN_SAVE_OBSERVATION_CHECKLISTUUID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklistuuid};
        ArrayList<SaveObservationDTO> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_OBSERVATION, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SaveObservationDTO role = new SaveObservationDTO();
                role.setId(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_ID)));
                role.setDepartmentId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_DEPARTMENT_ID)));
                role.setObservation(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_OBSERVATION)));
                role.setTargetDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_TARGET_DATE)));
                role.setClosedDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_CLOSE_DATE)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_CHECKLISTUUID)));
                role.setObservationType(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_TYPE)));
                role.setInspectionType(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_INSPECTION_TYPE)));
                role.setRemark(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_REMARK)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public ArrayList<SaveVesselChecklist> getSaveVesselChecklist(String checklistuuid) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_VESSEL_CHECKLIST_QUESTION_ID,
                COLUMN_SAVE_VESSEL_CHECKLIST_DATE,
                COLUMN_SAVE_VESSEL_CHECKLIST_RESPONSE,
                COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID,
                COLUMN_SAVE_VESSEL_CHECKLIST_REMARK
        };
        // sorting orders
        String selection = COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklistuuid};
        ArrayList<SaveVesselChecklist> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_VESSEL_CHECKLIST, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SaveVesselChecklist role = new SaveVesselChecklist();
                role.setQuestionId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_QUESTION_ID)));
                role.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_DATE)));
                role.setResponse(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_RESPONSE)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_CHECKLISTUUID)));
                role.setRemark(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_REMARK)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public ArrayList<ObservationInfoDTO> getOCMeethings(String checklistuuid) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_OBSERVATION_INFO_VESSEL_ID,
                COLUMN_SAVE_OBSERVATION_INFO_PORT_PLACE,
                COLUMN_SAVE_OBSERVATION_INFO_REPORT_DATE,
                COLUMN_SAVE_OBSERVATION_INFO_REPORT_TIME,
                COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_DATE,
                COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_TIME,
                COLUMN_SAVE_OBSERVATION_INFO_OPENING_HELD_AT,
                COLUMN_SAVE_OBSERVATION_INFO_OPENING_ATTENDED_BY,
                COLUMN_SAVE_OBSERVATION_INFO_CLOSING_HELD_AT,
                COLUMN_SAVE_OBSERVATION_INFO_CLOSING_ATTENDED_BY,
                COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID
        };
        // sorting orders
        String selection = COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID + " = ?";

        // selection arguments
        String[] selectionArgs = {checklistuuid};
        ArrayList<ObservationInfoDTO> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVE_OBSERVATION_INFO, //Table to query
                columns,    //columns to return
                selection,        //columns for the WHERE clause
                selectionArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ObservationInfoDTO role = new ObservationInfoDTO();
                role.setVesselId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_VESSEL_ID)));
                role.setPortPlace(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_PORT_PLACE)));
                role.setReportDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_REPORT_DATE)));
                role.setReportTime(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_REPORT_TIME)));
                role.setAuditorDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_DATE)));
                role.setAuditorTime(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_AUDITOR_TIME)));
                role.setOpeningHeldAt(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_OPENING_HELD_AT)));
                role.setOpeningAttendedBy(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_OPENING_ATTENDED_BY)));
                role.setClosingHeldAt(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_CLOSING_HELD_AT)));
                role.setClosingAttendedBy(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_CLOSING_ATTENDED_BY)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_OBSERVATION_INFO_CHECKLISTUUID)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public ArrayList<SaveVessel> getAllInspections() {

        //  final String TABLE_NAME = "name of table";
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE_VESSEL_ID,
                COLUMN_SAVE_VESSEL_START_DATE,
                COLUMN_SAVE_VESSEL_END_DATE,
                COLUMN_SAVE_VESSEL_PORT_FROM,
                COLUMN_SAVE_VESSEL_PORT_TO,
                COLUMN_SAVE_VESSEL_CHECKLIST_UUID,
                COLUMN_SAVE__VESSEL_CHECKLIST_TYPE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria

        Cursor cursor = db.query(TABLE_SAVE_VESSEL_INFO, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        ArrayList<SaveVessel> data = new ArrayList<SaveVessel>();

        if (cursor.moveToFirst()) {
            do {
                SaveVessel role = new SaveVessel();
                role.setVesselId(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_ID)));
                role.setStartDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_START_DATE)));
                role.setEndDate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_END_DATE)));
                role.setPortFrom(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_FROM)));
                role.setPortTo(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_PORT_TO)));
                role.setChecklistUUID(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE_VESSEL_CHECKLIST_UUID)));
                role.setChecklistType(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_CHECKLIST_TYPE)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<QuestionName> getQuestionFromCategory(String category_id) {

        String[] columns = {
                COLUMN_QUESTION_INFO,
                COLUMN_QUESTION_CATEGORY_TYPE_ID,
                COLUMN_QUESTION_CATEGORY_TYPE_NAME,
                COLUMN__QUESTION,
                COLUMN__QUESTION_CATEGORY_ID,
                COLUMN_QUESTION_ORDER,
                COLUMN__QUESTION_CATEGORY_NAME,
                COLUMN_QUESTION_ID,
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN__QUESTION_CATEGORY_ID + " = ?";

        // selection arguments
        String[] selectionArgs = {category_id};

        Cursor cursor = db.query(TABLE_CHECKLIST_QUESTION, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<QuestionName> data = new ArrayList<QuestionName>();

        if (cursor.moveToFirst()) {
            do {
                QuestionName role = new QuestionName();
                role.setInfo(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_INFO)));
                role.setCategory__type__id(cursor.getInt(cursor.getColumnIndex(COLUMN_QUESTION_CATEGORY_TYPE_ID)));
                role.setCategory__type__name(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_CATEGORY_TYPE_NAME)));
                role.setQuestion(cursor.getString(cursor.getColumnIndex(COLUMN__QUESTION)));
                role.setCategory__id(cursor.getInt(cursor.getColumnIndex(COLUMN__QUESTION_CATEGORY_ID)));
                role.setOrder(cursor.getString(cursor.getColumnIndex(COLUMN_QUESTION_ORDER)));
                role.setCategory__name(cursor.getString(cursor.getColumnIndex(COLUMN__QUESTION_CATEGORY_NAME)));
                role.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_QUESTION_ID)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<ShipSafe> getAllShipSafe() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_SHIPSAFE_VASSEL_ID,
                COLUMN_SHIPSAFE_REMARK,
                COLUMN_SHIPSAFE_SIGNATURES,
                COLUMN_SHIPSAFE_INSPECTOR
        };

        List<ShipSafe> roles = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHIPSAFE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ShipSafe role = new ShipSafe();
                role.setVasselId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSAFE_VASSEL_ID))));
                role.setRemark(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSAFE_REMARK)));
                role.setSignatures(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSAFE_SIGNATURES)));
                role.setInspector(cursor.getString(cursor.getColumnIndex(COLUMN_SHIPSAFE_INSPECTOR)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public List<ChecklistTypeDetails> getAllType() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_CHECKLIST_ID,
                COLUMN_CHECKLIST_NAME,
                COLUMN_IS_INTERNAL
        };
        // sorting orders
        String sortOrder =
                COLUMN_CHECKLIST_ID + " ASC";
        List<ChecklistTypeDetails> roles = new ArrayList<ChecklistTypeDetails>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CHECKLIST_TYPE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChecklistTypeDetails role = new ChecklistTypeDetails();
                role.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CHECKLIST_ID))));
                role.setName(cursor.getString(cursor.getColumnIndex(COLUMN_CHECKLIST_NAME)));
                role.setIs_internal(cursor.getString(cursor.getColumnIndex(COLUMN_IS_INTERNAL)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public List<UserInfo> getUserInfo(String email) {
        // array of columns to fetch
        String[] columns = {
                COLUMN_LOGIN_LAST_NAME,
                COLUMN_LOGIN_EMAIL,
                COLUMN_LOGIN_FIRST_NAME,
                COLUMN_LOGIN_USER_PASSWORD
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // sorting orders
        String selection = COLUMN_LOGIN_EMAIL + " = ?";

        // selection arguments
        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_APP_LOGIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order//The sort order


        List<UserInfo> userList = new ArrayList<>();
        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserInfo user = new UserInfo();
                user.setLName(cursor.getString(cursor.getColumnIndex(COLUMN_LOGIN_LAST_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_LOGIN_EMAIL)));
                user.setFName(cursor.getString(cursor.getColumnIndex(COLUMN_LOGIN_FIRST_NAME)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_LOGIN_USER_PASSWORD)));
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }

    public String getPortTitle(int portId) {
        String[] columns = {
                COLUMN_PORT_NAME
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_PORT_ID + " = ?";
        String[] selectionArgs = {String.valueOf(portId)};
        Cursor cursor = db.query(TABLE_PORT_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        String data = "";
        if (cursor.moveToFirst()) {
            do {
                data = cursor.getString(cursor.getColumnIndex(COLUMN_PORT_NAME));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public int getPortId(String countrywithport) {
        String[] columns = {
                COLUMN_PORT_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_COUNTRY_WITH_PORT + " = ?";
        String[] selectionArgs = {countrywithport};
        Cursor cursor = db.query(TABLE_PORT_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public int getDepartmentId(String departmentname) {
        String[] columns = {
                COLUMN_OD_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_OD_NAME + " = ?";
        String[] selectionArgs = {departmentname};
        Cursor cursor = db.query(TABLE_OBSERVATION_DEPARTMENT, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public String getDepartmentName(int departamentId) {
        String[] columns = {
                COLUMN_OD_NAME
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_OD_ID + " = ?";
        String[] selectionArgs = {String.valueOf(departamentId)};
        Cursor cursor = db.query(TABLE_OBSERVATION_DEPARTMENT, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = "";

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public String getVesselName(int vesselId) {
        String[] columns = {
                COLUMN_VESSEL_NAME
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_VESSEL_ID + " = ?";
        String[] selectionArgs = {String.valueOf(vesselId)};
        Cursor cursor = db.query(TABLE_VESSEL_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        String data = "";
        if (cursor.moveToFirst()) {
            do {
                data = cursor.getString(cursor.getColumnIndex(COLUMN_VESSEL_NAME));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public void deleteCheckListDataById(String checkListId) {
        deleteVasselInfoByCheckId(checkListId);
    }

    public void deleteVasselInfoByCheckId(String checkListId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(
                    TABLE_SAVE_VESSEL_INFO,  // Where to delete
                    COLUMN_SAVE_VESSEL_CHECKLIST_UUID + " = ? ",
                    new String[]{checkListId});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public int getVesselId(String vesselname) {
        String[] columns = {
                COLUMN_VESSEL_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_VESSEL_NAME + " = ?";
        String[] selectionArgs = {vesselname};
        Cursor cursor = db.query(TABLE_VESSEL_LIST, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public int getQuestionId(String questionwithguide) {
        String[] columns = {
                COLUMN_QUESTION_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN__QUESTION + " = ?";
        String[] selectionArgs = {questionwithguide};
        Cursor cursor = db.query(TABLE_CHECKLIST_QUESTION, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }



   /* public List<SaveVesselInfoDTO> getDataFromChecklistId(String ChecklistId) {

        //  final String TABLE_NAME = "name of table";
        // array of columns to fetch
        String[] columns = {
                COLUMN_SAVE__VESSEL_INFO_END_DATE,
                COLUMN_SAVE__VESSEL_INFO_CREATED_AT,
                COLUMN__SAVE__VESSEL_INFO_START_DATE,
                COLUMN__SAVE__VESSEL_INFO_CHECKLIST_TYPE_ID,
                COLUMN_SAVE__VESSEL_INFO_FROM_PORT_ID,
                COLUMN__SAVE__VESSEL_INFO_TO_PORT_ID,
                COLUMN__SAVE__VESSEL_INFO_MODIFIED_ON,
                COLUMN__SAVE__VESSEL_INFO_VESSEL_ID

        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_SAVE_VESSEL_INFO_CHECKLIST_ID + " = ?";

        // selection arguments
        String[] selectionArgs = {ChecklistId};

        Cursor cursor = db.query(TABLE_SAVE_VESSEL_INFO, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<SaveVesselInfoDTO> data = new ArrayList<SaveVesselInfoDTO>();

        if (cursor.moveToFirst()) {
            do {
                SaveVesselInfoDTO role = new SaveVesselInfoDTO();
                role.setEnddate(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_INFO_END_DATE)));
                role.setCreated_at(cursor.getString(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_INFO_CREATED_AT)));
                role.setStartdate(cursor.getString(cursor.getColumnIndex(COLUMN__SAVE__VESSEL_INFO_START_DATE)));
                role.setChecklisttypeid(cursor.getInt(cursor.getColumnIndex(COLUMN__SAVE__VESSEL_INFO_CHECKLIST_TYPE_ID)));
                role.setFromportid(cursor.getInt(cursor.getColumnIndex(COLUMN_SAVE__VESSEL_INFO_FROM_PORT_ID)));
                role.setToportid(cursor.getInt(cursor.getColumnIndex(COLUMN__SAVE__VESSEL_INFO_TO_PORT_ID)));
                role.setModified0n(cursor.getString(cursor.getColumnIndex(COLUMN__SAVE__VESSEL_INFO_MODIFIED_ON)));
                role.setVessel_id(cursor.getInt(cursor.getColumnIndex(COLUMN__SAVE__VESSEL_INFO_VESSEL_ID)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }*/


    public String IsInternal(String checklistid) {
        String[] columns = {
                COLUMN_IS_INTERNAL
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_CHECKLIST_ID + " = ?";
        String[] selectionArgs = {checklistid};
        Cursor cursor = db.query(TABLE_CHECKLIST_TYPE, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = "";

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;

    }

    public void deleteTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_APP_LOGIN, null, null);
        db.delete(TABLE_CHECKLIST_TYPE, null, null);
        db.delete(TABLE_CHECKLIST_QUESTION, null, null);
        db.delete(TABLE_PORT_LIST, null, null);
        db.delete(TABLE_NATIONALITY, null, null);
        db.delete(TABLE_DESIGNATION, null, null);
        db.delete(TABLE_P_CHECKLIST_CATEGORY, null, null);
        db.delete(TABLE_SHIPSIDE, null, null);

        db.close();
    }
}






/*
    public List<Top_V_Item> getAllInventory() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_ICODE_ITEM,
                COLUMN_BARCODE_ITEM,
                COLUMN_ITEMNAME_ITEM,
                COLUMN_SEC_ITEM,
                COLUMN_DEPT_ITEM,
                COLUMN_RSP_ITEM,
                COLUMN_BRAND_ITEM,
                COLUMN_LAST_CHANGE_ITEM
        };
        // sorting orders
        String sortOrder =
                COLUMN_ADMIN_CODE + " ASC";
        List<Top_V_Item> roles = new ArrayList<Top_V_Item>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 *
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method is to fetch all user and return the list of user records
 * @return list
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 * <p>
 * This method to update user record
 * @param user
 * <p>
 * This method is to delete user record
 * @param This method to check user exist or not
 * @param email
 * @return true/false
 * <p>
 * This method to check user exist or not
 * @param email
 * @return true/false
 * @para Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 * <p>
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 *//*
        Cursor cursor = db.query(TABLE_V_ITEM, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Top_V_Item role = new Top_V_Item();
                role.setRSP(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_RSP_ITEM))));
                role.setBARCODE(cursor.getString(cursor.getColumnIndex(COLUMN_BARCODE_ITEM)));
                role.setITEMNAME(cursor.getString(cursor.getColumnIndex(COLUMN_ITEMNAME_ITEM)));
                role.setSEC(cursor.getString(cursor.getColumnIndex(COLUMN_SEC_ITEM)));
                role.setDEPT(cursor.getString(cursor.getColumnIndex(COLUMN_DEPT_ITEM)));
                role.setICODE(cursor.getString(cursor.getColumnIndex(COLUMN_ICODE_ITEM)));
                role.setBRAND(cursor.getString(cursor.getColumnIndex(COLUMN_BRAND_ITEM)));
                role.setLAST_CHANGE(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_CHANGE_ITEM)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }

    public List<TOP_SITE> getAllSites() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_ADMIN_CODE,
                COLUMN_SHORT_NAME,
                COLUMN_SITE_NAME,
                COLUMN_SITE_TYPE,
                COLUMN_SQLDB_NAME,
                COLUMN_SQL_USER,
                COLUMN_SQL_PASS,
                COLUMN_TERMINAL_NAME
        };
        // sorting orders
        String sortOrder =
                COLUMN_ADMIN_CODE + " ASC";
        List<TOP_SITE> roles = new ArrayList<TOP_SITE>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 *//*
        Cursor cursor = db.query(TABLE_TOP_SITE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TOP_SITE role = new TOP_SITE();
                role.setADMSITE_CODE(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ADMIN_CODE))));
                role.setSHORTNAME(cursor.getString(cursor.getColumnIndex(COLUMN_SHORT_NAME)));
                role.setSITENAME(cursor.getString(cursor.getColumnIndex(COLUMN_SITE_NAME)));
                role.setSITE_TYPE(cursor.getString(cursor.getColumnIndex(COLUMN_SITE_TYPE)));
                role.setSQLDBNAME(cursor.getString(cursor.getColumnIndex(COLUMN_SQLDB_NAME)));
                role.setSQLDBUSERNAM(cursor.getString(cursor.getColumnIndex(COLUMN_SQL_USER)));
                role.setSQLDBPASSWORD(cursor.getString(cursor.getColumnIndex(COLUMN_SQL_PASS)));
                role.setSQLDBTERMINAL(cursor.getString(cursor.getColumnIndex(COLUMN_TERMINAL_NAME)));
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }


    public void addAdmin(Admin admin) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, admin.getId());
        values.put(COLUMN_ADMIN_NAME, admin.getName());
        values.put(COLUMN_ADMIN_TYPE, admin.getUtype());
        values.put(COLUMN_ROLE_ID, admin.getRoll_id());
        values.put(COLUMN_ADMIN_EMAIL, admin.getEmail());
        values.put(COLUMN_ADMIN_PASSWORD, admin.getPassword());

        // Inserting Row
        db.insert(TABLE_ADMIN, null, values);
        db.close();
    }

    public void addstk(Top_STK_TABLE stk) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ICODE, stk.getIcode());
        values.put(COLUMN_AD_CODE, stk.getAdcode());
        values.put(COLUMN_QTY, stk.getQty());
        values.put(COLUMN_LAST_CHANGE_STK, stk.getLastchange());
        values.put(COLUMN_LAST_UPD, stk.getLastupdate());

        // Inserting Row
        db.insert(TABLE_TOP_STK, null, values);
        db.close();
    }


    public void addrole(Role role) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ROLE_ID_TABLE, role.getRoll_id());
        values.put(COLUMN_MODULENAME, role.getModulename());
        values.put(COLUMN_U_ADD, role.getUadd());
        values.put(COLUMN_U_MODIFY, role.getUmodify());
        values.put(COLUMN_U_DEL, role.getUdel());
        values.put(COLUMN_U_VIEW, role.getUview());
        values.put(COLUMN_LAST_CHANGE, role.getLastchange());

        // Inserting Row
        db.insert(TABLE_ROLE, null, values);
        db.close();
    }

    *//**
 * This method is to fetch all user and return the list of user records
 *
 * @return list
 *//*

    public List<Admin> getAllAdmin() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_ADMIN_ID,
                COLUMN_ADMIN_EMAIL,
                COLUMN_ADMIN_NAME,
                COLUMN_ADMIN_PASSWORD
        };
        // sorting orders
        String sortOrder =
                COLUMN_ADMIN_NAME + " ASC";
        List<Admin> userList = new ArrayList<Admin>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 *//*
        Cursor cursor = db.query(TABLE_ADMIN, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Admin user = new Admin();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ADMIN_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_ADMIN_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_ADMIN_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_ADMIN_PASSWORD)));
                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }


    public List<Role> getAllRoles() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_ROLE_ID_TABLE,
                COLUMN_MODULENAME,
                COLUMN_U_ADD,
                COLUMN_U_MODIFY,
                COLUMN_U_DEL,
                COLUMN_U_VIEW,
                COLUMN_LAST_CHANGE
        };
        // sorting orders
        String sortOrder =
                COLUMN_ROLE_ID_TABLE + " ASC";
        List<Role> roles = new ArrayList<Role>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
 *//*
        Cursor cursor = db.query(TABLE_ROLE, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Role role = new Role();
                role.setRoll_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ROLE_ID_TABLE))));
                role.setModulename(cursor.getString(cursor.getColumnIndex(COLUMN_MODULENAME)));
                role.setUadd(cursor.getString(cursor.getColumnIndex(COLUMN_U_ADD)));
                role.setUmodify(cursor.getString(cursor.getColumnIndex(COLUMN_U_MODIFY)));
                role.setUdel(cursor.getString(cursor.getColumnIndex(COLUMN_U_DEL)));
                role.setUview(cursor.getString(cursor.getColumnIndex(COLUMN_U_VIEW)));
                role.setLastchange(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_CHANGE)));
                // Adding user record to list
                roles.add(role);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return roles;
    }
    *//**
 * This method to update user record
 *
 * @param user
 *//*

 *//**
 * This method is to delete user record
 *
 * @param
 *//*


    public void deleteRole(Role role) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_ROLE, COLUMN_ROLE_ID_TABLE + " = ?",
                new String[]{String.valueOf(role.getRoll_id())});
        db.close();
    }

    *//**
 * This method to check user exist or not
 *
 * @param email
 * @return true/false
 *//*

 *//**
 * This method to check user exist or not
 *
 * @param email
 * @return true/false
 * @para
 *//*
    public boolean checkAdmin(String email) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_ADMIN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_ADMIN_EMAIL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
 *//*
        Cursor cursor = db.query(TABLE_ADMIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public boolean checkAdmin(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_ADMIN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ADMIN_NAME + " = ?" + " AND " + COLUMN_ADMIN_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 *//*
        Cursor cursor = db.query(TABLE_ADMIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public List<Role> getroledata(String roleid) {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_MODULENAME,
                COLUMN_U_ADD,
                COLUMN_U_MODIFY,
                COLUMN_U_DEL,
                COLUMN_U_VIEW,
                COLUMN_LAST_CHANGE

        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ROLE_ID_TABLE + " = ?";

        // selection arguments
        String[] selectionArgs = {roleid};

        // query user table with conditions
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 *//*
        Cursor cursor = db.query(TABLE_ROLE, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Role> data = new ArrayList<Role>();

        if (cursor.moveToFirst()) {
            do {
                Role role = new Role();
                role.setModulename(cursor.getString(cursor.getColumnIndex(COLUMN_MODULENAME)));
                role.setUadd(cursor.getString(cursor.getColumnIndex(COLUMN_U_ADD)));
                role.setUmodify(cursor.getString(cursor.getColumnIndex(COLUMN_U_MODIFY)));
                role.setUdel(cursor.getString(cursor.getColumnIndex(COLUMN_U_DEL)));
                role.setUview(cursor.getString(cursor.getColumnIndex(COLUMN_U_VIEW)));
                role.setLastchange(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_CHANGE)));
                data.add(role);


            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public String getUserTYPE(String name) {
        String[] columns = {
                COLUMN_ADMIN_TYPE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ADMIN_NAME + " = ?";
        String[] selectionArgs = {name};
        Cursor cursor = db.query(TABLE_ADMIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = null;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public int getRoleID(String name) {
        String[] columns = {
                COLUMN_ROLE_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ADMIN_NAME + " = ?";
        String[] selectionArgs = {name};
        Cursor cursor = db.query(TABLE_ADMIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public String getModuleNameFromRole(String roleid) {
        String[] columns = {
                COLUMN_MODULENAME
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ROLE_ID_TABLE + " = ?";
        String[] selectionArgs = {roleid};
        Cursor cursor = db.query(TABLE_ROLE, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = null;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public int getEnt_No(String fileno) {
        String[] columns = {
                COLUMN_ENT_CODE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_FILE_NO + " = ?";
        String[] selectionArgs = {fileno};
        Cursor cursor = db.query(TABLE_INV_MAIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public int getGPEnt_No(String fileno) {
        String[] columns = {
                COLUMN_GP_ENT_CODE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_GP_NO + " = ?";
        String[] selectionArgs = {fileno};
        Cursor cursor = db.query(TABLE_GP_MAIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public int getDel_Ent_No(String fileno) {
        String[] columns = {
                COLUMN_DEL_ENT_CODE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_DEL_FILE_NO + " = ?";
        String[] selectionArgs = {fileno};
        Cursor cursor = db.query(TABLE_DEL_MAIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public int getSiteCode(String sitename) {
        String[] columns = {
                COLUMN_ADMIN_CODE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_SITE_NAME + " = ?";
        String[] selectionArgs = {sitename};
        Cursor cursor = db.query(TABLE_TOP_SITE, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        int data = 0;

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getInt(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }


    public String itemname(String name) {
        String[] columns = {
                COLUMN_ITEMNAME_ITEM
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ICODE_ITEM + " = ?";
        String[] selectionArgs = {name};
        Cursor cursor = db.query(TABLE_V_ITEM, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = "";

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public boolean isGPEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM gp_main";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }


    public boolean isTableEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM admin";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isSTKEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM stk";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isSiteEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM site";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isItemEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM v_item";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }

    public boolean isUserEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM admin";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0)
            return true;
        else
            return false;
    }


    public String geticode(String icode) {
        String[] columns = {
                COLUMN_ICODE_ITEM
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ICODE_ITEM + " = ?";
        String[] selectionArgs = {icode};
        Cursor cursor = db.query(TABLE_V_ITEM, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();
        String data = "";

        if (cursor.moveToFirst()) {
            do {

                data = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return data;
    }

    public List<Top_V_Item> getdatafromicode(String icode) {

        //  final String TABLE_NAME = "name of table";


        // array of columns to fetch
        String[] columns = {
                COLUMN_ICODE_ITEM,
                COLUMN_BARCODE_ITEM,
                COLUMN_ITEMNAME_ITEM,
                COLUMN_RSP_ITEM,
                COLUMN_SEC_ITEM,
                COLUMN_DEPT_ITEM,
                COLUMN_DIV,
                COLUMN_BRAND_ITEM,
                COLUMN_LAST_CHANGE_ITEM
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_ICODE_ITEM + " = ?" + " OR " + COLUMN_BARCODE_ITEM + " = ?";

        // selection arguments
        String[] selectionArgs = {icode, icode};

        // query user table with conditions
        *//**
 * Here query function is used to fetch records from user table this function works like we use sql query.
 * SQL query equivalent to this query function is
 * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
 *//*
        Cursor cursor = db.query(TABLE_V_ITEM, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Top_V_Item> data = new ArrayList<Top_V_Item>();

        if (cursor.moveToFirst()) {
            do {
                Top_V_Item role = new Top_V_Item();
                role.setICODE(cursor.getString(cursor.getColumnIndex(COLUMN_ICODE_ITEM)));
                role.setBARCODE(cursor.getString(cursor.getColumnIndex(COLUMN_BARCODE_ITEM)));
                role.setITEMNAME(cursor.getString(cursor.getColumnIndex(COLUMN_ITEMNAME_ITEM)));
                role.setRSP(cursor.getInt(cursor.getColumnIndex(COLUMN_RSP_ITEM)));
                role.setSEC(cursor.getString(cursor.getColumnIndex(COLUMN_SEC_ITEM)));
                role.setDEPT(cursor.getString(cursor.getColumnIndex(COLUMN_DEPT_ITEM)));
                role.setDIV(cursor.getString(cursor.getColumnIndex(COLUMN_DIV)));
                role.setBRAND(cursor.getString(cursor.getColumnIndex(COLUMN_BRAND_ITEM)));
                role.setLAST_CHANGE(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_CHANGE_ITEM)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public List<Inv_Main_Item> getInvMain() {

        String[] columns = {
                COLUMN_ENT_CODE,
                COLUMN_FILE_NAME,
                COLUMN_FILE_NO,
                COLUMN_FILE_TYPE,
                COLUMN_FILE_DATE,
                COLUMN_TID,
                COLUMN_T_LOC,
                COLUMN_F_USER,
                COLUMN_LOCK_STATUS,
                COLUMN_FILE_LAST_UPD
        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_INV_MAIN, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Inv_Main_Item> data = new ArrayList<Inv_Main_Item>();

        if (cursor.moveToFirst()) {
            do {
                Inv_Main_Item role = new Inv_Main_Item();
                role.setEnt_code(cursor.getInt(cursor.getColumnIndex(COLUMN_ENT_CODE)));
                role.setFile_name(cursor.getString(cursor.getColumnIndex(COLUMN_FILE_NAME)));
                role.setFile_no(cursor.getInt(cursor.getColumnIndex(COLUMN_FILE_NO)));
                role.setFile_type(cursor.getString(cursor.getColumnIndex(COLUMN_FILE_TYPE)));
                role.setFile_date(cursor.getString(cursor.getColumnIndex(COLUMN_FILE_DATE)));
                role.setTid(cursor.getString(cursor.getColumnIndex(COLUMN_TID)));
                role.setT_loc(cursor.getString(cursor.getColumnIndex(COLUMN_T_LOC)));
                role.setF_user(cursor.getString(cursor.getColumnIndex(COLUMN_F_USER)));
                role.setLock_status(cursor.getString(cursor.getColumnIndex(COLUMN_LOCK_STATUS)));
                role.setLastupdate(cursor.getString(cursor.getColumnIndex(COLUMN_FILE_LAST_UPD)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<Inv_Det_Item> getInvDet() {

        String[] columns = {
                COLUMN_MAIN_ENTCODE,
                COLUMN_DET_ICODE,
                COLUMN_DET_QTY
        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_INV_DET, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Inv_Det_Item> data = new ArrayList<Inv_Det_Item>();

        if (cursor.moveToFirst()) {
            do {
                Inv_Det_Item role = new Inv_Det_Item();
                role.setMain_entcode(cursor.getInt(cursor.getColumnIndex(COLUMN_MAIN_ENTCODE)));
                role.setIcode(cursor.getString(cursor.getColumnIndex(COLUMN_DET_ICODE)));
                role.setQty(cursor.getString(cursor.getColumnIndex(COLUMN_DET_QTY)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<GP_Main> getGPMain() {

        String[] columns = {
                COLUMN_GP_ENT_CODE,
                COLUMN_GP_TERMINAL_NO,
                COLUMN_GP_STAFF_ID,
                COLUMN_GP_STAFF_NAME,
                COLUMN_GP_CUST_MOB,
                COLUMN_GP_REMARKS,
                COLUMN_GP_BILL_NO,
                COLUMN_GP_NO,
                COLUMN_GP_DATE,
                COLUMN_GP_SITE_CODE,
                COLUMN_GP_CREATED_BY
        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_GP_MAIN, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<GP_Main> data = new ArrayList<GP_Main>();

        if (cursor.moveToFirst()) {
            do {
                GP_Main role = new GP_Main();
                role.setEnt_code(cursor.getInt(cursor.getColumnIndex(COLUMN_GP_ENT_CODE)));
                role.setTerminalno(cursor.getString(cursor.getColumnIndex(COLUMN_GP_TERMINAL_NO)));
                role.setStaffid(cursor.getInt(cursor.getColumnIndex(COLUMN_GP_STAFF_ID)));
                role.setStaffname(cursor.getString(cursor.getColumnIndex(COLUMN_GP_STAFF_NAME)));
                role.setCust_mobile(cursor.getString(cursor.getColumnIndex(COLUMN_GP_CUST_MOB)));
                role.setGp_remarks(cursor.getString(cursor.getColumnIndex(COLUMN_GP_REMARKS)));
                role.setBillno(cursor.getString(cursor.getColumnIndex(COLUMN_GP_BILL_NO)));
                role.setGp_no(cursor.getString(cursor.getColumnIndex(COLUMN_GP_NO)));
                role.setGp_date(cursor.getString(cursor.getColumnIndex(COLUMN_GP_DATE)));
                role.setGp_site_code(cursor.getInt(cursor.getColumnIndex(COLUMN_GP_SITE_CODE)));
                role.setCreatedby(cursor.getString(cursor.getColumnIndex(COLUMN_GP_CREATED_BY)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<GP_Det> getGPDet() {

        String[] columns = {
                COLUMN_GP_MAIN_ENTCODE,
                COLUMN_GP_ICODE,
                COLUMN_GP_QTY,
                COLUMN_GP_DT_REM,
                COLUMN_GP_IMG_URL
        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_GP_DET, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<GP_Det> data = new ArrayList<GP_Det>();

        if (cursor.moveToFirst()) {
            do {
                GP_Det role = new GP_Det();
                role.setMain_entcode(cursor.getInt(cursor.getColumnIndex(COLUMN_GP_MAIN_ENTCODE)));
                role.setIcode(cursor.getString(cursor.getColumnIndex(COLUMN_GP_ICODE)));
                role.setQty(cursor.getString(cursor.getColumnIndex(COLUMN_GP_QTY)));
                role.setGp_rem(cursor.getString(cursor.getColumnIndex(COLUMN_GP_DT_REM)));
                role.setGp_img_url(cursor.getString(cursor.getColumnIndex(COLUMN_GP_IMG_URL)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public List<Del_Main_Item> getDelMain() {

        String[] columns = {
                COLUMN_DEL_ENT_CODE,
                COLUMN_DEL_FILE_NAME,
                COLUMN_DEL_FILE_NO,
                COLUMN_DEL_FILE_TYPE,
                COLUMN_DEL_FILE_DATE,
                COLUMN_DEL_TID,
                COLUMN_DEL_T_LOC,
                COLUMN_DEL_F_USER,
                COLUMN_DEL_LOCK_STATUS,
                COLUMN_DEL_FILE_LAST_UPD,
                COLUMN_DEL_TO_SITE_CODE,
                COLUMN_DEL_TO_SITE_NAME,
                COLUMN_DEL_FROM_SITE_CODE,
                COLUMN_DEL_FROM_SITE_NAME

        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_DEL_MAIN, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Del_Main_Item> data = new ArrayList<Del_Main_Item>();

        if (cursor.moveToFirst()) {
            do {
                Del_Main_Item role = new Del_Main_Item();
                role.setEnt_code(cursor.getInt(cursor.getColumnIndex(COLUMN_DEL_ENT_CODE)));
                role.setFile_name(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_FILE_NAME)));
                role.setFile_no(cursor.getInt(cursor.getColumnIndex(COLUMN_DEL_FILE_NO)));
                role.setFile_type(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_FILE_TYPE)));
                role.setFile_date(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_FILE_DATE)));
                role.setTid(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_TID)));
                role.setT_loc(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_T_LOC)));
                role.setF_user(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_F_USER)));
                role.setLock_status(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_LOCK_STATUS)));
                role.setLastupdate(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_FILE_LAST_UPD)));
                role.setFrom_site_code(cursor.getInt(cursor.getColumnIndex(COLUMN_DEL_FROM_SITE_CODE)));
                role.setTo_site_code(cursor.getInt(cursor.getColumnIndex(COLUMN_DEL_TO_SITE_CODE)));
                role.setFrom_site_name(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_FROM_SITE_NAME)));
                role.setTo_site_name(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_TO_SITE_NAME)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public List<Del_Det_Item> getDelDet() {

        String[] columns = {
                COLUMN_DEL_MAIN_ENTCODE,
                COLUMN_DEL_DET_ICODE,
                COLUMN_DEL_DET_QTY
        };
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_DEL_DET, //Table to query
                columns,                    //columns to return
                null,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order
        List<Del_Det_Item> data = new ArrayList<Del_Det_Item>();

        if (cursor.moveToFirst()) {
            do {
                Del_Det_Item role = new Del_Det_Item();
                role.setMain_entcode(cursor.getInt(cursor.getColumnIndex(COLUMN_DEL_MAIN_ENTCODE)));
                role.setIcode(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_DET_ICODE)));
                role.setQty(cursor.getString(cursor.getColumnIndex(COLUMN_DEL_DET_QTY)));
                data.add(role);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public void deletetable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INV_MAIN, null, null);
        db.delete(TABLE_INV_DET, null, null);
        db.close();
    }

    public void deletetableDel() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DEL_MAIN, null, null);
        db.delete(TABLE_DEL_DET, null, null);
        db.close();
    }

    public void datareset() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TOP_SITE, null, null);
        db.delete(TABLE_TOP_STK, null, null);
        db.delete(TABLE_V_ITEM, null, null);
        db.close();
    }

    public void dellocal() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DEL_LOCAL, null, null);
        db.close();
    }

    public void delgp() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GP_MAIN, null, null);
        db.delete(TABLE_GP_DET, null, null);
        db.close();
    }
    public void  delinv() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INV_MAIN, null, null);
        db.delete(TABLE_INV_DET, null, null);
        db.close();
    }


    public void deleteicodefromlocal(String icode, String pos) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_DEL_LOCAL, COLUMN_DEL_LOCAL_ICODE + " = ?" + " AND " + COLUMN_DEL_LOCAL_POS + " = ?",
                new String[]{icode, pos});
        db.close();
    }*/



