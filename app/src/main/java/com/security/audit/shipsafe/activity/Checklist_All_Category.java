package com.security.audit.shipsafe.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.AllTypeListAdapter;
import com.security.audit.shipsafe.model.CCategoryDetails;
import com.security.audit.shipsafe.model.ChecklistTypeDTO;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.util.List;

public class Checklist_All_Category extends AppCompatActivity {
    Context mContext;
    RecyclerView customerRecycler;
    List<CCategoryDetails> Dto = null;
    ChecklistTypeDTO dto = null;
    DatabaseHelperAdmin databaseHelperAdmin, databaseHelperAdminId, databaseHelperIsInternal;
    Button btnSubmitReport;
    private LinearLayout PhotoReportGenerate;
    private LinearLayout ObservationReportGenerate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_list_view_all_type);
        getSupportActionBar().setTitle("Sections");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        initView();
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        databaseHelperAdminId = new DatabaseHelperAdmin(mContext);
        databaseHelperIsInternal = new DatabaseHelperAdmin(mContext);
        customerRecycler = (RecyclerView) findViewById(R.id.customerRecycler);


        btnSubmitReport = findViewById(R.id.btnSubmitReport);

       /* Intent intent = getIntent();
        dto = intent.getParcelableExtra("TypeObject");
            int id=dto.getID();*/
      /*  String ChecklistId = getIntent().getStringExtra("U_ID");

        List<SaveVesselInfoDTO> DTO = new ArrayList<>();

        DTO = databaseHelperAdminId.getDataFromChecklistId(ChecklistId);
        int id = DTO.get(0).getChecklisttypeid();*/
        final String ChecklistId = getIntent().getStringExtra("U_ID");
        final int id = getIntent().getIntExtra("CID", 0);
        final int Vesselid = getIntent().getIntExtra("V_ID", 0);
        btnSubmitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, FinalSubmit.class);
                i.putExtra("V_ID", Vesselid);
                i.putExtra("Checklist_UUID", ChecklistId);
                startActivity(i);
            }
        });
        PhotoReportGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, PhotoReport.class);
                i.putExtra("V_ID", Vesselid);
                i.putExtra("Checklist_UUID", ChecklistId);
                startActivity(i);

            }
        });
        ObservationReportGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String is_internal = databaseHelperIsInternal.IsInternal(String.valueOf(id));

                if (is_internal.equals("1")) {
                    Intent i = new Intent(mContext, Is_Internal_Details.class);
                    i.putExtra("V_ID", Vesselid);
                    i.putExtra("Checklist_UUID", ChecklistId);
                    i.putExtra("Is_Internal", is_internal);
                    startActivity(i);
                } else {
                    Intent i = new Intent(mContext, ObservationSecond.class);
                    i.putExtra("V_ID", Vesselid);
                    i.putExtra("Checklist_UUID", ChecklistId);
                    i.putExtra("Is_Internal", is_internal);
                    startActivity(i);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        customerRecycler.setLayoutManager(layoutManager);
        // For Android
       /* RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        customerRecycler.setLayoutManager(mLayoutManager);*/   //For Tablet
        Dto = databaseHelperAdmin.getDataFromtype(String.valueOf(id));
        AllTypeListAdapter adapter = new AllTypeListAdapter(mContext, Dto, ChecklistId);
        customerRecycler.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private void initView() {
        PhotoReportGenerate = (LinearLayout) findViewById(R.id.PhotoReportGenerate);
        ObservationReportGenerate = (LinearLayout) findViewById(R.id.ObservationReportGenerate);
    }
}
