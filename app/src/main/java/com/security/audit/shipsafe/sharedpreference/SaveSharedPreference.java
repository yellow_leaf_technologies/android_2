package com.security.audit.shipsafe.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setRequestCode(Context ctx, String code) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("RequestCode", code);
        editor.commit();
    }

    public static String getRequestCode(Context ctx) {
        return getSharedPreferences(ctx).getString("RequestCode", "");
    }

    public static void setCompanyName(Context ctx, String TerminalName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("CompanyName", TerminalName);
        editor.commit();
    }

    public static String getCompanyName(Context ctx) {
        return getSharedPreferences(ctx).getString("CompanyName", "");
    }

    public static void setStatus(Context ctx, String status) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("Status", status);
        editor.commit();
    }

    public static String getStatus(Context ctx) {
        return getSharedPreferences(ctx).getString("Status", "");
    }

    public static void removeStatus(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("Status");
        editor.commit();
    }






    public static void setUserName(Context ctx, String name) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("UNAME", name);
        editor.commit();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString("UNAME", "");
    }

    public static void removeUser(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("UNAME");
        editor.commit();
    }



    public static void setUserEmail(Context ctx, String name) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("UEMAIL", name);
        editor.commit();
    }

    public static String getUserEmail(Context ctx) {
        return getSharedPreferences(ctx).getString("UEMAIL", "");
    }

    public static void removeUserEmail(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("UEMAIL");
        editor.commit();
    }
    public static void setMacId(Context ctx, String mac) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("MAC", mac);
        editor.commit();
    }

    public static String getMacId(Context ctx) {
        return getSharedPreferences(ctx).getString("MAC", "");
    }

    public static void removeMacId(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("MAC");
        editor.commit();
    }

    public static void setAutoMac(Context ctx, int mac) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putInt("NO", mac);
        editor.commit();
    }

    public static int getAutoMac(Context ctx) {
        return getSharedPreferences(ctx).getInt("NO", 0);
    }

    public static void removeAutoMac(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("NO");
        editor.commit();
    }



    public static void setConnectionType(Context ctx, String type) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("TYPE", type);
        editor.commit();
    }

    public static String getConnectionType(Context ctx) {
        return getSharedPreferences(ctx).getString("TYPE", "");
    }

    public static void removeConnectionType(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("TYPE");
        editor.commit();
    }



    public static void setCompanyIds(Context ctx, String cid) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("CID", cid);
        editor.commit();
    }

    public static String getCompanyIds(Context ctx) {
        return getSharedPreferences(ctx).getString("CID", "");
    }

    public static void removeCompanyIds(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove("CID");
        editor.commit();
    }




}