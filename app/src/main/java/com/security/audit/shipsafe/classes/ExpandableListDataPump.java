package com.security.audit.shipsafe.classes;

import android.content.Context;

import com.security.audit.shipsafe.model.ShipSafe;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData(Context context) {
        DatabaseHelperAdmin dbHelper = new DatabaseHelperAdmin(context);

        HashMap<String, List<String>> expandableListDetail = new HashMap<>();

        List<String> cricket = new ArrayList<String>();
        cricket.add("Octobar 26,2017 5:30 PM");
        cricket.add("Octobar 26,2017 5:30 PM");
        cricket.add("Octobar 26,2017 5:30 PM");
        cricket.add("Octobar 26,2017 5:30 PM");
        cricket.add("Octobar 26,2017 5:30 PM");

        List<String> football = new ArrayList<String>();
        football.add("Octobar 26,2017 5:30 PM");
        football.add("Octobar 26,2017 5:30 PM");
        football.add("Octobar 26,2017 5:30 PM");
        football.add("Octobar 26,2017 5:30 PM");
        football.add("Octobar 26,2017 5:30 PM");

        List<String> basketball = new ArrayList<String>();
        basketball.add("Octobar 26,2017 5:30 PM");
        basketball.add("Octobar 26,2017 5:30 PM");
        basketball.add("Octobar 26,2017 5:30 PM");
        basketball.add("Octobar 26,2017 5:30 PM");
        basketball.add("Octobar 26,2017 5:30 PM");

        for (String vassel : dbHelper.getVessels()) {
            expandableListDetail.put(vassel, cricket);
        }
//        expandableListDetail.put("X-Press Hoogly", cricket);
//        expandableListDetail.put("X-Press Mahananda", football);
//        expandableListDetail.put("X-Press Makalu", basketball);


        return expandableListDetail;
    }
}