package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PNationalityDTO implements Serializable {

    String message;
    int status;
    NList data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public NList getData() {
        return data;
    }

    public void setData(NList data) {
        this.data = data;
    }

    public class NList{
        List<NationalityList> nationalityList;

        public List<NationalityList> getNationalityList() {
            return nationalityList;
        }

        public void setNationalityList(List<NationalityList> nationalityList) {
            this.nationalityList = nationalityList;
        }


        public class NationalityList{

            String name;
            int id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }

    }
}
