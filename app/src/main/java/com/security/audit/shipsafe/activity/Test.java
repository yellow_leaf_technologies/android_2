package com.security.audit.shipsafe.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.security.audit.shipsafe.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import id.zelory.compressor.Compressor;
import io.reactivex.schedulers.Schedulers;

public class Test extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener {

    private RoundedImageView ivImage1, ivImage2, ivImage3, ivImage4, ivImage5, ivImage6, ivImage7, ivImage8, ivImage9, ivImage10, ivImage11, ivImage12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ivImage1 = findViewById(R.id.iv_image1);
        ivImage2 = findViewById(R.id.iv_image2);
        ivImage3 = findViewById(R.id.iv_image3);
        ivImage4 = findViewById(R.id.iv_image4);
        ivImage5 = findViewById(R.id.iv_image5);
        ivImage6 = findViewById(R.id.iv_image6);
        ivImage7 = findViewById(R.id.iv_image7);
        ivImage8 = findViewById(R.id.iv_image8);
        ivImage9 = findViewById(R.id.iv_image9);
        ivImage10 = findViewById(R.id.iv_image10);
        ivImage11 = findViewById(R.id.iv_image11);
        ivImage12 = findViewById(R.id.iv_image12);
        findViewById(R.id.tv_single_selection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
            }
        });
        findViewById(R.id.tv_multi_selection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                        .setMaximumDisplayingImages(Integer.MAX_VALUE)
                        .isMultiSelect()
                        .setMinimumMultiSelectCount(1)
                        .setMaximumMultiSelectCount(12)
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
            }
        });
    }
    @Override
    public void onSingleImageSelected(Uri uri) {
        Glide.with(Test.this).load(uri).into(ivImage2);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onMultiImageSelected(List<Uri> uriList) {
        for (int i = 0; i < uriList.size(); i++) {
            if (i >= 12) return;
            RoundedImageView iv;
            switch (i) {
                case 0:
                    iv = ivImage1;
                    /*new Compressor(this)
                            .compressToFileAsFlowable(uriList.get(i))
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Consumer<File>() {
                                @Override
                                public void accept(File file) {
                                    compressedImage = file;
                                }
                            }, new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) {
                                    throwable.printStackTrace();
                                    showError(throwable.getMessage());
                                }
                            });*/
                    break;
                case 1:
                    iv = ivImage2;
                    break;
                case 2:
                    iv = ivImage3;
                    break;
                case 3:
                    iv = ivImage4;
                    break;
                case 4:
                    iv = ivImage5;
                    break;
                case 5:
                    iv = ivImage6;
                    break;

                case 6:
                    iv = ivImage7;
                    break;
                case 7:
                    iv = ivImage8;
                    break;
                case 8:
                    iv = ivImage9;
                    break;
                case 9:
                    iv = ivImage10;
                    break;
                case 10:
                    iv = ivImage11;
                    break;
                case 11:
                default:
                    iv = ivImage12;
            }
            Glide
                    .with(this)
                    .load(uriList.get(i))
                    .apply(new RequestOptions().override(300, 200))
                    .into(iv);
        }
    }
}