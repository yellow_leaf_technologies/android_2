package com.security.audit.shipsafe.api;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetDataService {
    @POST("savePhotoReport")
    Call<ResponseBody> sendPhotoReport(@Query("shipside_id") String shipsideId,
                                       @Query("checklistUUID") String checkListId,
                                       @Query("remark") String remark,
                                       @Query("images") List<String> images);

    @POST("sendObservation")
    Call<ResponseBody> sendObservation(@Query("vesselId") String vesselId,
                                       @Query("vmId") String vmId,
                                       @Query("portPlace") String portPlace,
                                       @Query("reportDate") String reportDate,
                                       @Query("auditorDate") String auditorDate,
                                       @Query("auditorTime") String auditorTime,
                                       @Query("openingHeldAt") String openingHeldAt,
                                       @Query("openingAttendedBy") String openingAttendedBy,
                                       @Query("closingHeldAt") String closingHeldAt,
                                       @Query("closingAttendedBy") String closingAttendedBy,
                                       @Query("checklistUUID") String checkListId
    );
}
