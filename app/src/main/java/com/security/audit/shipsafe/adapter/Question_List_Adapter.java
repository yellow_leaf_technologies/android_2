package com.security.audit.shipsafe.adapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.activity.Questions;
import com.security.audit.shipsafe.activity.VesselInfo;
import com.security.audit.shipsafe.model.Data;
import com.security.audit.shipsafe.model.QuestionName;
import com.security.audit.shipsafe.model.SaveVesselChecklist;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.security.audit.shipsafe.utils.Utils;

import org.jsoup.Jsoup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

public class Question_List_Adapter extends RecyclerView.Adapter<Question_List_Adapter.ItemViewHolder> {
    android.support.v7.app.AlertDialog b = null;
    private Context mContext;
    Questions questions;
    private final List<QuestionName> dto;
    int lastPosition = -1;
    int idCounter = 0;
    RoundedImageView ivImage2;
    LinearLayout dynamicLayout;
    Uri uri;
    List<Uri> uriList;
    int Position;
    DatePickerDialog.OnDateSetListener DATE;
    Calendar myCalendar = Calendar.getInstance();
    //private OnItemClickListener mListener;
    IAdapter adapter;
    DatabaseHelperAdmin databaseHelperAnwser, databaseHelperResponse, databaseHelperquestionID;
    ArrayAdapter<String> AnswerAdapter;
    String checklistUUID = null;
    int check2 = 1;

    int SelectedId = 0;

    public Question_List_Adapter(Context c, List<QuestionName> dto, Questions questions, int SelectedId, String checklistUUID) {
        mContext = c;
        this.dto = dto;
        this.questions = questions;
        this.SelectedId = SelectedId;
        this.checklistUUID = checklistUUID;

    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView typeName;
        Button btnUploadImage;
        EditText DatePick;
        LinearLayout AddedLayout;
        Spinner Answer;
        EditText remark;
        int position = 0;


//        private IAdapter adapter;

        public ItemViewHolder(final View itemView) {
            super(itemView);
            typeName = (TextView) itemView.findViewById(R.id.QuestionWithGuide);
            btnUploadImage = (Button) itemView.findViewById(R.id.btnUpload_Images);
            DatePick = (EditText) itemView.findViewById(R.id.QustionDate);
            AddedLayout = (LinearLayout) itemView.findViewById(R.id.AddedLayout);
            Answer = (Spinner) itemView.findViewById(R.id.Answer);
            remark = (EditText) itemView.findViewById(R.id.Question_Remarks);


            databaseHelperAnwser = new DatabaseHelperAdmin(mContext);
            databaseHelperquestionID = new DatabaseHelperAdmin(mContext);
            databaseHelperResponse = new DatabaseHelperAdmin(mContext);


            ArrayList<String> AnswerArray = new ArrayList<>();
            AnswerArray.add("Select");
            AnswerArray.add("YES");
            AnswerArray.add("NO");
            AnswerArray.add("NA");
            AnswerArray.add("NS");

            AnswerAdapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, AnswerArray);
            AnswerAdapter.setDropDownViewResource(R.layout.nationalityr_item_view);
            Answer.setAdapter(AnswerAdapter);


//            this.adapter=adapter;

            //final View SendView=itemView;
            btnUploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCustomDialog(v, position, AddedLayout);
                }
            });


            DatePick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Questions) mContext).CallDatePicker(DatePick);
                }
            });


        }

    }

    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {

        lastPosition = position;
        idCounter = idCounter + 1;

        final QuestionName item = dto.get(position);
        String typename = item.getQuestion();
        String question = html2text(typename);
        itemViewHolder.position = position;
        itemViewHolder.typeName.setText(question);
        if (item.getRemark() != null && item.getRemark().length() > 0)
            itemViewHolder.remark.setText(item.getRemark());

        itemViewHolder.remark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null && s.length() > 0) {
                    dto.get(position).setRemark(s.toString());
                }
            }
        });

        itemViewHolder.DatePick.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null && s.length() > 0) {
                    dto.get(position).setDate(s.toString());
                }
            }
        });
//
//        if (SelectedId == 1) {
//            itemViewHolder.Answer.setSelection(1);
////            String name = dto.get(position).getQuestion();
////            int questionId = dto.get(position).getId();
////            SaveVesselChecklist data = new SaveVesselChecklist();
////            data.setResponse("YES");
////            data.setChecklistUUID(checklistUUID);
////            data.setDate(itemViewHolder.DatePick.getText().toString());
////            data.setQuestionId(questionId);
////            data.setRemark(itemViewHolder.remark.getText().toString());
////            databaseHelperAnwser.addSaveVesselChecklist(data);
//
//        } else if (SelectedId == 2) {
//            itemViewHolder.Answer.setSelection(2);
//        }

        final int pos = position;
        itemViewHolder.Answer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    //do nothing
                } else {
                    String response = itemViewHolder.Answer.getSelectedItem().toString();
                    item.setSelect(response);
//                    String name = dto.get(pos).getQuestion();
//
//                    int questionId = databaseHelperquestionID.getQuestionId(name);
//                    String date = itemViewHolder.DatePick.getText().toString();
//                    String remark = itemViewHolder.remark.getText().toString();
//                    SaveVesselChecklist data = new SaveVesselChecklist();
//                    data.setResponse(response);
//                    data.setChecklistUUID(checklistUUID);
//                    data.setDate(date);
//                    data.setQuestionId(questionId);
//                    data.setRemark(remark);
//                    databaseHelperAnwser.addSaveVesselChecklist(data);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!TextUtils.isEmpty(item.getDate()))
            itemViewHolder.DatePick.setText(item.getDate());

        if (!TextUtils.isEmpty(item.getSelect())) {
            if (item.getSelect().equals("YES"))
                itemViewHolder.Answer.setSelection(1);
            else if (item.getSelect().equals("NO"))
                itemViewHolder.Answer.setSelection(2);
            else if (item.getSelect().equals("NA"))
                itemViewHolder.Answer.setSelection(3);
            else if (item.getSelect().equals("NS"))
                itemViewHolder.Answer.setSelection(4);

        }
        if (item.getImages() != null && item.getImages().size() > 0) {
            setPhotos(item.getImages(), itemViewHolder.AddedLayout, position, true);
        }
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

   /* public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {0
            return Html.fromHtml(html).toString();     // without library

        }
    }*/


    public void updatePosition(int newValue) {
        SelectedId = newValue;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_item_view, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);


        return itemViewHolder;
    }


    void openCustomDialog(View view, final int position, final LinearLayout AddedLayout) {
        final AlertDialog.Builder customDialog
                = new AlertDialog.Builder(view.getRootView().getContext());
        //customDialog.setTitle("Inspector Signature");

        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.dialog_photo_picker, null);
        // v.setBackgroundColor(Color.WHITE);
        customDialog.setCancelable(false);
        customDialog.setTitle("Add Photo");
        customDialog.setView(v);
        customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        final AlertDialog dialog = customDialog.create();
        dialog.show();  // to show
        final LinearLayout SingleSelection, MultipleSelection;

        SingleSelection = (LinearLayout) v.findViewById(R.id.single_selection_image);
        MultipleSelection = (LinearLayout) v.findViewById(R.id.multiple_selection_image);


        SingleSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof Questions) {
                    ((Questions) mContext).CallSingleSelection(position, AddedLayout);
                    dialog.dismiss();

                }
            }
        });
        MultipleSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof Questions) {
                    ((Questions) mContext).CallMultipleSelection(position, AddedLayout);
                    dialog.dismiss();
                }
            }
        });
    }


    public void setPhotos(List<Uri> uriList, final LinearLayout ItemView, final int position, boolean isReset) {
        for (int i = 0; i < uriList.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RoundedImageView ivImage2;
            final View views = inflater.inflate(R.layout.image_view_item, null);
            ivImage2 = views.findViewById(R.id.DynamicImage);
            final ImageButton btnDel = views.findViewById(R.id.DynamicImageBtn);
            ItemView.addView(views);
            btnDel.setTag(i);
            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Uri> images = dto.get(position).getImages();
                    if (images != null && images.size() > 0) {
                        try {
                            images.remove(Integer.parseInt(btnDel.getTag().toString()));
                        } catch (Exception e) {
                        }
                    }
                    ItemView.removeView(views);
                }
            });
            if (i >= 10) return;
            RoundedImageView iv;
            switch (i) {
                case 0:
                    iv = ivImage2;
                    break;
                default:
                    iv = ivImage2;
            }
            Glide
                    .with(mContext)
                    .load(uriList.get(i))
                    .apply(new RequestOptions().override(300, 200))
                    .into(iv);
        }
        if (!isReset)
            dto.get(position).setImages(uriList);
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return dto.size();
    }


}