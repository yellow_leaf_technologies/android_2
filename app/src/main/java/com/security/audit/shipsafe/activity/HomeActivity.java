package com.security.audit.shipsafe.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;

import java.util.ArrayList;
import java.util.Collections;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    LinearLayout btnCreateChecklist, btnViewObservation, btnEditChecklist;
    TextView FirstName, Username, UserEmail;
    public static Activity Home;
    Context mContext;

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        Home = this;
        mContext = this;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        FirstName = (TextView) headerView.findViewById(R.id.FirstLetter);
        Username = (TextView) headerView.findViewById(R.id.Uname);
        UserEmail = (TextView) headerView.findViewById(R.id.Uemail);
        String first = SaveSharedPreference.getUserName(mContext);
//        String letter=first.substring(0,1);
        //   FirstName.setText(letter);
        Username.setText(SaveSharedPreference.getUserName(mContext));
        UserEmail.setText(SaveSharedPreference.getUserEmail(mContext));
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        btnCreateChecklist = (LinearLayout) findViewById(R.id.btnCreateChecklist);
        btnViewObservation = (LinearLayout) findViewById(R.id.btnViewObservationReport);
        btnEditChecklist = (LinearLayout) findViewById(R.id.btnEditChecklist);

        btnCreateChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, checklist_type.class);
                startActivity(i);
            }
        });
        btnViewObservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, ViewObservation.class);
                startActivity(i);
            }
        });

        btnEditChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, EditChecklist.class);
                startActivity(i);
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile) {
            Intent i = new Intent(HomeActivity.this, ProfileAcivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        removeColor(navigationView);
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    private void removeColor(NavigationView view) {
        for (int i = 0; i < view.getMenu().size(); i++) {
            MenuItem item = view.getMenu().getItem(i);
            item.setChecked(false);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_camera) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent(HomeActivity.this, Is_Internal_Details.class);
            startActivity(i);

        } else if (id == R.id.nav_slideshow) {

            Intent i = new Intent(HomeActivity.this, FinalSubmit.class);
            startActivity(i);

        } else if (id == R.id.nav_manage) {
           /* Intent i=new Intent(HomeActivity.this,Test2.class);
            startActivity(i);*/
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void MakeToast(Context context, String msg) {

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
