package com.security.audit.shipsafe.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.rank.RankAdapter;
import com.security.audit.shipsafe.model.SaveStaffinfo;
import com.security.audit.shipsafe.model.SaveVessel;
import com.security.audit.shipsafe.model.ShipSafe;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.security.audit.shipsafe.utils.ImageSaver;
import com.williamww.silkysignature.views.SignaturePad;

import java.io.File;
import java.util.List;

public class FinalSubmit extends AppCompatActivity {

    ImageView MasterSignature, InspectorSignature;
    Context mContext;
    public SignaturePad mSignaturePad;
    private Button btnSigninrequest, btnFinalize;

    private TextView tvInspector, tvInspectionDate, tvInspectionPort, tvVessel;
    private EditText edRemark;
    private DatabaseHelperAdmin dbHelper;
    private RankAdapter rankAdapter;
    private RecyclerView rvRank;
    private String inspectorSignaturePath;
    private String masterSignaturePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_submit);
        mContext = this;


        btnSigninrequest = findViewById(R.id.btnSigninrequest);
        btnFinalize = findViewById(R.id.btnFinalize);
        MasterSignature = (ImageView) findViewById(R.id.master_signature);
        InspectorSignature = (ImageView) findViewById(R.id.inspector_signature);

        MasterSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCustomDialog(v);
            }
        });


        InspectorSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCustomDialogInspector(v);
            }
        });
        initUI();
        setListener();

        uploadDataUI();
    }

    private void initUI() {
        tvInspector = findViewById(R.id.tvInspector);
        tvInspectionDate = findViewById(R.id.tvInspectionDate);
        tvInspectionPort = findViewById(R.id.tvInspectionPort);
        tvVessel = findViewById(R.id.tvVessel);
        edRemark = findViewById(R.id.edRemark);
        rvRank = findViewById(R.id.rvRank);
        initListRank();
    }

    private void initListRank() {
        tvInspector.setText(SaveSharedPreference.getUserName(mContext));
        rvRank.setHasFixedSize(true);
        rvRank.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rankAdapter = new RankAdapter();
        rvRank.setAdapter(rankAdapter);
    }

    private void updateAdapterRank() {
        List<SaveStaffinfo> saveStaffinfos = dbHelper.getSaveStaffinfoByChecklistUUID(getIntent().getStringExtra("Checklist_UUID"));
        rankAdapter.setItems(saveStaffinfos);
    }

    private void uploadDataUI() {
        dbHelper = new DatabaseHelperAdmin(mContext);
        final int vesselId = getIntent().getIntExtra("V_ID", 0);

        SaveVessel saveVessel = dbHelper.getSaveVesselByID(vesselId);
        if (saveVessel != null) {
            try {
                tvInspectionDate.setText(String.valueOf(saveVessel.getStartDate()));
                tvInspectionPort.setText(dbHelper.getPortTitle(saveVessel.getPortFrom()));
                tvVessel.setText(dbHelper.getVesselName(saveVessel.getVesselId()));
                updateAdapterRank();
            } catch (Exception e) {

            }
        }
    }


    private void setListener() {
        btnSigninrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.deleteCheckListDataById(getIntent().getStringExtra("Checklist_UUID"));
                goHome();
            }
        });
        btnFinalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dbHelper.addShipSafe(new ShipSafe(getIntent().getStringExtra("Checklist_UUID"), getIntent().getIntExtra("V_ID", 0), edRemark.getText().toString(), masterSignaturePath, inspectorSignaturePath));
                    goHome();
                } catch (Exception e) {
                }
            }
        });

    }


    private void goHome() {

        Intent i2 = new Intent(mContext, HomeActivity.class);
        startActivity(i2);
        finish();
    }


    void openCustomDialogInspector(View view) {
        final android.app.AlertDialog.Builder customDialog
                = new android.app.AlertDialog.Builder(view.getRootView().getContext());
        customDialog.setTitle("Inspector Signature");
        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.dialog_popup_signature_inspector, null);
        // v.setBackgroundColor(Color.WHITE);
        final SignaturePad mSignaturePad;

        mSignaturePad = (SignaturePad) v.findViewById(R.id.Signature_Inspector);
        final TextView ClearMaster = (TextView) v.findViewById(R.id.Clear_Inspector);

        ClearMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignaturePad.clear();
            }
        });


        customDialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        customDialog.setNegativeButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
                InspectorSignature.setImageBitmap(signatureBitmap);
                File file = new ImageSaver(getApplicationContext()).
                        setFileName(String.format("Signature_%d.png", System.currentTimeMillis())).
                        setDirectoryName("SignaturePad").
                        save(signatureBitmap);
                if (file != null) {
                    scanMediaFile(file);
                    inspectorSignaturePath = file.getAbsolutePath();
                    Toast.makeText(mContext, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
                    inspectorSignaturePath = null;
                    Toast.makeText(mContext, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }

            }
        });
        customDialog.setView(v);
        customDialog.show();
    }


    void openCustomDialog(View view) {
        final android.app.AlertDialog.Builder customDialog
                = new android.app.AlertDialog.Builder(view.getRootView().getContext());
        customDialog.setTitle("Master Signature");
        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.dialog_popup_signature, null);
        // v.setBackgroundColor(Color.WHITE);
        final SignaturePad mSignaturePad;

        mSignaturePad = (SignaturePad) v.findViewById(R.id.Signature_Master);
        final TextView ClearMaster = (TextView) v.findViewById(R.id.Clear_Master);

        ClearMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignaturePad.clear();
            }
        });


        customDialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        customDialog.setNegativeButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
                MasterSignature.setImageBitmap(signatureBitmap);
                File file = new ImageSaver(getApplicationContext()).
                        setFileName(String.format("Signature_%d.png", System.currentTimeMillis())).
                        setDirectoryName("SignaturePad").
                        save(signatureBitmap);
                if (file != null) {
                    scanMediaFile(file);
                    masterSignaturePath = file.getAbsolutePath();
                    Toast.makeText(mContext, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
                    masterSignaturePath = null;
                    Toast.makeText(mContext, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }
            }
        });
        customDialog.setView(v);
        customDialog.show();
    }


    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        mContext.sendBroadcast(mediaScanIntent);
    }

}
