package com.security.audit.shipsafe.api.view;

import com.security.audit.shipsafe.api.errors.WebError;

import java.util.ArrayList;

public interface ViewPresenter<T> {

    void onSuccess(T data);

    void onSuccess(ArrayList<T> data);

    void onAccountError(WebError error);
}
