package com.security.audit.shipsafe.adapter;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.activity.Questions;
import com.security.audit.shipsafe.model.CCategoryDetails;
import com.security.audit.shipsafe.model.ChecklistCategoryDTO;

import java.util.List;

public class AllTypeListAdapter extends RecyclerView.Adapter<AllTypeListAdapter.ItemViewHolder> {
    android.support.v7.app.AlertDialog b = null;
    private Context mContext;
    private final List<CCategoryDetails> dto;
    int lastPosition = -1;
    String checklistUUID=null;

    public AllTypeListAdapter(Context c, List<CCategoryDetails> dto,String checklistUUID) {
        mContext = c;
        this.dto = dto;
        this.checklistUUID=checklistUUID;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView  catName;

        public ItemViewHolder(View itemView) {
            super(itemView);
            catName = (TextView) itemView.findViewById(R.id.TypeName);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
             Intent i=new Intent(mContext, Questions.class);
            i.putExtra("CAT_TYPE", (Parcelable) dto.get(getAdapterPosition()));
            i.putExtra("C_ID",checklistUUID);
            mContext.startActivity(i);
        }
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {

        lastPosition = position;
      /*  int[] androidColors = mContext.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        itemViewHolder.circle.setColorFilter(randomAndroidColor);*/

        /*

             String customer_name = dto.get(position).getFirst_name()+" "+dto.get(position).getLast_name();
        String mobile_no = dto.get(position).getMobile();
        String email_address = dto.get(position).getEmail();

        String firstnameletter=dto.get(position).getDisplayname();
        String one=  firstnameletter.substring(0,1).toUpperCase();
        itemViewHolder.firstletter.setText(one);
        itemViewHolder.CustomerName.setText(customer_name);
        itemViewHolder.mobilenumber.setText(mobile_no);*/
        String catname=dto.get(position).getName();



        itemViewHolder.catName.setText(catname);
        itemViewHolder.catName.setSelected(true);


    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_view, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public int getItemCount() {
        return dto.size();
    }

}
