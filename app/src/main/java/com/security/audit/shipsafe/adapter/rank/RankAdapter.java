package com.security.audit.shipsafe.adapter.rank;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.core.BaseRecyclerViewAdapter;
import com.security.audit.shipsafe.adapter.core.BaseViewHolder;
import com.security.audit.shipsafe.adapter.core.BaseViewWrapper;
import com.security.audit.shipsafe.model.SaveStaffinfo;

import io.reactivex.annotations.NonNull;

public class RankAdapter extends BaseRecyclerViewAdapter<SaveStaffinfo, BaseViewHolder> {

    @Override
    protected BaseViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.view_holder_rank, parent, false);
        return new RankViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewWrapper<BaseViewHolder> wrapper, int position) {
        RankViewHolder holder = (RankViewHolder) wrapper.getView();
        SaveStaffinfo item = (SaveStaffinfo) getItems().get(position);
        if (item != null) {
            holder.bind(item, position);
        }
    }

}
