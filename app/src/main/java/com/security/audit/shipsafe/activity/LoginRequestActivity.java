package com.security.audit.shipsafe.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.extra.UtilsDialog;
import com.security.audit.shipsafe.model.AppLogin;
import com.security.audit.shipsafe.model.PChecklistTypeDTO;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;
import com.security.audit.shipsafe.sigleton.AppSingleton;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.security.audit.shipsafe.web_services.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginRequestActivity extends Activity {
    TextView logiText;
    EditText et_RequestCode;
    Button btnSignUp;
    ImageView btnlinkpage;
    TextView t1, t2, t3;
    private static final String[] COUNTRIES = new String[]{"Company Name",
            "France", "Italy", "Germany", "Spain"};
    private Spinner mySpinner;
    private Typeface myFont;
    ProgressDialog progressDialog;
    DatabaseHelperAdmin databaseHelperAdmin, databaseHelper, databaseHelper2, databaseHelperAdmin3, databaseHelperAdmin4;
    Context mContext;
    Dialog progress;
    Typeface face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_request);
        databaseHelperAdmin = new DatabaseHelperAdmin(this);
        databaseHelper = new DatabaseHelperAdmin(this);
        databaseHelper2 = new DatabaseHelperAdmin(this);
        mContext = this;
        databaseHelperAdmin3 = new DatabaseHelperAdmin(mContext);
        databaseHelperAdmin4 = new DatabaseHelperAdmin(mContext);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Please Wait..");
        t1 = (TextView) findViewById(R.id.t1);
        t2 = (TextView) findViewById(R.id.t2);
        t3 = (TextView) findViewById(R.id.t3);

        logiText = (TextView) findViewById(R.id.loginrequesttext);
        et_RequestCode = (EditText) findViewById(R.id.requestcode);
        btnSignUp = (Button) findViewById(R.id.btnSigninrequest);
        btnlinkpage = (ImageView) findViewById(R.id.linkwebrequest);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "railway_semibold.ttf");
        logiText.setTypeface(custom_font);
        t1.setTypeface(custom_font);
        t2.setTypeface(custom_font);
        t3.setTypeface(custom_font);
        Typeface custom = Typeface.createFromAsset(getAssets(), "rale_regular.ttf");
        et_RequestCode.setTypeface(custom);
        Typeface remember = Typeface.createFromAsset(getAssets(), "cen_regular.ttf");
        btnSignUp.setTypeface(remember);
        btnlinkpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.topcubit.com")));

            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = et_RequestCode.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    et_RequestCode.setError("Please Enter Request Code");
                    et_RequestCode.requestFocus();
                    return;
                }
                String mac = getMacAddr();
                SaveSharedPreference.setMacId(mContext,mac);

                fetchAppPassword( et_RequestCode.getText().toString().trim());


            }
        });

         face = Typeface.createFromAsset(getAssets(), "rale_regular.ttf");


    }
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }


    private void VerifyAppPassword() {

        if (databaseHelper2.checkAppPassword(et_RequestCode.getText().toString().trim())) {
            String code = et_RequestCode.getText().toString().trim();
            SaveSharedPreference.setRequestCode(mContext, code);

            Intent i2 = new Intent(mContext, Login.class);
            startActivity(i2);
            finish();
        } else {
            Toast.makeText(mContext, "Wrong! Password", Toast.LENGTH_SHORT).show();
        }
    }



    private class MyArrayAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MyArrayAdapter(LoginRequestActivity con) {
            // TODO Auto-generated constructor stub
            mInflater = LayoutInflater.from(con);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return COUNTRIES.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.my_spinner_style, null);
                holder = new ListContent();

                holder.name = (TextView) v.findViewById(R.id.textView1);

                v.setTag(holder);
            } else {

                holder = (ListContent) v.getTag();
            }

            holder.name.setTypeface(myFont);
            holder.name.setText("" + COUNTRIES[position]);

            return v;
        }

    }


    private void fetchAppPassword(final String password) {
        showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_APP_PASSWORD, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //      hideDialog();
                dismissProgress();
                String json = response.toString();


                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        jsonObject = new JSONObject(response);
                        AppLogin appLogin;
                        appLogin = gson.fromJson(jsonObject.toString(),AppLogin.class);
                        ArrayList<AppLogin> pass=new ArrayList<>();
                        pass.add(appLogin);
                        String companyid=appLogin.getData().getCompanyId();
                        String code=appLogin.getData().getPassword();
                        SaveSharedPreference.setCompanyIds(mContext,companyid);
                        SaveSharedPreference.setRequestCode(mContext,code);
                        int status = appLogin.getStatus();

                        if (status == 0) {
                            String Message = appLogin.getMessage();
                            MakeToast(mContext, Message);
                        } else if (status == 1) {
                            if (pass != null && pass.size() > 0) {
                                setAppPass(pass);

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("password", password);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }

    public void setAppPass(final ArrayList<AppLogin> itemlist) {
        databaseHelperAdmin.add_app_password(itemlist);
        Intent i=new Intent(mContext,Login.class);
        startActivity(i);
    }


    static class ListContent {

        TextView name;

    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }



    private void showProgress() {
        if (progress == null) {
            progress = UtilsDialog.processDialog(mContext, "Loading...", true, face);
            progress.show();
        } else {
            progress.show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void dismissProgress() {
        if (!LoginRequestActivity.this.isDestroyed() && progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}