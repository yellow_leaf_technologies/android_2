package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CCategoryDetails implements Parcelable {
        int type__id;    //checklist id
        String type__name;
        String order;
        int id;    // category id
        String name;

    public CCategoryDetails(Parcel in) {
        type__id = in.readInt();
        type__name = in.readString();
        order = in.readString();
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<CCategoryDetails> CREATOR = new Creator<CCategoryDetails>() {
        @Override
        public CCategoryDetails createFromParcel(Parcel in) {
            return new CCategoryDetails(in);
        }

        @Override
        public CCategoryDetails[] newArray(int size) {
            return new CCategoryDetails[size];
        }
    };

    public CCategoryDetails() {

    }

    public int getType__id() {
            return type__id;
        }

        public void setType__id(int type__id) {
            this.type__id = type__id;
        }

        public String getType__name() {
            return type__name;
        }

        public void setType__name(String type__name) {
            this.type__name = type__name;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type__id);
        dest.writeString(type__name);
        dest.writeString(order);
        dest.writeInt(id);
        dest.writeString(name);
    }
}
