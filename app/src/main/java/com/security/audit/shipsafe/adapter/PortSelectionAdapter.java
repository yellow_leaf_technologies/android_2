package com.security.audit.shipsafe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.model.PortListDeatils;

import java.util.ArrayList;
import java.util.List;

public class PortSelectionAdapter extends RecyclerView.Adapter<PortSelectionAdapter.MyViewHolder>
        implements Filterable {
    private Context mContext;
    private List<PortListDeatils> contactList;
    private List<PortListDeatils> contactListFiltered;

    private CallbackInterface mCallback;

    public interface CallbackInterface {
        void onHandleSelection(int position, PortListDeatils name);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ConName;

        public MyViewHolder(View view) {
            super(view);
            ConName = view.findViewById(R.id.CPTo);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    // listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public PortSelectionAdapter(Context context, List<PortListDeatils> Name) {
        this.mContext = context;
        this.contactList = Name;
        this.contactListFiltered = Name;
        try{
            mCallback = (CallbackInterface) mContext;
        }catch(ClassCastException ex){
            //.. should log the error or throw and exception
            Log.e("MyAdapter","Must implement the CallbackInterface in the Activity", ex);
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_countryportto, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PortListDeatils contact = contactListFiltered.get(position);
        holder.ConName.setText(contact.getFull_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCallback != null){
                    mCallback.onHandleSelection(position, contactListFiltered.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<PortListDeatils> filteredList = new ArrayList<>();
                    for (PortListDeatils row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCountry__name().toLowerCase().contains(charString.toLowerCase()) || row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<PortListDeatils>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



}