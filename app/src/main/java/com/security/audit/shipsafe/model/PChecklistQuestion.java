package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PChecklistQuestion implements Serializable {


    int status;
    String message;
    CQData data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CQData getData() {
        return data;
    }

    public void setData(CQData data) {
        this.data = data;
    }

    public class CQData {
        List<ChecklistQuestions> checklistQuestions;

        public List<ChecklistQuestions> getChecklistQuestions() {
            return checklistQuestions;
        }

        public void setChecklistQuestions(List<ChecklistQuestions> checklistQuestions) {
            this.checklistQuestions = checklistQuestions;
        }


        public class ChecklistQuestions {


            String info;
            int category__type__id;
            String category__type__name;
            String question;
            int category__id;
            String order;
            String category__name;
            int id;

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public int getCategory__type__id() {
                return category__type__id;
            }

            public void setCategory__type__id(int category__type__id) {
                this.category__type__id = category__type__id;
            }

            public String getCategory__type__name() {
                return category__type__name;
            }

            public void setCategory__type__name(String category__type__name) {
                this.category__type__name = category__type__name;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public int getCategory__id() {
                return category__id;
            }

            public void setCategory__id(int category__id) {
                this.category__id = category__id;
            }

            public String getOrder() {
                return order;
            }

            public void setOrder(String order) {
                this.order = order;
            }

            public String getCategory__name() {
                return category__name;
            }

            public void setCategory__name(String category__name) {
                this.category__name = category__name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }


}
