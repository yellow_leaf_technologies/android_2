package com.security.audit.shipsafe.api.presenter;

import android.content.Context;
import android.net.Uri;

import com.security.audit.shipsafe.api.core.ApiUtils;
import com.security.audit.shipsafe.api.errors.WebError;
import com.security.audit.shipsafe.api.presenter.core.BasePresenter;
import com.security.audit.shipsafe.api.view.ViewPresenter;
import com.security.audit.shipsafe.model.ShipSideCheckList;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoReportPresenter extends BasePresenter {

    public PhotoReportPresenter(Context context, ViewPresenter view) {
        super(context, view);
    }

    public void sendPhotoReport(ShipSideCheckList item) {
        List<String> images = new ArrayList<>();
        if (item.getSetImages() != null && item.getSetImages().size() > 0) {
            for (Uri uri : item.getSetImages()) {
                images.add(uri.getPath());
            }
            item.setImages(images);
        }
        ApiUtils.getDataService(getContext())
                .sendPhotoReport(item.getShipsideId(), item.getCheckListId(), item.getRemarks(), images)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        view.onAccountError(new WebError());
                    }
                });
    }

}
