package com.security.audit.shipsafe.activity;

import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.Question_List_Adapter;
import com.security.audit.shipsafe.model.CCategoryDetails;
import com.security.audit.shipsafe.model.ChecklistCategoryDTO;
import com.security.audit.shipsafe.model.QuestionCheckList;
import com.security.audit.shipsafe.model.QuestionName;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import id.zelory.compressor.Compressor;

public class Questions extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener {
    Context mContext;
    RecyclerView customerRecycler;
    List<QuestionName> Dto = null;
    CCategoryDetails dto = null;
    DatabaseHelperAdmin databaseHelperAdmin;
    Questions question;
    int idCounter = 0;
    RoundedImageView ivImage2;
    Question_List_Adapter adapter;
    int Position = 0;
    LinearLayout ItemView = null;
    DatePickerDialog.OnDateSetListener DatePicker, TBMeethingDatepicker;
    Calendar myCalendar = Calendar.getInstance();
    boolean clearDate = false;

    LinearLayout btnAllYes, btnAllNo, btnSave;

    int SelectedId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_questions);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        question = this;
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        customerRecycler = (RecyclerView) findViewById(R.id.QuestionRecycler);

        btnAllYes = (LinearLayout) findViewById(R.id.btnAllYes);
        btnAllNo = (LinearLayout) findViewById(R.id.btnAllNo);
        btnSave = (LinearLayout) findViewById(R.id.btnSave);

        Intent intent = getIntent();
        dto = intent.getParcelableExtra("CAT_TYPE");
        int category_id = dto.getId();
        int checklist_type = dto.getType__id();
        String name = dto.getName();
        getSupportActionBar().setTitle(name);

        final String checklistUUID = getIntent().getStringExtra("C_ID");
        Dto = databaseHelperAdmin.getQuestionFromCategory(String.valueOf(category_id));
        for (QuestionName item : Dto) {
            QuestionCheckList questionCheckList = databaseHelperAdmin.getQuestionInfoCheckListByQuestionID(String.valueOf(item.getId()), checklistUUID);
            if (questionCheckList != null) {
                item.setQuestionCheckListId(questionCheckList.getId());
                item.setRemark(questionCheckList.getRemark());
                item.setSelect(questionCheckList.getSelect());
                item.setDate(questionCheckList.getDate());
                if (item.getImages() == null) {
                    List<String> images = databaseHelperAdmin.getPhotoReportCheckListImage(String.valueOf(questionCheckList.getId()), getIntent().getStringExtra("C_ID"));
                    if (images != null && images.size() > 0) {
                        for (String image : images) {
                            item.setImage(Uri.parse(image));
                        }
                    }
                }
            }

        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        customerRecycler.setLayoutManager(layoutManager);
        customerRecycler.setHasFixedSize(true);
        customerRecycler.setItemViewCacheSize(Dto.size());
        adapter = new Question_List_Adapter(mContext, Dto, question, SelectedId, checklistUUID);
        customerRecycler.setAdapter(adapter);


        btnAllYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (QuestionName item : Dto) {
                    item.setSelect("YES");
                }
                adapter.notifyItemRangeChanged(0, Dto.size());
            }
        });
        btnAllNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (QuestionName item : Dto) {
                    item.setSelect("NO");
                }
                adapter.notifyItemRangeChanged(0, Dto.size());
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (QuestionName item : Dto) {
                    if ((item.getRemark() != null && item.getRemark().length() > 0) || (item.getSelect() != null && item.getSelect().length() > 0)
                            || (item.getDate() != null && item.getDate().length() > 0) || (item.getImages() != null && item.getImages().size() > 0)) {
                        String questionCheckListId = item.getQuestionCheckListId() == null ? UUID.randomUUID().toString() : item.getQuestionCheckListId();
                        databaseHelperAdmin.addQuestionCheckList(
                                new QuestionCheckList(questionCheckListId, String.valueOf(item.getId()), checklistUUID, item.getRemark(), item.getSelect(), item.getDate()));
                        databaseHelperAdmin.removePhotoReportCheckListImage(String.valueOf(questionCheckListId), getIntent().getStringExtra("C_ID"));
                        if (item.getImages() != null && item.getImages().size() > 0) {
                            for (Uri image : item.getImages()) {
                                databaseHelperAdmin.addPhotoReportCheckListImage(questionCheckListId, getIntent().getStringExtra("C_ID"), image);
                            }
                        }
                    }
                }
                Questions.this.onBackPressed();
            }
        });

    /* customerRecycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
         @Override
         public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
             String title1 = ((TextView) customerRecycler.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.QuestionWithGuide)).getText().toString();
             Toast.makeText(getApplicationContext(), title1, Toast.LENGTH_SHORT).show();
             return false;
         }

         @Override
         public void onTouchEvent(RecyclerView rv, MotionEvent e) {

         }

         @Override
         public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

         }
     });*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public void CallSingleSelection(int position, LinearLayout AddedLayout) {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
        ItemView = AddedLayout;
        Position = position;
    }

    public void CallDatePicker(final EditText datepick) {
        DatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                datepick.setText(sdf.format(myCalendar.getTime()));
                clearDate = true;
                datepick.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);
            }

        };


        if (clearDate == true) {
            datepick.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
            datepick.setText("DD/MM/YYYY");
            clearDate = false;
        } else {
            new DatePickerDialog(mContext, DatePicker, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }


    }


    public void CallMultipleSelection(int position, LinearLayout AddedLayout) {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(10)
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
        ItemView = AddedLayout;

        Position = position;

    }

    @Override
    public void onSingleImageSelected(Uri uri) {

        List<Uri> itemList = new ArrayList<>();
        itemList.add(uri);
        adapter.setPhotos(itemList, ItemView, Position, false);


        /*LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


      //  File compressedImage=null;

       *//* File image=new File(uri.getPath());
        try {
            compressedImage = new Compressor(this)
                    .setMaxWidth(640)
                    .setMaxHeight(480)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(image);
        } catch (IOException e) {
            e.printStackTrace();
        }*//*



        final View views = inflater.inflate(R.layout.image_view_item, null);
        ImageView ivImage2 = (RoundedImageView) views.findViewById(R.id.DynamicImage);
        ImageButton btnDel = (ImageButton) views.findViewById(R.id.DynamicImageBtn);

        Glide
                .with(mContext)
                .load(uri)
                .apply(new RequestOptions().override(300, 200))
                .into(ivImage2);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ItemView == null) {
                    return;
                }
                ItemView.removeView(views);
            }
        });

    //    Uri saveUri=Uri.fromFile(new File(compressedImage));

     *//*

        MakeToast(mContext,String.valueOf(stream));*//*


        InputStream iStream = null;
        try {
            iStream = getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] inputData=null;
        try {
          inputData = getBytes(iStream);
            MakeToast(mContext,String.valueOf(Arrays.toString(inputData)));
        } catch (IOException e) {
            e.printStackTrace();
        }


       //BitmapFactory.decodeByteArray( inputData.accImage, 0,currentAccount.accImage.length);





        ItemView.addView(views);

*/
        // adapter.setPhoto(uri,pos,view);


    }


    public byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public byte[] convertImageToByte(Uri uri) {
        byte[] data = null;
        try {
            ContentResolver cr = getBaseContext().getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onMultiImageSelected(List<Uri> uriList) {

        adapter.setPhotos(uriList, ItemView, Position, false);


      /*  for (int i = 0; i < uriList.size(); i++) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final View views = inflater.inflate(R.layout.image_view_item, null);
            ivImage2 = (RoundedImageView) views.findViewById(R.id.DynamicImage);
            ImageButton btnDel = (ImageButton) views.findViewById(R.id.DynamicImageBtn);
            ItemView.addView(views);

            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemView.removeView(views);
                }
            });

            if (i >= 10) return;
            RoundedImageView iv;
            switch (i) {
                case 0:
                    iv = ivImage2;
                    break;
                default:
                    iv = ivImage2;
            }
            Glide
                    .with(mContext)
                    .load(uriList.get(i))
                    .apply(new RequestOptions().override(300, 200))
                    .into(iv);
        }*/


    }
}