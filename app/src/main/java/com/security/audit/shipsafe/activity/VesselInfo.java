package com.security.audit.shipsafe.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.extra.NoDefaultSpinner;
import com.security.audit.shipsafe.extra.RandomString;
import com.security.audit.shipsafe.model.ChecklistTypeDTO;
import com.security.audit.shipsafe.model.ChecklistTypeDetails;
import com.security.audit.shipsafe.model.PortName;
import com.security.audit.shipsafe.model.SaveVessel;
import com.security.audit.shipsafe.model.SaveVesselInfo;
import com.security.audit.shipsafe.model.SaveVesselInfoDTO;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class VesselInfo extends AppCompatActivity {
    TextView InitailDay, InitialDate, EndDay, EndDate;
    LinearLayout btnInitailDate, btnEndDate;
    DatePickerDialog.OnDateSetListener DATE, DATE2, DATE3, DATE4;
    Calendar myCalendar = Calendar.getInstance();
    Context mContext;
    SearchableSpinner VesselNameSpinner;
    ArrayAdapter<String> adapter;
    ArrayList<String> vesselarray;
    LinearLayout btnInspectionPortFrom, btnInspectionPortTo;
    DatabaseHelperAdmin databaseHelperVessel, databaseHelperToPort, databaseHelperFromPort, databaseHelperVesselId, databaseHelperSaveVesselInfo;
    TextView InitailDateText, EndDateText;
    PortName dto = null;
    ChecklistTypeDetails ChecklistDto = null;
    Button btnSaveVesselInfo;
    int id;

    public static TextView PortFrom, PortTo;

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_vessesl_info);
        getSupportActionBar().setTitle("Vessel Info");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        VesselNameSpinner = (SearchableSpinner) findViewById(R.id.VesselNameSpinner);
        InitailDateText = (TextView) findViewById(R.id.InitialDateText);
        EndDateText = (TextView) findViewById(R.id.EndDateText);
        btnInspectionPortFrom = (LinearLayout) findViewById(R.id.btnInspectionPortFrom);
        btnInspectionPortTo = (LinearLayout) findViewById(R.id.btnInspectionPortTo);

        PortFrom = (TextView) findViewById(R.id.PortFrom);
        PortTo = (TextView) findViewById(R.id.PortTo);

        btnInitailDate = (LinearLayout) findViewById(R.id.btnInitialDate);
        btnEndDate = (LinearLayout) findViewById(R.id.btnEndDate);

        databaseHelperVessel = new DatabaseHelperAdmin(mContext);
        databaseHelperFromPort = new DatabaseHelperAdmin(mContext);
        databaseHelperToPort = new DatabaseHelperAdmin(mContext);
        databaseHelperSaveVesselInfo = new DatabaseHelperAdmin(mContext);
        databaseHelperVesselId = new DatabaseHelperAdmin(mContext);


        vesselarray = databaseHelperVessel.getVessels();

        adapter = new ArrayAdapter<String>(mContext, R.layout.rank_spinner_item_view, vesselarray);
        adapter.setDropDownViewResource(R.layout.rank_spinner_item_view);
        VesselNameSpinner.setAdapter(adapter);
        VesselNameSpinner.setTitle("Select Vessel");
        VesselNameSpinner.setPositiveButton("OK");

        VesselNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        DATE = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();
            }

        };

        btnInitailDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(VesselInfo.this, DATE, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        DATE2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        btnEndDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(VesselInfo.this, DATE2, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnInspectionPortFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, CountryFrom.class);
                startActivity(i);

            }
        });
        btnInspectionPortTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, CountryTo.class);
                startActivity(i);

            }
        });

        btnSaveVesselInfo = findViewById(R.id.btnSaveVesselInfo);


        final Intent intent = getIntent();
        ChecklistDto = intent.getParcelableExtra("TypeObject");
        id = ChecklistDto.getId();

        btnSaveVesselInfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {


                if (InitailDateText.getText().equals("Select Date")) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the initial date");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else if (EndDateText.getText().equals("Select Date")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the end date");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (PortFrom.getText().equals("Select Port")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the inspection port from");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else if (PortTo.getText().equals("Select Port")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the inspection port to");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {

                    int vesselId;
                    String startDate;
                    String endDate;
                    int portFrom;
                    int portTo;
                    String checklistUUID;
                    int checklistType;


                    String VesselName = VesselNameSpinner.getSelectedItem().toString();
                    String InspectionPortTo = PortTo.getText().toString();
                    String InspectionPortFrom = PortFrom.getText().toString();
                    String InitialDate = InitailDateText.getText().toString();
                    String EndDate = EndDateText.getText().toString();

                    vesselId = databaseHelperVesselId.getVesselId(VesselName);
                    startDate = InitialDate;
                    endDate = EndDate;
                    portFrom = databaseHelperFromPort.getPortId(InspectionPortFrom);
                    portTo = databaseHelperToPort.getPortId(InspectionPortTo);
                    int ChecklistTypeId = id;
                    checklistType = id;

                    String easy = RandomString.digits + "ACEFGHJKLMNPQRUVWXY123456789";
                    RandomString str1 = new RandomString(8, new SecureRandom(), easy);
                    RandomString str2 = new RandomString(4, new SecureRandom(), easy);
                    RandomString str3 = new RandomString(4, new SecureRandom(), easy);
                    RandomString str4 = new RandomString(4, new SecureRandom(), easy);
                    RandomString str5 = new RandomString(12, new SecureRandom(), easy);

                    String str = str1.nextString() + "-" + str2.nextString() + "-" + str3.nextString() + "-" + str4.nextString() + "-" + str5.nextString();
                    checklistUUID = str;

                    SaveVessel vessel = new SaveVessel();

                    vessel.setVesselId(vesselId);
                    vessel.setStartDate(startDate);
                    vessel.setEndDate(endDate);
                    vessel.setPortFrom(portFrom);
                    vessel.setPortTo(portTo);
                    vessel.setChecklistUUID(checklistUUID);
                    vessel.setChecklistType(checklistType);
                    vessel.setStatus("Not Synced");

                    databaseHelperSaveVesselInfo.addSaveVesselInfo(vessel);
                    Intent i2 = new Intent(mContext, Staff_Information.class);
                    i2.putExtra("MAC_U_ID", checklistUUID);
                    i2.putExtra("VESSEL_ID", vesselId);
                    i2.putExtra("CID", ChecklistTypeId);
                    startActivity(i2);

                }


            }
        });


    }

    public void autono() {
        if (SaveSharedPreference.getAutoMac(mContext) == 0) {

            int autono = 1;
            SaveSharedPreference.setAutoMac(mContext, autono);
            // fileno.setText(autono, TextView.BufferType.EDITABLE);
        } else if (SaveSharedPreference.getAutoMac(mContext) != 0) {
            //int autono=SaveSharedPreference.getFILENO(getActivity());
            int change = SaveSharedPreference.getAutoMac(mContext);
            change = change + 1;
            SaveSharedPreference.setAutoMac(mContext, change);
        }
    }

    private void updateLabel1() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        InitailDateText.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        EndDateText.setText(sdf.format(myCalendar.getTime()));
    }


    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_Vessel_Info:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
