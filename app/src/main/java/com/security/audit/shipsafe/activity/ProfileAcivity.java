package com.security.audit.shipsafe.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.api.errors.WebError;
import com.security.audit.shipsafe.api.presenter.ObservationPresenter;
import com.security.audit.shipsafe.api.presenter.PhotoReportPresenter;
import com.security.audit.shipsafe.api.view.ViewModelPresenter;
import com.security.audit.shipsafe.model.ObservationInfoDTO;
import com.security.audit.shipsafe.model.SaveObservationDTO;
import com.security.audit.shipsafe.model.SaveStaffinfo;
import com.security.audit.shipsafe.model.SaveVessel;
import com.security.audit.shipsafe.model.SaveVesselChecklist;
import com.security.audit.shipsafe.model.ShipSideCheckList;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;
import com.security.audit.shipsafe.sigleton.AppSingleton;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.security.audit.shipsafe.web_services.WebServiceCall;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileAcivity extends AppCompatActivity implements ViewModelPresenter {
    Context mContext;
    LinearLayout btnLogout;
    TextView ShowUserName, ShowUserEmail;
    Button UploadData, RefereshData;
    DatabaseHelperAdmin databaseHelperSaveVesselList, databaseHelperSaveStaffinfo, databaseHelperSaveObservation, databaseHelperObservationInfo, databaseHelperSaveVesselChecklist;

    DatabaseHelperAdmin dbHelber;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_profile);
        mContext = this;
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnLogout = (LinearLayout) findViewById(R.id.Logout);
        ShowUserEmail = (TextView) findViewById(R.id.ShowUserEmail);
        ShowUserName = (TextView) findViewById(R.id.ShowUserName);
        UploadData = (Button) findViewById(R.id.sync_data);
        RefereshData = (Button) findViewById(R.id.refresh_data);
        databaseHelperSaveVesselList = new DatabaseHelperAdmin(mContext);
        ShowUserEmail.setText(SaveSharedPreference.getUserEmail(mContext));
        ShowUserName.setText(SaveSharedPreference.getUserName(mContext));
        databaseHelperSaveStaffinfo = new DatabaseHelperAdmin(mContext);
        databaseHelperSaveObservation = new DatabaseHelperAdmin(mContext);
        databaseHelperObservationInfo = new DatabaseHelperAdmin(mContext);
        databaseHelperSaveVesselChecklist = new DatabaseHelperAdmin(mContext);

        dbHelber = new DatabaseHelperAdmin(mContext);


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                SaveSharedPreference.removeStatus(mContext);
                SaveSharedPreference.removeUser(mContext);
                SaveSharedPreference.removeUserEmail(mContext);
                Intent i = new Intent(mContext, Login.class);
                startActivity(i);
                HomeActivity.Home.finish();
            }
        });


        UploadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<SaveVessel> dataSave = databaseHelperSaveVesselList.getAllInspections();
                for (int i = 0; i < dataSave.size(); i++) {
                    int vesselid, ChecklistType, portTo, portFrom;
                    String sDate, eDate, checklistUUID;
                    vesselid = dataSave.get(i).getVesselId();
                    sDate = dataSave.get(i).getStartDate();
                    eDate = dataSave.get(i).getEndDate();
                    portFrom = dataSave.get(i).getPortFrom();
                    portTo = dataSave.get(i).getPortTo();
                    checklistUUID = dataSave.get(i).getChecklistUUID();
                    ChecklistType = dataSave.get(i).getChecklistType();
                    DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

                    Date date = null;
                    try {
                        date = inputFormat.parse(sDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String startDate = outputFormat.format(date);

                    Date date2 = null;
                    try {
                        date2 = inputFormat.parse(eDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String endDate = outputFormat.format(date2);


                    SaveVesselInfo(vesselid, startDate, endDate, portFrom, portTo, checklistUUID, ChecklistType);

                    ArrayList<SaveObservationDTO> obsDto = databaseHelperSaveObservation.getObservation(checklistUUID);
                    for (int i1 = 0; i1 < obsDto.size(); i1++) {

                        int departmentId = obsDto.get(i1).getDepartmentId();
                        String observation = obsDto.get(i1).getObservation();
                        String tDate = obsDto.get(i1).getTargetDate();
                        String cDate = obsDto.get(i1).getClosedDate();
                        String ChecklistUUID = obsDto.get(i1).getChecklistUUID();
                        String observationType = obsDto.get(i1).getObservationType();
                        String inspectionType = obsDto.get(i1).getInspectionType();


                        DateFormat inputFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                        DateFormat outputFormat1 = new SimpleDateFormat("dd MMM yyyy");

                        Date tdate = null;
                        try {
                            tdate = inputFormat1.parse(tDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String targetDate = outputFormat1.format(date);

                        Date cdate = null;
                        try {
                            cdate = inputFormat1.parse(cDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String closedDate = outputFormat1.format(date2);


                        SaveObservation(departmentId, observation, targetDate, closedDate, ChecklistUUID, observationType, inspectionType);


                        if (observationType.equals("null") && inspectionType.equals("null")) {

                        } else {
                            ArrayList<ObservationInfoDTO> obsinfo = databaseHelperObservationInfo.getOCMeethings(checklistUUID);
                            int vesselId = obsinfo.get(0).getVesselId();
                            int portPlace = obsinfo.get(0).getPortPlace();
                            String rDate = obsinfo.get(0).getReportDate();
                            String reportTime = obsinfo.get(0).getReportTime();
                            String aDate = obsinfo.get(0).getAuditorDate();
                            String auditorTime = obsinfo.get(0).getAuditorTime();
                            String openingHeldAt = obsinfo.get(0).getOpeningHeldAt();
                            String openingAttendedBy = obsinfo.get(0).getOpeningAttendedBy();
                            String closingHeldAt = obsinfo.get(0).getClosingHeldAt();
                            String closingAttendedBy = obsinfo.get(0).getClosingAttendedBy();
                            String checklistUID = obsinfo.get(0).getChecklistUUID();


                            Date rdate = null;
                            try {
                                rdate = inputFormat1.parse(rDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String reportDate = outputFormat1.format(rdate);

                            Date adate = null;
                            try {
                                adate = inputFormat1.parse(aDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String auditorDate = outputFormat1.format(adate);

                            SaveOCMeeting(vesselId, portPlace, reportDate, reportTime, auditorDate, auditorTime, openingHeldAt, openingAttendedBy, closingHeldAt, closingAttendedBy, checklistUID);

                        }
                    }

                    ArrayList<SaveVesselChecklist> vesselChecklists = databaseHelperSaveVesselChecklist.getSaveVesselChecklist(checklistUUID);
                    for (int j = 0; j < vesselChecklists.size(); j++) {
                        int questionId = vesselChecklists.get(j).getQuestionId();
                        String qdate = vesselChecklists.get(j).getDate();
                        String response = vesselChecklists.get(j).getResponse();
                        String checklistUUID1 = vesselChecklists.get(j).getChecklistUUID();
                        String remark = vesselChecklists.get(j).getRemark();


                        DateFormat inputFormat2 = new SimpleDateFormat("dd/MM/yyyy");
                        DateFormat outputFormat2 = new SimpleDateFormat("dd MMM yyyy");
                        Date Qdate = null;
                        try {
                            Qdate = inputFormat2.parse(qdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String QDate = outputFormat2.format(Qdate);

                        SaveOuestions(questionId, QDate, response, checklistUUID1, remark);

                    }


                    ArrayList<SaveStaffinfo> staffinfo = databaseHelperSaveStaffinfo.getSaveStaffinfoByChecklistUUID(checklistUUID);


                    for (int i2 = 0; i2 < staffinfo.size(); i2++) {

                        String rank = staffinfo.get(i2).getRank();
                        String name = staffinfo.get(i2).getName();
                        String nationality = staffinfo.get(i2).getNationality();
                        int yearincompany = staffinfo.get(i2).getYearInCompany();
                        int monthincompany = staffinfo.get(i2).getMonthInCompany();
                        String checklistUID = staffinfo.get(i2).getChecklistUUID();

                        SaveStaffInfo(rank, name, yearincompany, monthincompany, nationality, checklistUID);
                    }
                }
                sendPhotoReportData();
//                sendObservationData();
            }
        });

        RefereshData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }


    private void SaveOCMeeting(final int vesselId, final int portPlace, final String reportDate, final String reportTime, final String auditorDate, final String auditorTime, final String openingHeldAt, final String openingAttendedBy, final String closingHeldAt, final String closingAttendedBy, final String checklistUID) {
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_OBSERVATION_INFO, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("vesselId", String.valueOf(vesselId));
                params.put("portPlace", String.valueOf(portPlace));
                params.put("reportDate", reportDate);
                params.put("reportTime", reportTime);
                params.put("auditorDate", auditorDate);
                params.put("auditorTime", auditorTime);
                params.put("openingHeldAt", openingHeldAt);
                params.put("openingAttendedBy", openingAttendedBy);
                params.put("closingHeldAt", closingHeldAt);
                params.put("closingAttendedBy", closingAttendedBy);
                params.put("checklistUUID", checklistUID);

                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);


    }


    private void SaveOuestions(final int questionId, final String date, final String response, final String checklistUUID, final String remark) {
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_QUESTIONS, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("questionId", String.valueOf(questionId));
                params.put("date", date);
                params.put("response", response);
                params.put("checklistUUID", checklistUUID);
                params.put("remark", remark);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);


    }

    private void endpointPhotoReportData(final ShipSideCheckList item) {
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_PHOTO_REPORT, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("shipside_id", item.getShipsideId());
                params.put("checklistUUID", item.getCheckListId());
                params.put("remark", !TextUtils.isEmpty(item.getRemarks()) ? item.getRemarks() : "");
                params.put("images", "");
//                if (item.getImages() != null && item.getImages().size() > 0)
//                    for (int i = 0; i < item.getImages().size(); i++) {
//                        params.put("images", item.getImages().get(i).toString());
//                    }
//                else
//                    params.put("images", "");
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }

    private void sendPhotoReportData() {
        List<ShipSideCheckList> list = dbHelber.getAllShipsideCheckList();
        for (ShipSideCheckList item : list) {
            new PhotoReportPresenter(getApplicationContext(), this).sendPhotoReport(item);
        }

    }

    private void sendObservationData() {
        List<SaveObservationDTO> list = dbHelber.getAllObservation();
        for (SaveObservationDTO item : list) {
            new ObservationPresenter(getApplicationContext(), this).sendObservation(item);
        }
    }


    private void SaveObservation(final int departmentId, final String observation, final String targetDate, final String closedDate, final String checklistUUID, final String observationType, final String inspectionType) {
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_OBSERVATION, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("departmentId", String.valueOf(departmentId));
                params.put("observation", observation);
                params.put("targetDate", targetDate);
                params.put("closedDate", closedDate);
                params.put("checklistUUID", checklistUUID);
                params.put("insType", observationType);
                params.put("obsType", inspectionType);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);


    }

    private void SaveVesselInfo(final int vesselid, final String startDate, final String endDate, final int portFrom, final int portTo, final String checklistUUID, final int checklistType) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_VESSEL_INFO, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("vesselId", String.valueOf(vesselid));
                params.put("startDate", startDate);
                params.put("endDate", endDate);
                params.put("portFrom", String.valueOf(portFrom));
                params.put("portTo", String.valueOf(portTo));
                params.put("checklistUUID", checklistUUID);
                params.put("checklistType", String.valueOf(checklistType));
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void SaveStaffInfo(final String rank, final String name, final int yearInCompany, final int monthInCompany, final String nationality, final String checklistUUID) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SAVE_STAFF_INFO, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                //  String json = response.toString();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String Message = jsonObject.getString("message");

                    if (status == 0) {
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        MakeToast(mContext, Message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                //   dismissProgress();
                Log.d("Failed", "Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("rank", rank);
                params.put("name", name);
                params.put("yearInCompany", String.valueOf(yearInCompany));
                params.put("monthInCompany", String.valueOf(monthInCompany));
                params.put("nationality", nationality);
                params.put("checklistUUID", checklistUUID);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    public void MakeToast(Context context, String msg) {

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(String success) {

    }

    @Override
    public void onSuccess(List<String> success) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onSuccess(ArrayList data) {

    }

    @Override
    public void onAccountError(WebError error) {

    }
}
