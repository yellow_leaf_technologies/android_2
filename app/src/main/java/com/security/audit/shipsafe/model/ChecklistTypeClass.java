package com.security.audit.shipsafe.model;

import java.io.Serializable;

public class ChecklistTypeClass implements Serializable {
    boolean is_internal;
    int id;
    String name;

    public boolean isIs_internal() {
        return is_internal;
    }

    public void setIs_internal(boolean is_internal) {
        this.is_internal = is_internal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
