package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class ObservationDepartmentDTO implements Serializable {

    int status;
    String message;
    OData data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OData getData() {
        return data;
    }

    public void setData(OData data) {
        this.data = data;
    }

    public class OData {

        List<ObservationDepartment> observationDepartment;

        public List<ObservationDepartment> getObservationDepartment() {
            return observationDepartment;
        }

        public void setObservationDepartment(List<ObservationDepartment> observationDepartment) {
            this.observationDepartment = observationDepartment;
        }

        public class ObservationDepartment {

            int checklist_type__id;
            String checklist_type__name;
            int id;
            String name;

            public int getChecklist_type__id() {
                return checklist_type__id;
            }

            public void setChecklist_type__id(int checklist_type__id) {
                this.checklist_type__id = checklist_type__id;
            }

            public String getChecklist_type__name() {
                return checklist_type__name;
            }

            public void setChecklist_type__name(String checklist_type__name) {
                this.checklist_type__name = checklist_type__name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


}
