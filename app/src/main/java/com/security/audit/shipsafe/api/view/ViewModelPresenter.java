package com.security.audit.shipsafe.api.view;

import java.util.List;

public interface ViewModelPresenter extends ViewPresenter {
    void onSuccess(String success);

    void onSuccess(List<String> success);
}
