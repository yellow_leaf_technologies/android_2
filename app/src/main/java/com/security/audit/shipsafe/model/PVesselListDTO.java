package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PVesselListDTO implements Serializable {
    int status;
    String message;
    Datum data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

  public   class Datum {
        List<VesselList> vesselList;

        public List<VesselList> getVesselList() {
            return vesselList;
        }

        public void setVesselList(List<VesselList> vesselList) {
            this.vesselList = vesselList;
        }


      public class VesselList {
          String username;
          String email;
          int vmid;
          int id;
          String name;

          public String getUsername() {
              return username;
          }

          public void setUsername(String username) {
              this.username = username;
          }

          public String getEmail() {
              return email;
          }

          public void setEmail(String email) {
              this.email = email;
          }

          public int getVmid() {
              return vmid;
          }

          public void setVmid(int vmid) {
              this.vmid = vmid;
          }

          public int getId() {
              return id;
          }

          public void setId(int id) {
              this.id = id;
          }

          public String getName() {
              return name;
          }

          public void setName(String name) {
              this.name = name;
          }
      }

    }





}
