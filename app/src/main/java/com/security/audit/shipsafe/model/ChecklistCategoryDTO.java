package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ChecklistCategoryDTO implements Parcelable {
    int ID;
    String categoryName;
    String createdAt;
    String deletedAt;
    int companyId;
    int types;

    public ChecklistCategoryDTO() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getTypes() {
        return types;
    }

    public void setTypes(int types) {
        this.types = types;
    }

    public static Creator<ChecklistCategoryDTO> getCREATOR() {
        return CREATOR;
    }

    public ChecklistCategoryDTO(Parcel in) {
        ID = in.readInt();
        categoryName = in.readString();
        createdAt = in.readString();
        deletedAt = in.readString();
        companyId = in.readInt();
        types = in.readInt();
    }

    public static final Creator<ChecklistCategoryDTO> CREATOR = new Creator<ChecklistCategoryDTO>() {
        @Override
        public ChecklistCategoryDTO createFromParcel(Parcel in) {
            return new ChecklistCategoryDTO(in);
        }

        @Override
        public ChecklistCategoryDTO[] newArray(int size) {
            return new ChecklistCategoryDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(categoryName);
        dest.writeString(createdAt);
        dest.writeString(deletedAt);
        dest.writeInt(companyId);
        dest.writeInt(types);
    }
}
