package com.security.audit.shipsafe.api;

import android.content.Context;

import com.security.audit.shipsafe.api.core.ApiUtils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance(Context context) {
        if (retrofit == null) {
            retrofit =
                    new Retrofit.Builder()
                            .baseUrl(ApiUtils.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(new OkHttpClientModule().okHttpClient(context))
                            .build();
        }
        return retrofit;
    }
}