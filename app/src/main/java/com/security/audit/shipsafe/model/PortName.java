package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PortName implements Parcelable {
    int ID;
    String locationCode;
    int countryID;
    String portCode;
    String portName;
    String createdAt;
    String updatedAt;
    String updatedBy;

    public PortName() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public static Creator<PortName> getCREATOR() {
        return CREATOR;
    }

    public PortName(Parcel in) {
        ID = in.readInt();
        locationCode = in.readString();
        countryID = in.readInt();
        portCode = in.readString();
        portName = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        updatedBy = in.readString();
    }

    public static final Creator<PortName> CREATOR = new Creator<PortName>() {
        @Override
        public PortName createFromParcel(Parcel in) {
            return new PortName(in);
        }

        @Override
        public PortName[] newArray(int size) {
            return new PortName[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(locationCode);
        dest.writeInt(countryID);
        dest.writeString(portCode);
        dest.writeString(portName);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(updatedBy);
    }
}
