package com.security.audit.shipsafe.model;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShipSideCheckList {

    transient private String id;
    @SerializedName("shipside_id")
    @Expose
    private String shipsideId;
    @SerializedName("checklistUUID")
    @Expose
    private String checkListId;
    @SerializedName("remark")
    @Expose
    private String remarks;
    transient private List<Uri> setImages;
    @SerializedName("images")
    @Expose
    private List<String> images;

    public ShipSideCheckList() {

    }

    public ShipSideCheckList(String id, String shipsideId, String checkListId, String remarks) {
        this.id = id;
        this.shipsideId = shipsideId;
        this.checkListId = checkListId;
        this.remarks = remarks;
    }

    public ShipSideCheckList(String id, String shipsideId, String checkListId, String remarks, List<Uri> setImages) {
        this.id = id;
        this.shipsideId = shipsideId;
        this.checkListId = checkListId;
        this.remarks = remarks;
        this.setImages = setImages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShipsideId() {
        return shipsideId;
    }

    public void setShipsideId(String shipsideId) {
        this.shipsideId = shipsideId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<Uri> getSetImages() {
        return setImages;
    }

    public void setSetImages(List<Uri> setImages) {
        this.setImages = setImages;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getImages() {
        return images;
    }
}
