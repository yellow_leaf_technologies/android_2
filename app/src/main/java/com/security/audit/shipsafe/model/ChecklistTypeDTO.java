package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ChecklistTypeDTO implements Parcelable {
    int ID;
    String checklistName;
    String createdAt;
    String deletedAt;
    String companyId;

    public ChecklistTypeDTO(Parcel in) {
        ID = in.readInt();
        checklistName = in.readString();
        createdAt = in.readString();
        deletedAt = in.readString();
        companyId = in.readString();
    }

    public static final Creator<ChecklistTypeDTO> CREATOR = new Creator<ChecklistTypeDTO>() {
        @Override
        public ChecklistTypeDTO createFromParcel(Parcel in) {
            return new ChecklistTypeDTO(in);
        }

        @Override
        public ChecklistTypeDTO[] newArray(int size) {
            return new ChecklistTypeDTO[size];
        }
    };

    public ChecklistTypeDTO() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getChecklistName() {
        return checklistName;
    }

    public void setChecklistName(String checklistName) {
        this.checklistName = checklistName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(checklistName);
        dest.writeString(createdAt);
        dest.writeString(deletedAt);
        dest.writeString(companyId);
    }
}
