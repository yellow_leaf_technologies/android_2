package com.security.audit.shipsafe.web_services;

public class URLs {

   // private static final String ROOT_URL = "http://111.93.195.22/";


    private static final String ROOT_URL = "https://shipsafe.topcubit.com/";


    public static final String URL_COMPANY_TABLE = ROOT_URL + "getcompanytable";
    public static final String URL_USER_TABLE = ROOT_URL + "getusertable";
    public static final String URL_VESSEL_LIST = ROOT_URL + "getvessellist";
    public static final String URL_CHECKLIST_TYPE = ROOT_URL + "getChecklistType";
    public static final String URL_CHECKLIST_CATEGORY = ROOT_URL + "getcategory";
    public static final String URL_NATIONALITIES = ROOT_URL + "getnationalities";
    public static final String URL_RANK = ROOT_URL + "designations";
    public static final String URL_COUNTRIES = ROOT_URL + "getcountries";
    public static final String URL_PORTS = ROOT_URL + "getports";
    public static final String URL_COUNTRY_WITH_PORT = ROOT_URL + "getcountrywithports";
    public static final String URL_QUESTIONS = ROOT_URL + "getquestiontable";
    public static final String URL_SHIPSIDE = ROOT_URL + "getshipsides";

}