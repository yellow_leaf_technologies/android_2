package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ChecklistTypeDetails implements Parcelable {
    int id;
    String name;
    String is_internal;

    public ChecklistTypeDetails(Parcel in) {
        id = in.readInt();
        name = in.readString();
        is_internal = in.readString();
    }

    public static final Creator<ChecklistTypeDetails> CREATOR = new Creator<ChecklistTypeDetails>() {
        @Override
        public ChecklistTypeDetails createFromParcel(Parcel in) {
            return new ChecklistTypeDetails(in);
        }

        @Override
        public ChecklistTypeDetails[] newArray(int size) {
            return new ChecklistTypeDetails[size];
        }
    };

    public ChecklistTypeDetails() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_internal() {
        return is_internal;
    }

    public void setIs_internal(String is_internal) {
        this.is_internal = is_internal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(is_internal);
    }
}
