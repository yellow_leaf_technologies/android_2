package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class InspectionTypeDTO implements Serializable {

    int status;
    String message;
    InsData data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InsData getData() {
        return data;
    }

    public void setData(InsData data) {
        this.data = data;
    }

    public class InsData {
        List<InsTypeList> instypeList;

        public List<InsTypeList> getInstypeList() {
            return instypeList;
        }

        public void setInstypeList(List<InsTypeList> instypeList) {
            this.instypeList = instypeList;
        }

        public class InsTypeList {
            int id;
            String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

}
