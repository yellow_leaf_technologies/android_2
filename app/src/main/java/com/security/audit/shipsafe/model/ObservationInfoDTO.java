package com.security.audit.shipsafe.model;

public class ObservationInfoDTO {

    int vesselId;
    int portPlace;
    String reportDate;
    String reportTime;
    String auditorDate;
    String auditorTime;
    String openingHeldAt;
    String openingAttendedBy;
    String closingHeldAt;
    String closingAttendedBy;
    String checklistUUID;

    public int getVesselId() {
        return vesselId;
    }

    public void setVesselId(int vesselId) {
        this.vesselId = vesselId;
    }

    public int getPortPlace() {
        return portPlace;
    }

    public void setPortPlace(int portPlace) {
        this.portPlace = portPlace;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getAuditorDate() {
        return auditorDate;
    }

    public void setAuditorDate(String auditorDate) {
        this.auditorDate = auditorDate;
    }

    public String getAuditorTime() {
        return auditorTime;
    }

    public void setAuditorTime(String auditorTime) {
        this.auditorTime = auditorTime;
    }

    public String getOpeningHeldAt() {
        return openingHeldAt;
    }

    public void setOpeningHeldAt(String openingHeldAt) {
        this.openingHeldAt = openingHeldAt;
    }

    public String getOpeningAttendedBy() {
        return openingAttendedBy;
    }

    public void setOpeningAttendedBy(String openingAttendedBy) {
        this.openingAttendedBy = openingAttendedBy;
    }

    public String getClosingHeldAt() {
        return closingHeldAt;
    }

    public void setClosingHeldAt(String closingHeldAt) {
        this.closingHeldAt = closingHeldAt;
    }

    public String getClosingAttendedBy() {
        return closingAttendedBy;
    }

    public void setClosingAttendedBy(String closingAttendedBy) {
        this.closingAttendedBy = closingAttendedBy;
    }

    public String getChecklistUUID() {
        return checklistUUID;
    }

    public void setChecklistUUID(String checklistUUID) {
        this.checklistUUID = checklistUUID;
    }
}
