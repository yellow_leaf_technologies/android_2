package com.security.audit.shipsafe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CountryName implements Parcelable {
    int ID;
    String countryName;
    String countryCode;
    String description;
    String createdAt;
    String updated_At;

    public CountryName() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdated_At() {
        return updated_At;
    }

    public void setUpdated_At(String updated_At) {
        this.updated_At = updated_At;
    }

    public static Creator<CountryName> getCREATOR() {
        return CREATOR;
    }

    public CountryName(Parcel in) {
        ID = in.readInt();
        countryName = in.readString();
        countryCode = in.readString();
        description = in.readString();
        createdAt = in.readString();
        updated_At = in.readString();
    }

    public static final Creator<CountryName> CREATOR = new Creator<CountryName>() {
        @Override
        public CountryName createFromParcel(Parcel in) {
            return new CountryName(in);
        }

        @Override
        public CountryName[] newArray(int size) {
            return new CountryName[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(countryName);
        dest.writeString(countryCode);
        dest.writeString(description);
        dest.writeString(createdAt);
        dest.writeString(updated_At);
    }
}
