package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class ObservationTypeDTO implements Serializable {

    int status;
    String message;
    ObsData data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ObsData getData() {
        return data;
    }

    public void setData(ObsData data) {
        this.data = data;
    }

    public class ObsData {
        List<ObsTypeList> obstypeList;

        public List<ObsTypeList> getObstypeList() {
            return obstypeList;
        }

        public void setObstypeList(List<ObsTypeList> obstypeList) {
            this.obstypeList = obstypeList;
        }

        public class ObsTypeList {
            int id;
            String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


}
