package com.security.audit.shipsafe.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;

public class Splash extends Activity {
  ImageView playgif;
  Context mContext;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_splash);
    mContext=this;
    playgif = (ImageView) findViewById(R.id.gif);

    Glide.with(getApplicationContext()).
            load(R.drawable.splash).into(playgif);
    if(SaveSharedPreference.getRequestCode(mContext).length()!=0 && SaveSharedPreference.getStatus(mContext).length()==0)
    {
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          Intent startActivity = new Intent(Splash.this, Login.class);
          startActivity(startActivity);
          finish();
        }
      }, 2000);
    }
    else if(SaveSharedPreference.getStatus(mContext).equals("Login"))
    {
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          Intent startActivity = new Intent(Splash.this, HomeActivity.class);
          startActivity(startActivity);
          finish();
        }
      }, 2000);
    }
    else{
      StartNextActivity();
    }

  }

  public void StartNextActivity() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        Intent startActivity = new Intent(Splash.this, LoginRequestActivity.class);
        startActivity(startActivity);
        finish();
      }
    }, 2000);
  }

}

