package com.security.audit.shipsafe.adapter;

import java.io.Serializable;
import java.util.Calendar;

interface IAdapter extends Serializable {
    void setDataAt(int index, Calendar date);
    void notifyItemChanged(int index);
}
