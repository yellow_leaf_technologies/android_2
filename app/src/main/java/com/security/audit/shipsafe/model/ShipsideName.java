package com.security.audit.shipsafe.model;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class ShipsideName {
    int id;
    String name;
    int checklist_type__id;
    String checklist_type__name;
    int order;
    String remarks;
    List<Uri> images;
    String shipSideCheckListID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChecklist_type__id() {
        return checklist_type__id;
    }

    public void setChecklist_type__id(int checklist_type__id) {
        this.checklist_type__id = checklist_type__id;
    }

    public String getChecklist_type__name() {
        return checklist_type__name;
    }

    public void setChecklist_type__name(String checklist_type__name) {
        this.checklist_type__name = checklist_type__name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<Uri> getImages() {
        return images;
    }

    public void setImages(List<Uri> images) {
        if (this.images == null)
            this.images = images;
        else
            this.images.addAll(images);
    }

    public void setImage(Uri item) {
        if (images == null) {
            images = new ArrayList<>();
        }
        images.add(item);
    }

    public void setShipSideCheckListID(String shipSideCheckListID) {
        this.shipSideCheckListID = shipSideCheckListID;
    }

    public String getShipSideCheckListID() {
        return shipSideCheckListID;
    }
}

