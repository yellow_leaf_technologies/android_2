package com.security.audit.shipsafe.model;

public class FilteredPortList {
    String name;
    String country__name;
    int country__id;
    String full_name;
    String port_code;
    int id;
    String locode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry__name() {
        return country__name;
    }

    public void setCountry__name(String country__name) {
        this.country__name = country__name;
    }

    public int getCountry__id() {
        return country__id;
    }

    public void setCountry__id(int country__id) {
        this.country__id = country__id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getPort_code() {
        return port_code;
    }

    public void setPort_code(String port_code) {
        this.port_code = port_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocode() {
        return locode;
    }

    public void setLocode(String locode) {
        this.locode = locode;
    }
}

