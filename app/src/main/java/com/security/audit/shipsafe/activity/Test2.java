/*

package com.security.audit.shipsafe.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.security.audit.shipsafe.R;
import com.williamww.silkysignature.views.SignaturePad;

import java.util.List;

public class Test2 extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener{
    LinearLayout dynamicLayout;
    int idCounter = 0;
    RoundedImageView ivImage2;
    Button btnUploadImage;

    LinearLayout layout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        dynamicLayout=(LinearLayout)findViewById(R.id.dynamicLayoutphoto);

        btnUploadImage=(Button)findViewById(R.id.Upload_Images);

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCustomDialogInspector(v);
            }
        });



        findViewById(R.id.tv_single_selection1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.tv_multi_selection1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        List<ModelClass.Employement> employements = MDC.getEmployment();
        if (employements != null && employements.size() > 0) {

            for (int i = 0; i < employements.size(); i++) {

                final View childViewEmployement = getLayoutInflater().inflate(R.layout.custom_employement, null);
                linearChildEmployement.addView(childViewEmployement);

                tietEmpDesignation = childViewEmployement.findViewById(R.id.tietEmpDesignation);
                tietEmpOrganization = childViewEmployement.findViewById(R.id.tietEmpOrganization);
                tietEmpExpType = childViewEmployement.findViewById(R.id.tietEmpExpType);
                tietEmpWorkedTill = childViewEmployement.findViewById(R.id.tietEmpWorkedTill);
                tietEmpJobProf = childViewEmployement.findViewById(R.id.tietEmpJobProf);

                String IdEmp = employements.get(i).getId();
                String Designation = employements.get(i).getDesigination();
                String Organiza = employements.get(i).getOrgination();
                String CurrentCom = employements.get(i).getCurrentCompany();
                String StartWorking = employements.get(i).getStartWorking();
                String EndWorking = employements.get(i).getEndWorking();
                String JobPro = employements.get(i).getJobProfile();

                if (Designation != null) {
                    tietEmpDesignation.setText(Designation);
                }
                if (Organiza != null) {
                    tietEmpOrganization.setText(Organiza);
                }
                if (CurrentCom != null) {
                    tietEmpExpType.setText(CurrentCom);
                }
                if (StartWorking != null && EndWorking != null) {
                    tietEmpWorkedTill.setText(StartWorking + "To" + EndWorking);
                }
                if (JobPro != null) {
                    tietEmpJobProf.setText(JobPro);
                }

                //Comment for Shadab
                //i just used setTag() to that button whose inflating from customView.
                //And i want to get all the data from this custom view to next Activity after clicking (setTag) button.
                //Because i have so many variables at this customView so i cant send it to next Activity directly.
                //For that i just converted to all into JSON format and then used setArgument & getArgument.
                final ImageButton btnEditEmp = childViewEmployement.findViewById(R.id.btnEditEmp);
                btnEditEmp.setTag(employements.get(i));
                btnEditEmp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ModelClass.Employement Emp = (ModelClass.Employement) btnEditEmp.getTag();
                        String EmploymentData = new Gson().toJson(Emp, ModelClass.Employement.class);
                        EmploymentFragment employmentFragment = new EmploymentFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString("EmploymentData", EmploymentData);
                        employmentFragment.setArguments(bundle);
                        Prefrences.swapFragment(getFragmentManager(), employmentFragment);
                    }
                });
            }
        }


    }


    void openCustomDialogInspector(View view) {
        final android.app.AlertDialog.Builder customDialog
                = new android.app.AlertDialog.Builder(view.getRootView().getContext());
        //customDialog.setTitle("Inspector Signature");
        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.dialog_photo_picker, null);
        // v.setBackgroundColor(Color.WHITE);
        final TextView SingleSelection,MultipleSelection;

        SingleSelection=(TextView) v.findViewById(R.id.single_selection_image);
        MultipleSelection=(TextView)v.findViewById(R.id.multiple_selection_image);

        SingleSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");



            }
        });

        MultipleSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                        .setMaximumDisplayingImages(Integer.MAX_VALUE)
                        .isMultiSelect()
                        .setMinimumMultiSelectCount(1)
                        .setMaximumMultiSelectCount(60)
                        .build();
                pickerDialog.show(getSupportFragmentManager(), "picker");
            }
        });

        customDialog.setCancelable(true);
        customDialog.setView(v);
        customDialog.show();
    }

    public void close(DialogInterface dialogInterface){

        dialogInterface.dismiss();

    }

    @Override
    public void onSingleImageSelected(Uri uri) {
        idCounter = idCounter + 1;
        final View views = getLayoutInflater().inflate(R.layout.image_view_item, null);
        ivImage2 = (RoundedImageView) views.findViewById(R.id.iv_imageNew);
        ImageButton btnDel=(ImageButton)views.findViewById(R.id.btn_delete_image);
        views.setId(idCounter);
        dynamicLayout.addView(views);
        Glide.with(Test2.this).load(uri).into(ivImage2);

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dynamicLayout.removeView(views);
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onMultiImageSelected(List<Uri> uriList) {

        for (int i=0; i < uriList.size(); i++) {


            final View views = getLayoutInflater().inflate(R.layout.image_view_item, null);
            ivImage2 = (RoundedImageView) views.findViewById(R.id.iv_imageNew);
            ImageButton btnDel=(ImageButton)views.findViewById(R.id.btn_delete_image);
            dynamicLayout.addView(views);

            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dynamicLayout.removeView(views);
                }
            });

            if (i >= 60) return;
            RoundedImageView iv;
            switch (i) {
                case 0:
                    iv = ivImage2;
                    break;
                default:
                    iv = ivImage2;
            }
            Glide
                    .with(this)
                    .load(uriList.get(i))
                    .apply(new RequestOptions().override(300, 200))
                    .into(iv);



        }
    }

}

*/
