package com.security.audit.shipsafe.adapter;

import android.app.DialogFragment;
import android.os.Bundle;

public class MyDialogFragment extends DialogFragment {
    private int index;
    private IAdapter adapter;


    public static MyDialogFragment newInstance(int index, IAdapter adapter) {
        MyDialogFragment f = new MyDialogFragment();

        Bundle args = new Bundle();
        args.putInt("index", index);
        args.putSerializable("adapter", adapter);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        index = args.getInt("index");
        adapter = (IAdapter) args.getSerializable("adapter");
    }

    // Build view and set OnClickListener for setting date
}