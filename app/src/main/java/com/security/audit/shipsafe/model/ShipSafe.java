package com.security.audit.shipsafe.model;

public class ShipSafe {

    private String checklistUUID;
    private int vasselId;
    private String remark;
    private String signatures;
    private String inspector;


    public ShipSafe() {

    }

    public ShipSafe(String checklistUUID, int vasselId, String remark, String signatures, String inspector) {
        this.checklistUUID = checklistUUID;
        this.vasselId = vasselId;
        this.remark = remark;
        this.signatures = signatures;
        this.inspector = inspector;
    }

    public String getChecklistUUID() {
        return checklistUUID;
    }

    public void setChecklistUUID(String checklistUUID) {
        this.checklistUUID = checklistUUID;
    }

    public int getVasselId() {
        return vasselId;
    }

    public void setVasselId(int vasselId) {
        this.vasselId = vasselId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSignatures() {
        return signatures;
    }

    public void setSignatures(String signatures) {
        this.signatures = signatures;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }
}
