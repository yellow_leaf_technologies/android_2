package com.security.audit.shipsafe.api.core;

import android.content.Context;

import com.security.audit.shipsafe.api.GetDataService;
import com.security.audit.shipsafe.api.RetrofitClientInstance;

public class ApiUtils {

    public static final String BASE_URL = "https://shipsafe.topcubit.com/";

    public static GetDataService getDataService(Context context) {
        return RetrofitClientInstance.getRetrofitInstance(context).create(GetDataService.class);
    }
}
