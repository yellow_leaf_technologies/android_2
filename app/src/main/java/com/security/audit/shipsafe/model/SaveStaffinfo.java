package com.security.audit.shipsafe.model;

public class SaveStaffinfo {

    String rank;
    String name;
    int yearInCompany;
    int monthInCompany;
    String nationality;
    String checklistUUID;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearInCompany() {
        return yearInCompany;
    }

    public void setYearInCompany(int yearInCompany) {
        this.yearInCompany = yearInCompany;
    }

    public int getMonthInCompany() {
        return monthInCompany;
    }

    public void setMonthInCompany(int monthInCompany) {
        this.monthInCompany = monthInCompany;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getChecklistUUID() {
        return checklistUUID;
    }

    public void setChecklistUUID(String checklistUUID) {
        this.checklistUUID = checklistUUID;
    }
}
