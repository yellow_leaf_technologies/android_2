package com.security.audit.shipsafe.api;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpClientModule {
    public OkHttpClient okHttpClient(Context context) {
        return new OkHttpClient()
                .newBuilder()
                .cache(cache(context))
                .addInterceptor(interceptor(context))
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public Cache cache(Context context) {
        return new Cache(file(context), 10 * 1024 * 1024);
    }

    public File file(Context context) {
        File file = new File(context.getCacheDir(), "HttpCache");
        file.mkdirs();
        return file;
    }

    public Interceptor interceptor(Context context) {
        return chain -> {
            Request origin = chain.request();
            Request.Builder builder = origin.newBuilder();
            builder.header("Content-type", "application/json")
                    .header("Cache-Control", "max-age=432000")
                    .removeHeader("Pragma");
            if (!TextUtils.isEmpty(null)) {
                builder.header("Authorization", "Bearer " + null);
            }
            Request request = builder.build();
            Response response = null;
            try {
                response = chain.proceed(request);
                response.cacheResponse();
            } catch (SocketTimeoutException exception) {
                exception.printStackTrace();
            }

            return response;
        };
    }
}
