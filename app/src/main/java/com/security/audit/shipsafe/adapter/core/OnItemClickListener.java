package com.security.audit.shipsafe.adapter.core;

public interface OnItemClickListener<T> {
    void onClick(T model, int position);
}

