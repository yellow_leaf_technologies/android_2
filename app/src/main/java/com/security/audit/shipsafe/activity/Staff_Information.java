package com.security.audit.shipsafe.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.model.SaveStaffinfo;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.lang.reflect.Field;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Staff_Information extends AppCompatActivity {
    TextView selectCountry;
    CircleImageView addNewInformation;
    LinearLayout dynamicLayout;
    Context mContext;
    Spinner RankSpinner,NationalitySpinner,ExperienceYear,ExperienceMonth;
    ArrayList<String> spinnerarray;
    ArrayList<String>rankarray;
    ArrayList<Integer>yearrank;
    ArrayList<Integer>monthrank;
    EditText EnterStaffName;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter1;
    ArrayAdapter<Integer> adapter2,adapter3;
    Button btnSaveInfo;


    DatabaseHelperAdmin databaseHelperAdmin,databaseHelperAdmin1,databaseHelperSaveStaffInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_staff_information);
        getSupportActionBar().setTitle("Staff Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext=this;
        databaseHelperSaveStaffInfo=new DatabaseHelperAdmin(mContext);
        dynamicLayout=(LinearLayout)findViewById(R.id.dynamicLayoutInformation);
        addNewInformation=(CircleImageView)findViewById(R.id.addNewInformation);

        final String ChecklistUUID= getIntent().getStringExtra("MAC_U_ID");
        addNewInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View views = getLayoutInflater().inflate(R.layout.add_new_staff_information, null);
                Spinner NewRank,NewNationality,NewYear,NewMonth;
                EditText NewName;
                ImageView DelLayout;

                NewRank=(Spinner)views.findViewById(R.id.AddNewRank);
                NewNationality=(Spinner)views.findViewById(R.id.AddNewNationality);
                NewYear=(Spinner)views.findViewById(R.id.AddNewExperience);
                NewMonth=(Spinner)views.findViewById(R.id.AddNewMonth);
                NewName=(EditText) views.findViewById(R.id.AddNeName);
                DelLayout=(ImageView)views.findViewById(R.id.Del_Rank);
                NewNationality.setAdapter(adapter);
                NewRank.setAdapter(new MyCustomAdapter(Staff_Information.this, R.layout.rank_spinner_item_view, rankarray));
                NewYear.setAdapter(adapter2);
                NewMonth.setAdapter(adapter3);

                int posRank = RankSpinner.getSelectedItemPosition();
                NewRank.setSelection(posRank);

                int posNationality = NationalitySpinner.getSelectedItemPosition();
                NewNationality.setSelection(posNationality);

                int posYear = ExperienceYear.getSelectedItemPosition();
                NewYear.setSelection(posYear);

                int posMonth = ExperienceMonth.getSelectedItemPosition();
                NewMonth.setSelection(posMonth);
                NewName.setText(EnterStaffName.getText().toString());

                int eyear=Integer.parseInt(ExperienceYear.getSelectedItem().toString());
                int emonth=Integer.parseInt(ExperienceMonth.getSelectedItem().toString());

                SaveStaffinfo info=new SaveStaffinfo();
                info.setRank(RankSpinner.getSelectedItem().toString());
                info.setName(EnterStaffName.getText().toString());
                info.setNationality(NationalitySpinner.getSelectedItem().toString());
                info.setYearInCompany(eyear);
                info.setMonthInCompany(emonth);
                info.setChecklistUUID(ChecklistUUID);

                databaseHelperSaveStaffInfo.addSaveStaffInfo(info);

                RankSpinner.setSelection(0);
                NationalitySpinner.setSelection(0);
                ExperienceYear.setSelection(0);
                ExperienceMonth.setSelection(0);
                EnterStaffName.setText("");


                dynamicLayout.addView(views);
                DelLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "Deleted!", Toast.LENGTH_SHORT).show();
                        dynamicLayout.removeView(views);
                    }
                });
            }
        });

        databaseHelperAdmin=new DatabaseHelperAdmin(mContext);
        RankSpinner=(Spinner)findViewById(R.id.RankSpinner);
        NationalitySpinner=(Spinner)findViewById(R.id.NationalitySpinner);
        RankSpinner=(Spinner)findViewById(R.id.RankSpinner);
        ExperienceYear=(Spinner)findViewById(R.id.YearRank);
        ExperienceMonth=(Spinner)findViewById(R.id.MonthRank);
        EnterStaffName=(EditText)findViewById(R.id.EnterStaffName);

        spinnerarray = new ArrayList<String>();
        rankarray = new ArrayList<String>();
        spinnerarray = new ArrayList<String>();
        rankarray = new ArrayList<String>();
        yearrank = new ArrayList<Integer>();
        yearrank.add(0);
        yearrank.add(1);
        yearrank.add(2);
        yearrank.add(3);
        yearrank.add(4);
        yearrank.add(5);
        yearrank.add(6);
        yearrank.add(7);
        yearrank.add(8);
        yearrank.add(9);
        yearrank.add(10);
        yearrank.add(11);
        yearrank.add(12);
        yearrank.add(13);
        yearrank.add(14);
        yearrank.add(15);
        yearrank.add(16);
        yearrank.add(17);
        yearrank.add(18);
        yearrank.add(19);
        yearrank.add(20);
        yearrank.add(21);
        yearrank.add(22);
        yearrank.add(23);
        yearrank.add(24);
        yearrank.add(25);
        yearrank.add(26);
        yearrank.add(27);
        yearrank.add(28);
        yearrank.add(29);
        yearrank.add(30);

        monthrank = new ArrayList<Integer>();
        monthrank.add(0);
        monthrank.add(1);
        monthrank.add(2);
        monthrank.add(3);
        monthrank.add(4);
        monthrank.add(5);
        monthrank.add(6);
        monthrank.add(7);
        monthrank.add(8);
        monthrank.add(9);
        monthrank.add(10);
        monthrank.add(11);


        databaseHelperAdmin1=new DatabaseHelperAdmin(mContext);

        spinnerarray=databaseHelperAdmin.getNationality();
        rankarray=databaseHelperAdmin.getRank();

        RankSpinner.setAdapter(new MyCustomAdapter(Staff_Information.this, R.layout.rank_spinner_item_view, rankarray));

        adapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, spinnerarray);
        adapter.setDropDownViewResource(R.layout.nationalityr_item_view);
        NationalitySpinner.setAdapter(adapter);

        adapter2 = new ArrayAdapter<Integer>(mContext, R.layout.rank_spinner_item_view, yearrank);
        adapter.setDropDownViewResource(R.layout.rank_spinner_item_view);
        ExperienceYear.setAdapter(adapter2);

        adapter3 = new ArrayAdapter<Integer>(mContext, R.layout.rank_spinner_item_view, monthrank);
        adapter.setDropDownViewResource(R.layout.rank_spinner_item_view);
        ExperienceMonth.setAdapter(adapter3);

        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(ExperienceYear);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(ExperienceMonth);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }



        final int ChecklistTypeId=getIntent().getIntExtra("CID",0);
        final int VesselId=getIntent().getIntExtra("VESSEL_ID",0);


        btnSaveInfo=findViewById(R.id.btnSaveStaffInformation);

        btnSaveInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String name=EnterStaffName.getText().toString();

                if (name.equals(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please fill staff information");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else
                {
                    int eyear=Integer.parseInt(ExperienceYear.getSelectedItem().toString());
                    int emonth=Integer.parseInt(ExperienceMonth.getSelectedItem().toString());
                    SaveStaffinfo info=new SaveStaffinfo();
                    info.setRank(RankSpinner.getSelectedItem().toString());
                    info.setName(EnterStaffName.getText().toString());
                    info.setNationality(NationalitySpinner.getSelectedItem().toString());
                    info.setYearInCompany(eyear);
                    info.setMonthInCompany(emonth);
                    info.setChecklistUUID(ChecklistUUID);

                    databaseHelperSaveStaffInfo.addSaveStaffInfo(info);

                    Intent i2 = new Intent(mContext, Checklist_All_Category.class);
                    i2.putExtra("U_ID", ChecklistUUID);
                    i2.putExtra("V_ID", VesselId);
                    i2.putExtra("CID",ChecklistTypeId);
                    startActivity(i2);
                }

            }
        });

    }


    public class MyCustomAdapter extends ArrayAdapter<String>{

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<String> objects) {
            super(context, textViewResourceId, objects);
// TODO Auto-generated constructor stub
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
// TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
//return super.getView(position, convertView, parent);

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.rank_spinner_item_view, parent, false);
            TextView label=(TextView)row.findViewById(R.id.rank_item);
            label.setText(rankarray.get(position));
            return row;
        }
    }

    void openCustomDialog(View view) {
        final AlertDialog.Builder customDialog
                = new AlertDialog.Builder(view.getRootView().getContext());
        customDialog.setTitle("Required Password");
        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.enter_name_staff_information, null);
        // v.setBackgroundColor(Color.WHITE);
        final EditText enterpass;

        enterpass = (EditText) v.findViewById(R.id.master_pass);


        customDialog.setPositiveButton("Enter", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                String s = enterpass.getText().toString();
            }
        });

        customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub

            }
        });

        customDialog.setView(v);
        customDialog.show();
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_Save_Staff_Information:

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
