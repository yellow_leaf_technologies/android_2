package com.security.audit.shipsafe.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.List;

public class InternetConnectionObserver {
    private static InternetConnectionObserver instance;
    private BroadcastReceiver receiver;
    private List<ConnectionListener> listeners = new ArrayList<>();
    private boolean isStartState = true;
    private int lastStatus;

    private InternetConnectionObserver(Context context) {
        createBroadcastReceiver(context);
    }

    public static InternetConnectionObserver getInstance(Context context) {
        if (instance == null) {
            instance = new InternetConnectionObserver(context);
        }
        return instance;
    }

    private void createBroadcastReceiver(Context context) {
        if (receiver == null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            receiver =
                    new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            if (isStartState) {
                                isStartState = false;
                                lastStatus = NetworkUtil.getConnectivityStatusString(context);
                                return;
                            }
                            int status = NetworkUtil.getConnectivityStatusString(context);
                            if (status != lastStatus) {
                                lastStatus = status;
                                switch (status) {
                                    case NetworkUtil.NETWORK_STATUS_MOBILE:
                                        for (ConnectionListener listener : listeners) {
                                            listener.onConnectionStateChanged(
                                                    ConnectionState.MOBILE_NETWORK_CONNECTED);
                                        }
                                        break;
                                    case NetworkUtil.NETWORK_STATUS_WIFI:
                                        for (ConnectionListener listener : listeners) {
                                            listener.onConnectionStateChanged(
                                                    ConnectionState.WIFI_CONNECTED);
                                        }
                                        break;
                                    case NetworkUtil.NETWORK_STATUS_NOT_CONNECTED:
                                        for (ConnectionListener listener : listeners) {
                                            listener.onConnectionStateChanged(
                                                    ConnectionState.DISCONNECTED);
                                        }
                                        break;
                                }
                            }
                        }
                    };
            try {
                context.registerReceiver(receiver, filter);
            } catch (Exception e) {
            }
        }
    }

    public void addListener(ConnectionListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener() {
        listeners.clear();
    }

    public interface ConnectionListener {
        void onConnectionStateChanged(ConnectionState state);
    }

    public enum ConnectionState {
        WIFI_CONNECTED,
        MOBILE_NETWORK_CONNECTED,
        DISCONNECTED
    }
}

