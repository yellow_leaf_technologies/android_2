package com.security.audit.shipsafe.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.activity.PhotoReport;
import com.security.audit.shipsafe.activity.Questions;
import com.security.audit.shipsafe.model.ShipsideName;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ShipSideAdapter extends RecyclerView.Adapter<ShipSideAdapter.ItemViewHolder> {
    AlertDialog b = null;
    private Context mContext;
    private final List<ShipsideName> dto;
    int lastPosition = -1;
    PhotoReport photoReport;

    public ShipSideAdapter(Context c, List<ShipsideName> dto, PhotoReport photoReport) {
        mContext = c;
        this.dto = dto;
        this.photoReport = photoReport;
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView ShipSideName;
        TextView noofphotos;
        TextView DateoofPhotoReport;
        ImageView btnAddedImages;
        EditText RemarksPhotoReport;
        LinearLayout AddedPhotosInPhotoReport;
        int position;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ShipSideName = (TextView) itemView.findViewById(R.id.ShipSideName);
            noofphotos = (TextView) itemView.findViewById(R.id.noofphotos);
            DateoofPhotoReport = (TextView) itemView.findViewById(R.id.DateoofPhotoReport);
            btnAddedImages = (ImageView) itemView.findViewById(R.id.btnAddedImages);
            RemarksPhotoReport = (EditText) itemView.findViewById(R.id.RemarksPhotoReport);
            AddedPhotosInPhotoReport = (LinearLayout) itemView.findViewById(R.id.AddedPhotosInPhotoReport);

            btnAddedImages.setOnClickListener(v -> openCustomDialog(v, position, AddedPhotosInPhotoReport));
        }

        @Override
        public void onClick(View v) {
            /* Intent i=new Intent(mContext, Questions.class);
            i.putExtra("CAT_TYPE", (Parcelable) dto.get(getAdapterPosition()));
            mContext.startActivity(i);*/
        }
    }


    void openCustomDialog(View view, final int position, final LinearLayout AddedLayout) {
        final android.app.AlertDialog.Builder customDialog
                = new android.app.AlertDialog.Builder(view.getRootView().getContext());
        //customDialog.setTitle("Inspector Signature");

        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.dialog_photo_picker, null);
        // v.setBackgroundColor(Color.WHITE);
        customDialog.setCancelable(false);
        customDialog.setTitle("Add Photo");
        customDialog.setView(v);
        customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        final android.app.AlertDialog dialog = customDialog.create();
        dialog.show();  // to show
        final LinearLayout SingleSelection, MultipleSelection;

        SingleSelection = (LinearLayout) v.findViewById(R.id.single_selection_image);
        MultipleSelection = (LinearLayout) v.findViewById(R.id.multiple_selection_image);


        SingleSelection.setOnClickListener(v1 -> {
            if (mContext instanceof PhotoReport) {
                ((PhotoReport) mContext).CallSingleSelection(position, AddedLayout);
                dialog.dismiss();

            }
        });
        MultipleSelection.setOnClickListener(v12 -> {
            if (mContext instanceof PhotoReport) {
                ((PhotoReport) mContext).CallMultipleSelection(position, AddedLayout);
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {
        lastPosition = position;
        ShipsideName item = dto.get(position);
        String name = item.getName();
        itemViewHolder.ShipSideName.setText(name);
        itemViewHolder.position = position;
        if (item.getRemarks() != null)
            itemViewHolder.RemarksPhotoReport.setText(item.getRemarks());
        itemViewHolder.RemarksPhotoReport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    dto.get(position).setRemarks(s.toString());
                }
            }
        });
        if (item.getImages() != null && item.getImages().size() > 0) {
            setPhotos(item.getImages(), itemViewHolder.AddedPhotosInPhotoReport, position, true);
        }
    }

    public void setPhotos(List<Uri> uriList, final LinearLayout ItemView, final int position, boolean isReset) {
        for (int i = 0; i < uriList.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RoundedImageView ivImage2;
            final View views = inflater.inflate(R.layout.image_view_item, null);
            ivImage2 = views.findViewById(R.id.DynamicImage);
            final ImageButton btnDel = views.findViewById(R.id.DynamicImageBtn);
            ItemView.addView(views);
            btnDel.setTag(i);
            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Uri> images = dto.get(position).getImages();
                    if (images != null && images.size() > 0) {
                        try {
                            images.remove(Integer.parseInt(btnDel.getTag().toString()));
                        } catch (Exception e) {
                        }
                    }
                    ItemView.removeView(views);
                }
            });
            if (i >= 10) return;
            RoundedImageView iv;
            switch (i) {
                case 0:
                    iv = ivImage2;
                    break;
                default:
                    iv = ivImage2;
            }
            Glide
                    .with(mContext)
                    .load(uriList.get(i))
                    .apply(new RequestOptions().override(300, 200))
                    .into(iv);
        }
        if (!isReset)
            dto.get(position).setImages(uriList);
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item_view, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public int getItemCount() {
        return dto.size();
    }

}
