package com.security.audit.shipsafe.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.model.SaveObservationDTO;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ObservationSecond extends AppCompatActivity {

    LinearLayout dynamicLayout;
    CircleImageView addNewObservation;
    Spinner observationType, inspectionType, Department_Spinner;
    EditText TargetDate, CloseDate, Remarks;
    private static final String[] COUNTRIES = new String[]{"Company Name",
            "France", "Italy", "Germany", "Spain"};
    DatePickerDialog.OnDateSetListener DATE, DATE2, DATE3, DATE4;
    Calendar myCalendar = Calendar.getInstance();
    private TextView DepartmentText;
    private Spinner DepartmentSpinner;
    private TextView ObservationTypeText;
    private TextView InspectionTypeText;
    private Spinner ObservationTypeSpinner;
    private Spinner InspectionTypeSpinner;
    private EditText TargetDateObservationSecond;
    private EditText CloseDateObservationSecond;
    private EditText RemObSecond;
    private CircleImageView addNewObservationSecond;
    private LinearLayout dynamicLayoutObservationSecond;
    ArrayAdapter<String> OAdapter, IAdapter;
    Context mContext;
    ArrayList<String> ObserVationTypeArray;
    ArrayList<String> InspectionTypeArray;
    ArrayList<String> ObservationDepartmentArray;
    DatabaseHelperAdmin databaseHelperOType, databaseHelperIType, databaseHelperObservationDepartment, databaseHelperDepartmentId;
    String is_internal;
    boolean clearTargetDate = false;
    boolean clearCloseDate = false;
    ArrayAdapter<String> adapter;
    int Vesselid;
    String checklistUUID;
    SaveObservationDTO info = new SaveObservationDTO();
    private LinearLayout btnSubmitObservationReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observation_second);
        getSupportActionBar().setTitle("Observation Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();
        mContext = this;
        databaseHelperOType = new DatabaseHelperAdmin(mContext);
        databaseHelperIType = new DatabaseHelperAdmin(mContext);
        dynamicLayout = (LinearLayout) findViewById(R.id.dynamicLayoutObservationSecond);
        addNewObservation = (CircleImageView) findViewById(R.id.addNewObservationSecond);
        observationType = (Spinner) findViewById(R.id.Observation_Type_Spinner);
        inspectionType = (Spinner) findViewById(R.id.Inspection_type_Spinner);
        TargetDate = (EditText) findViewById(R.id.TargetDateObservationSecond);
        CloseDate = (EditText) findViewById(R.id.CloseDateObservationSecond);
        Remarks = (EditText) findViewById(R.id.Rem_Ob_Second);
        Department_Spinner = (Spinner) findViewById(R.id.Department_Spinner);
        databaseHelperObservationDepartment = new DatabaseHelperAdmin(mContext);
        databaseHelperDepartmentId = new DatabaseHelperAdmin(mContext);
        newObservationIds = new ArrayList<>();

        ObserVationTypeArray = databaseHelperOType.getObservations();


        ObservationDepartmentArray = databaseHelperObservationDepartment.getObservationDepartment();

        adapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, ObservationDepartmentArray);
        adapter.setDropDownViewResource(R.layout.nationalityr_item_view);
        Department_Spinner.setAdapter(adapter);


        OAdapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, ObserVationTypeArray);
        OAdapter.setDropDownViewResource(R.layout.nationalityr_item_view);
        ObservationTypeSpinner.setAdapter(OAdapter);


        InspectionTypeArray = databaseHelperIType.getInspections();

        IAdapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, InspectionTypeArray);
        IAdapter.setDropDownViewResource(R.layout.nationalityr_item_view);
        InspectionTypeSpinner.setAdapter(IAdapter);
        Remarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null)
                    info.setRemark(s.toString());
            }
        });


        is_internal = getIntent().getStringExtra("Is_Internal");
        Vesselid = getIntent().getIntExtra("V_ID", 0);
        checklistUUID = getIntent().getStringExtra("Checklist_UUID");

        info.setChecklistUUID(checklistUUID);
        info.setDepartmentId(-1);
        ObservationTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getAdapter().getItem(position);
                info.setObservationType(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        InspectionTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getAdapter().getItem(position);
                info.setInspectionType(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        DATE = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                boolean dateset;
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                clearTargetDate = true;
                updateLabel();
                TargetDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);
            }

        };
        DATE2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                clearCloseDate = true;
                updateLabel1();
                CloseDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);
            }

        };

        TargetDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (clearTargetDate == true) {
                    TargetDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                    TargetDate.setText("DD/MM/YYYY");
                    clearTargetDate = false;
                } else {

                    new DatePickerDialog(ObservationSecond.this, DATE, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                }


            }
        });
        CloseDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (clearCloseDate == true) {
                    CloseDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                    CloseDate.setText("DD/MM/YYYY");
                    clearCloseDate = false;
                } else {
                    new DatePickerDialog(ObservationSecond.this, DATE2, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        addNewObservation.setOnClickListener(v -> {
            databaseHelperDepartmentId.addSaveObservation(info);
            info = new SaveObservationDTO();
            if (is_internal.equals("1")) {
                final View views = getLayoutInflater().inflate(R.layout.add_internal_new_observation, null);
                final EditText targetDateTwo, closeDateTwo, RemarkTwo;
                Spinner ObservationType, InspectionType;
                ObservationType = (Spinner) views.findViewById(R.id.NewObservationTypeSecond);
                InspectionType = (Spinner) views.findViewById(R.id.NewInspectionTypeSecond);
                targetDateTwo = (EditText) views.findViewById(R.id.NewDateObSecond);
                closeDateTwo = (EditText) views.findViewById(R.id.NewCloseDateObSecond);
                RemarkTwo = (EditText) views.findViewById(R.id.New_Remarks_Set_Second);
                String date1 = TargetDate.getText().toString();
                targetDateTwo.setText(date1);
                closeDateTwo.setText(CloseDate.getText().toString());
                RemarkTwo.setText(Remarks.getText().toString());
                MyArrayAdapter ma = new MyArrayAdapter(ObservationSecond.this);
                ObservationType.setAdapter(ma);

                ObserVationTypeArray = databaseHelperOType.getObservations();
                OAdapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, ObserVationTypeArray);
                OAdapter.setDropDownViewResource(R.layout.nationalityr_item_view);
                ObservationType.setAdapter(OAdapter);


                InspectionTypeArray = databaseHelperIType.getInspections();

                IAdapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, InspectionTypeArray);
                IAdapter.setDropDownViewResource(R.layout.nationalityr_item_view);
                InspectionType.setAdapter(IAdapter);

                int pos = ObservationTypeSpinner.getSelectedItemPosition();
                ObservationType.setSelection(pos);

                int pos1 = InspectionTypeSpinner.getSelectedItemPosition();
                InspectionType.setSelection(pos1);


//                    SaveObservationDTO info = new SaveObservationDTO();
//
//                    info.setChecklistUUID(checklistUUID);
//                    info.setTargetDate(TargetDate.getText().toString());
//                    info.setClosedDate(CloseDate.getText().toString());
//                    info.setObservation(Remarks.getText().toString());
//                    info.setDepartmentId(-1);
//                    info.setObservationType(ObservationTypeSpinner.getSelectedItem().toString());
//                    info.setInspectionType(InspectionTypeSpinner.getSelectedItem().toString());
//                    databaseHelperDepartmentId.addSaveObservation(info);


                TargetDate.setText("");
                CloseDate.setText("");
                Remarks.setText("");
                ObservationTypeSpinner.setSelection(0);
                InspectionTypeSpinner.setSelection(0);
                TargetDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                CloseDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                clearCloseDate = false;
                clearTargetDate = false;


                DATE3 = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        targetDateTwo.setText(sdf.format(myCalendar.getTime()));
                    }

                };
                DATE4 = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        closeDateTwo.setText(sdf.format(myCalendar.getTime()));
                    }

                };

                targetDateTwo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(ObservationSecond.this, DATE3, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });
                closeDateTwo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(ObservationSecond.this, DATE4, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });


                Toast.makeText(ObservationSecond.this, "Observation Added", Toast.LENGTH_SHORT).show();
                dynamicLayout.addView(views);
                LinearLayout btnDelAccount = (LinearLayout) views.findViewById(R.id.del_layout_Second);
                btnDelAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(ObservationSecond.this, "Deleted!", Toast.LENGTH_SHORT).show();
                        dynamicLayout.removeView(views);
                    }
                });
            } else if (is_internal.equals("0")) {
                addObservation(null);
            }
        });


        if (is_internal.equals("0")) {

            ObservationTypeText.setVisibility(View.GONE);
            ObservationTypeSpinner.setVisibility(View.GONE);
            InspectionTypeText.setVisibility(View.GONE);
            InspectionTypeSpinner.setVisibility(View.GONE);

        } else if (is_internal.equals("1")) {
            DepartmentText.setVisibility(View.GONE);
            DepartmentSpinner.setVisibility(View.GONE);

        }
        btnSubmitObservationReport = findViewById(R.id.btnSubmitObservationReport);
        setListener();

        restoreObservationData();
    }

    private List<String> newObservationIds;

    @Override
    public void onBackPressed() {
        if (!isSubject)
            if (newObservationIds != null && newObservationIds.size() > 0) {
                for (String id : newObservationIds)
                    databaseHelperDepartmentId.removeObservationById(id);
            }
        super.onBackPressed();
    }

    private void addObservation(final SaveObservationDTO item) {
        final View views = getLayoutInflater().inflate(R.layout.add_technical_new_observation, null);
        Spinner Department;
        final EditText Tdate, Cdate;
        EditText Remark;
        LinearLayout delLayout;
        boolean clearTdate = false;
        boolean clearCdate = false;
        Department = (Spinner) views.findViewById(R.id.AddNewType);
        Tdate = (EditText) views.findViewById(R.id.AddDateOb);
        Cdate = (EditText) views.findViewById(R.id.AddCloseDateSet);
        Remark = (EditText) views.findViewById(R.id.Add_Remarks_Set);
        delLayout = (LinearLayout) views.findViewById(R.id.del_layout_technical);

        ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.nationalityr_item_view, ObservationDepartmentArray);
        adapter.setDropDownViewResource(R.layout.nationalityr_item_view);
        Department.setAdapter(adapter);
        if (item == null) {
            int pos = Department_Spinner.getSelectedItemPosition();
            Department.setSelection(pos);

            Tdate.setText(TargetDate.getText().toString());
            Cdate.setText(CloseDate.getText().toString());
            Remark.setText(Remarks.getText().toString());

            String DepartmentName = Department_Spinner.getSelectedItem().toString();
            int DepartmentId = databaseHelperDepartmentId.getDepartmentId(DepartmentName);
            SaveObservationDTO info = new SaveObservationDTO();
            info.setId(UUID.randomUUID().toString());
            info.setChecklistUUID(checklistUUID);
            info.setTargetDate(TargetDate.getText().toString());
            info.setClosedDate(CloseDate.getText().toString());
            info.setRemark(Remarks.getText().toString());
            info.setDepartmentId(DepartmentId);
            info.setObservationType("null");
            info.setInspectionType("null");
            newObservationIds.add(info.getId());
            databaseHelperDepartmentId.addSaveObservation(info);
        } else {
            String departmentName = databaseHelperDepartmentId.getDepartmentName(item.getDepartmentId());
            for (int i = 0; i < ObservationDepartmentArray.size(); i++) {
                if (departmentName.equals(ObservationDepartmentArray.get(i))) {
                    Department.setSelection(i);
                }
            }
            if (!TextUtils.isEmpty(item.getTargetDate()))
                Tdate.setText(item.getTargetDate());
            if (!TextUtils.isEmpty(item.getClosedDate()))
                Cdate.setText(item.getClosedDate());
            if (!TextUtils.isEmpty(item.getRemark()))
                Remark.setText(item.getRemark());
        }

        Department_Spinner.setSelection(0);
        TargetDate.setText("");
        CloseDate.setText("");
        Remarks.setText("");
        TargetDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
        CloseDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);

        clearCloseDate = false;
        clearTargetDate = false;


        DATE3 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Tdate.setText(sdf.format(myCalendar.getTime()));

            }

        };
        DATE4 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Cdate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        Tdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new DatePickerDialog(ObservationSecond.this, DATE3, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        Cdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ObservationSecond.this, DATE4, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dynamicLayout.addView(views);

        delLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ObservationSecond.this, "Deleted!", Toast.LENGTH_SHORT).show();
                dynamicLayout.removeView(views);
                if (item != null)
                    databaseHelperDepartmentId.removeObservationById(item.getId());
            }
        });
    }

    private void setListener() {
        btnSubmitObservationReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TargetDate.getText().equals("DD/MM/YYYY")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the target date");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else if (CloseDate.getText().equals("DD/MM/YYYY")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please select the close date");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    isSubject = true;
                    try {
                        if (!info.getTargetDate().equals("DD/MM/YYYY") && !CloseDate.getText().equals("DD/MM/YYYY")) {
                            databaseHelperDepartmentId.addSaveObservation(info);
                            info = new SaveObservationDTO();
                        }
                    } catch (Exception ignore) {

                    }
                    ObservationSecond.this.onBackPressed();
                }
            }
        });
    }

    private boolean isSubject = false;

    private void restoreObservationData() {
        DatabaseHelperAdmin dbHelber = new DatabaseHelperAdmin(getApplicationContext());
        ArrayList<SaveObservationDTO> obsDto = dbHelber.getObservation(checklistUUID);
        if (obsDto != null && obsDto.size() > 0) {
            for (SaveObservationDTO item : obsDto) {
                addObservation(item);
            }
        }
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        TargetDate.setText(sdf.format(myCalendar.getTime()));
        info.setTargetDate(TargetDate.getText().toString());
    }

    private void updateLabel1() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        CloseDate.setText(sdf.format(myCalendar.getTime()));
        info.setClosedDate(CloseDate.getText().toString());
    }

    private void initView() {
        DepartmentText = (TextView) findViewById(R.id.DepartmentText);
        DepartmentSpinner = (Spinner) findViewById(R.id.Department_Spinner);
        ObservationTypeText = (TextView) findViewById(R.id.ObservationTypeText);
        InspectionTypeText = (TextView) findViewById(R.id.InspectionTypeText);
        ObservationTypeSpinner = (Spinner) findViewById(R.id.Observation_Type_Spinner);
        InspectionTypeSpinner = (Spinner) findViewById(R.id.Inspection_type_Spinner);
        TargetDateObservationSecond = (EditText) findViewById(R.id.TargetDateObservationSecond);
        CloseDateObservationSecond = (EditText) findViewById(R.id.CloseDateObservationSecond);
        RemObSecond = (EditText) findViewById(R.id.Rem_Ob_Second);
        addNewObservationSecond = (CircleImageView) findViewById(R.id.addNewObservationSecond);
        dynamicLayoutObservationSecond = (LinearLayout) findViewById(R.id.dynamicLayoutObservationSecond);
    }

    private class MyArrayAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MyArrayAdapter(ObservationSecond con) {
            // TODO Auto-generated constructor stub
            mInflater = LayoutInflater.from(con);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return COUNTRIES.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final LoginRequestActivity.ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.spinner_observation, null);
                holder = new LoginRequestActivity.ListContent();

                holder.name = (TextView) v.findViewById(R.id.textView1);

                v.setTag(holder);
            } else {

                holder = (LoginRequestActivity.ListContent) v.getTag();
            }

            //    holder.name.setTypeface(myFont);
            holder.name.setText("" + COUNTRIES[position]);

            return v;
        }

    }

    static class ListContent {

        TextView name;

    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
