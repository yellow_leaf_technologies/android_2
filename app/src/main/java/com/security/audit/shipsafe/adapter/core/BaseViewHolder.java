package com.security.audit.shipsafe.adapter.core;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

public abstract class BaseViewHolder<T> extends LinearLayout implements View.OnClickListener {

    protected OnItemClickListener<T> onItemClickListener;

    public BaseViewHolder(View view, OnItemClickListener<T> onItemClickListener) {
        super(view.getContext());
        this.onItemClickListener = onItemClickListener;
        this.addView(view);
        this.setLayoutParams(
                new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.MATCH_PARENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT));
        initUI();
        setListener();
    }

    protected abstract void initUI();

    protected void setListener() {
    }

    @Override
    public void onClick(View view) {
    }

    protected abstract void bind(T t, int position);
}