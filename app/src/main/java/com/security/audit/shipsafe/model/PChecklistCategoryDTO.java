package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PChecklistCategoryDTO implements Serializable {

    int status;
    String message;
    CCat data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CCat getData() {
        return data;
    }

    public void setData(CCat data) {
        this.data = data;
    }


    public class CCat {
        List<Checklistcategories> checklistCategories;

        public List<Checklistcategories> getChecklistCategories() {
            return checklistCategories;
        }

        public void setChecklistCategories(List<Checklistcategories> checklistCategories) {
            this.checklistCategories = checklistCategories;
        }

        public class Checklistcategories {

            int type__id;
            String type__name;
            String order;
            int id;
            String name;

            public int getType__id() {
                return type__id;
            }

            public void setType__id(int type__id) {
                this.type__id = type__id;
            }

            public String getType__name() {
                return type__name;
            }

            public void setType__name(String type__name) {
                this.type__name = type__name;
            }

            public String getOrder() {
                return order;
            }

            public void setOrder(String order) {
                this.order = order;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }


    }
}