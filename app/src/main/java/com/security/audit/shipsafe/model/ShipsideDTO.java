package com.security.audit.shipsafe.model;

public class ShipsideDTO {

    int ID;
    String shipsideName;
    String createdAt;
    String updatedAt;
    int companyId;
    int updatedBy;
    int checktype;
    int orderno;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getShipsideName() {
        return shipsideName;
    }

    public void setShipsideName(String shipsideName) {
        this.shipsideName = shipsideName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public int getChecktype() {
        return checktype;
    }

    public void setChecktype(int checktype) {
        this.checktype = checktype;
    }

    public int getOrderno() {
        return orderno;
    }

    public void setOrderno(int orderno) {
        this.orderno = orderno;
    }
}
