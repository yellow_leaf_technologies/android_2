package com.security.audit.shipsafe.adapter.core;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, V extends View>
        extends RecyclerView.Adapter<BaseViewWrapper<V>> {

    protected List<T> items = new ArrayList<>();

    protected OnItemClickListener<T> clickListener;

    @Override
    public BaseViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BaseViewWrapper<>(onCreateItemView(parent, viewType));
    }

    @Override
    public int getItemCount() {
        if (items == null) items = new ArrayList<>();
        return items.size();
    }

    public void setItems(List<T> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    public void clearItems() {
        this.items = new ArrayList<>();
        this.notifyDataSetChanged();
    }

    public void setClickListener(OnItemClickListener<T> clickListener) {
        this.clickListener = clickListener;
    }

    public List<T> getItems() {
        return items;
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);
}

