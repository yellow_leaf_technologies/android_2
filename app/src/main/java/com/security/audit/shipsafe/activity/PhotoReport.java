package com.security.audit.shipsafe.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.asksira.bsimagepicker.BSImagePicker;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.ShipSideAdapter;
import com.security.audit.shipsafe.model.ShipSideCheckList;
import com.security.audit.shipsafe.model.ShipsideName;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PhotoReport extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener {
    Context mContext;
    RecyclerView PhotoReportRecycler;
    DatabaseHelperAdmin databaseHelperAdmin;
    List<ShipsideName> Dto;
    ShipSideAdapter adapter;
    PhotoReport photoReport;
    LinearLayout ItemView = null;
    LinearLayout btnSubmitPhotoReport;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_photo_report);
        getSupportActionBar().setTitle("Photo Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        photoReport = this;
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        PhotoReportRecycler = (RecyclerView) findViewById(R.id.PhotoReportRecycler);
        btnSubmitPhotoReport = findViewById(R.id.btnSubmitPhotoReport);
        Dto = databaseHelperAdmin.getAllShipSide();

        List<ShipSideCheckList> checkLists = databaseHelperAdmin.getAllShipsideCheckList(getIntent().getStringExtra("Checklist_UUID"));
        for (ShipsideName item : Dto) {
            for (ShipSideCheckList sideCheckList : checkLists) {
                if (item.getId() == Integer.parseInt(sideCheckList.getShipsideId())) {
                    item.setRemarks(sideCheckList.getRemarks());
                    item.setShipSideCheckListID(sideCheckList.getId());
                    if (item.getImages() == null) {
                        List<String> images = databaseHelperAdmin.getPhotoReportCheckListImage(String.valueOf(sideCheckList.getId()), getIntent().getStringExtra("Checklist_UUID"));
                        if (images != null && images.size() > 0) {
                            for (String image : images) {
                                item.setImage(Uri.parse(image));
                            }
                        }
                    }
                }
            }
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        PhotoReportRecycler.setLayoutManager(layoutManager);
        PhotoReportRecycler.setItemViewCacheSize(Dto.size());
        adapter = new ShipSideAdapter(mContext, Dto, photoReport);
        PhotoReportRecycler.setAdapter(adapter);

        setListener();
    }

    private void setListener() {
        btnSubmitPhotoReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCheckInputData()) {
                    List<ShipSideCheckList> shipSideCheckList = new ArrayList<>();
                    for (ShipsideName item : Dto) {
                        ShipSideCheckList sideCheckList = new ShipSideCheckList();
                        boolean isAdding = false;
                        if (item.getRemarks() != null && item.getRemarks().length() > 0) {
                            if (item.getShipSideCheckListID() == null)
                                sideCheckList.setId(UUID.randomUUID().toString());
                            else
                                sideCheckList.setId(item.getShipSideCheckListID());
                            sideCheckList.setShipsideId(String.valueOf(item.getId()));
                            sideCheckList.setCheckListId(getIntent().getStringExtra("Checklist_UUID"));
                            sideCheckList.setRemarks(item.getRemarks());
                            isAdding = true;
                        }
                        if (item.getImages() != null && item.getImages().size() > 0) {
                            sideCheckList.setSetImages(item.getImages());
                            isAdding = true;
                        }
                        if (isAdding) {
                            shipSideCheckList.add(sideCheckList);
                        }

                    }
                    databaseHelperAdmin.addShipSideCheckList(shipSideCheckList);
                    for (ShipSideCheckList item : shipSideCheckList) {
                        databaseHelperAdmin.removePhotoReportCheckListImage(item.getId(), item.getCheckListId());
                        if (item.getSetImages() != null && item.getSetImages().size() > 0) {
                            for (Uri image : item.getSetImages()) {
                                databaseHelperAdmin.addPhotoReportCheckListImage(item.getId(), item.getCheckListId(), image);
                            }
                        }
                    }
                    PhotoReport.this.onBackPressed();
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Please input all data");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });
    }

    private boolean isCheckInputData() {
        for (ShipsideName item : Dto) {
            if (item.getRemarks() != null && item.getRemarks().length() > 0) {
                return true;
            }
        }
        return false;
    }

    public void CallSingleSelection(int position, LinearLayout AddedLayout) {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
        ItemView = AddedLayout;
        this.position = position;
    }


    public void CallMultipleSelection(int position, LinearLayout AddedLayout) {
        this.position = position;
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.security.audit.shipsafe.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(10)
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
        ItemView = AddedLayout;
        this.position = position;
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList) {
        adapter.setPhotos(uriList, ItemView, position, false);
    }

    @Override
    public void onSingleImageSelected(Uri uri) {
        List<Uri> itemList = new ArrayList<>();
        itemList.add(uri);
        adapter.setPhotos(itemList, ItemView, position, false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
