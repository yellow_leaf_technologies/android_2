package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PChecklistTypeDTO implements Serializable {
    int status;
    String message;
    CType data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CType getData() {
        return data;
    }

    public void setData(CType data) {
        this.data = data;
    }

    public class CType {
        public List<ChecklistTypeData> getChecklistTypes() {
            return checklistTypes;
        }
        List<ChecklistTypeData>checklistTypes;
        public class ChecklistTypeData {

            boolean is_internal;
            int id;
            String name;

            public boolean isIs_internal() {
                return is_internal;
            }

            public void setIs_internal(boolean is_internal) {
                this.is_internal = is_internal;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}