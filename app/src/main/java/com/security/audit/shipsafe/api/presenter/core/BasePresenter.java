package com.security.audit.shipsafe.api.presenter.core;

import android.content.Context;

import com.security.audit.shipsafe.api.view.ViewPresenter;
import com.security.audit.shipsafe.utils.InternetConnectionObserver;

public abstract class BasePresenter {
    private Context context;
    protected ViewPresenter view;

    protected RefreshEndpoint endpoint;

    protected static final int NO_AUTH = 401;

    public BasePresenter(Context context, ViewPresenter view) {
        this.context = context;
        this.view = view;
        this.initInternetConnectionObserver();
    }

    public Context getContext() {
        return context;
    }


    protected void onInternetConnectionStateChanged(boolean isConnected) {
    }

    private void initInternetConnectionObserver() {
        InternetConnectionObserver.getInstance(context)
                .addListener(
                        state -> {
                            boolean isConnected = false;
                            switch (state) {
                                case WIFI_CONNECTED:
                                case MOBILE_NETWORK_CONNECTED: {
                                    isConnected = true;
                                    break;
                                }
                                case DISCONNECTED: {
                                    break;
                                }
                            }
                            onInternetConnectionStateChanged(isConnected);
                        });
    }

    protected interface RefreshEndpoint {
        void onSuccess();

        void onError();
    }
}

