package com.security.audit.shipsafe.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.extra.UtilsDialog;
import com.security.audit.shipsafe.model.InspectionTypeDTO;
import com.security.audit.shipsafe.model.ObservationDepartmentDTO;
import com.security.audit.shipsafe.model.ObservationTypeDTO;
import com.security.audit.shipsafe.model.PChecklistCategoryDTO;
import com.security.audit.shipsafe.model.PChecklistQuestion;
import com.security.audit.shipsafe.model.PChecklistTypeDTO;
import com.security.audit.shipsafe.model.PDesignationListDTO;
import com.security.audit.shipsafe.model.PNationalityDTO;
import com.security.audit.shipsafe.model.PShipSideDTO;
import com.security.audit.shipsafe.model.PVesselListDTO;
import com.security.audit.shipsafe.model.PortListDTO;
import com.security.audit.shipsafe.model.UserInfo;
import com.security.audit.shipsafe.model.UserLoginDTO;
import com.security.audit.shipsafe.model.Value;
import com.security.audit.shipsafe.sharedpreference.SaveSharedPreference;
import com.security.audit.shipsafe.sigleton.AppSingleton;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;
import com.security.audit.shipsafe.web_services.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Login extends Activity {

    TextView logiText, rememberText, companytext;
    EditText et_username, et_password;
    Button btnSignUp;
    ImageView btnlinkpage;
    Context mContext;
    String CompanyName, RequestCode;
    int CompanyId;
    CheckBox RememberMe;
    DatabaseHelperAdmin databaseHelperAdmin, databaseHelper, databaseHelper2, databaseHelperCategory1, databaseHelperCategory2, databaseHelperNationality1, databaseHelperNationality2, databaseHelperRank1, databaseHelperRank2, databaseHelperUserInfo, databaseHelperQuestions,
            databaseHelperUserInformation, databaseHelperVessel, databaseHelperQuestions1, databaseHelperAdminShipside, databaseHelperAdminShipSide2, databaseHelperAddPort, databaseHelperDeleteTable, databaseHelperInspectionType1, databaseHelperInspectionType2, databaseHelperObservationType1, databaseHelperObservationType2,
    databaseHelperObservationDepartment1,databaseHelperObservationDepartment2;
    ProgressDialog progressDialog;
    Dialog progress;
    Typeface face;
    int res = 0;
    String Mac;
    int Count = 0;
    String companyId=null;
    int Response = 0;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        mContext = this;
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        databaseHelper = new DatabaseHelperAdmin(mContext);
        databaseHelper2 = new DatabaseHelperAdmin(mContext);
        databaseHelperCategory1 = new DatabaseHelperAdmin(mContext);
        databaseHelperCategory2 = new DatabaseHelperAdmin(mContext);
        databaseHelperNationality1 = new DatabaseHelperAdmin(mContext);
        databaseHelperNationality2 = new DatabaseHelperAdmin(mContext);
        databaseHelperRank1 = new DatabaseHelperAdmin(mContext);
        databaseHelperRank2 = new DatabaseHelperAdmin(mContext);
        databaseHelperUserInfo = new DatabaseHelperAdmin(mContext);
        databaseHelperQuestions = new DatabaseHelperAdmin(mContext);
        databaseHelperVessel = new DatabaseHelperAdmin(mContext);
        databaseHelperQuestions1 = new DatabaseHelperAdmin(mContext);
        databaseHelperAdminShipside = new DatabaseHelperAdmin(mContext);
        databaseHelperAdminShipSide2 = new DatabaseHelperAdmin(mContext);

        databaseHelperUserInformation = new DatabaseHelperAdmin(mContext);
        databaseHelperAddPort = new DatabaseHelperAdmin(mContext);
        databaseHelperDeleteTable = new DatabaseHelperAdmin(mContext);
        databaseHelperInspectionType1 = new DatabaseHelperAdmin(mContext);
        databaseHelperInspectionType2 = new DatabaseHelperAdmin(mContext);
        databaseHelperObservationType1 = new DatabaseHelperAdmin(mContext);
        databaseHelperObservationType2 = new DatabaseHelperAdmin(mContext);
        databaseHelperObservationDepartment1=new DatabaseHelperAdmin(mContext);
        databaseHelperObservationDepartment2=new DatabaseHelperAdmin(mContext);
        companyId = SaveSharedPreference.getCompanyIds(mContext);

        //   CompanyName= SaveSharedPreference.getCompanyName(mContext);
        RequestCode = SaveSharedPreference.getRequestCode(mContext);
        Mac = SaveSharedPreference.getMacId(mContext);
        //  CompanyId=databaseHelperAdmin.getCompanyid(CompanyName,RequestCode);

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Please Wait..");

        //  MakeToast(mContext,String.valueOf(CompanyId));

        RememberMe = (CheckBox) findViewById(R.id.keepmeloggedin);
        // SaveSharedPreference.setConnectionType(mContext,"Not Connected");

        //logiText = (TextView) findViewById(R.id.logintext);
        et_username = (EditText) findViewById(R.id.username);
        et_password = (EditText) findViewById(R.id.password);
        btnSignUp = (Button) findViewById(R.id.btnSignin);
        btnlinkpage = (ImageView) findViewById(R.id.linkweb);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "railway_semibold.ttf");
        // logiText.setTypeface(custom_font);
        Typeface custom = Typeface.createFromAsset(getAssets(), "rale_regular.ttf");
        et_username.setTypeface(custom);
        et_password.setTypeface(custom);

        face = Typeface.createFromAsset(getAssets(), "rale_regular.ttf");

        Typeface remember = Typeface.createFromAsset(getAssets(), "cen_regular.ttf");
        rememberText = (TextView) findViewById(R.id.remembertext);
        rememberText.setTypeface(remember);
        btnSignUp.setTypeface(remember);


        btnlinkpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.topcubit.com")));

            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et_username.getText().toString();
                String pass = et_password.getText().toString();
                if (TextUtils.isEmpty(name)) {
                    et_username.setError("Please Enter User Name");
                    et_username.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    et_password.setError("Please Enter Password");
                    et_password.requestFocus();
                    return;
                }

                if (SaveSharedPreference.getConnectionType(mContext).equals("Connected")) {
                    verifyFromSQLite();
                } else {


                    if (isNetworkAvailable(Login.this) == true) {

                        LoginApp(name, pass, Mac,companyId);

                    } else {

                        MakeToast(mContext, "Please Connect the device with the Internet!");

                    }


                }
                // verifyFromSQLite();
            }
        });

        companytext = (TextView) findViewById(R.id.companynamelogin);
        companytext.setTypeface(custom_font);
        companytext.setText("Audit System");

    }

    public void DataCalling()

    {



        boolean check3 = databaseHelper.isChecklistTypeEmpty();
        if (check3 == false) {
            fetchChecklistType(companyId);

        } else if (check3 == true) {
            //  MakeToast(mContext, "Not Empty");

        }

        boolean check9 = databaseHelperNationality1.isPortListEmpty();
        if (check9 == false) {

            fetchPortList(companyId);


        } else if (check9 == true) {
            //MakeToast(mContext, "Not Empty");

        }
        boolean check2 = databaseHelper2.isVesselNameEmpty();
        if (check2 == false) {

            fetchVesselList(companyId);

        } else if (check2 == true) {

        }

        boolean check5 = databaseHelperNationality1.isNationalityEmpty();
        if (check5 == false) {

            fetchNationality(companyId);


        } else if (check5 == true) {
            //MakeToast(mContext, "Not Empty");

        }

        boolean check6 = databaseHelperRank1.isRankEmpty();
        if (check6 == false) {

            fetchDesignation(companyId);

        } else if (check6 == true) {
            //MakeToast(mContext, "Not Empty");

        }

        boolean check4 = databaseHelperCategory1.isCategoryEmpty();
        if (check4 == false) {
            fetchChecklistCategory(companyId);


        } else if (check4 == true) {
            //MakeToast(mContext, "Not Empty");

        }


        boolean check1 = databaseHelperQuestions1.isQuestionEmpty();
        if (check1 == false) {

            fetchQuestions(companyId);

        } else if (check1 == true) {

        }


        boolean check7 = databaseHelperAdminShipside.isShipSideEmpty();
        if (check7 == false) {

            fetchShipSide(companyId);


        } else if (check7 == true) {

        }

        boolean check8 = databaseHelperInspectionType1.isInspectionTypeEmpty();
        if (check8 == false) {

            fetchInspectionTypes(companyId);

        } else if (check8 == true) {

        }


        boolean check10 = databaseHelperObservationType1.isObservationTypeEmpty();
        if (check10 == false) {

            fetchObservationTypes(companyId);

        } else if (check10 == true) {

        }

        boolean check11=databaseHelperObservationDepartment1.isObservationDepartmentEmpty();
        if(check11==false)
        {
            fetchObservationDepartment(companyId);

        }
        else if(check11==true)
        {

        }


    }


    private void LoginApp(final String email, final String password, final String device_id,final String companyId) {
        //showDialog();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_USER_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                //  hideDialog();

                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = null;
                    try {

                        ArrayList<UserLoginDTO> login = new ArrayList<>();
                        jsonObject = new JSONObject(response);
                        UserLoginDTO userLoginDTO;
                        userLoginDTO = gson.fromJson(jsonObject.toString(), UserLoginDTO.class);
                       /* JSONObject containerObject = new JSONObject(response);
                        JSONObject jObject = containerObject.getJSONObject("data");
                        //has method
                        if (jObject.has("companyId")) {//get Value of video
                            SaveSharedPreference.setCompanyIds(mContext, userLoginDTO.getData().getCompanyId());
                        } else {
                            SaveSharedPreference.setCompanyIds(mContext, null);
                        }*/
                        int status = userLoginDTO.getStatus();

                        if (status == 0) {
                            String Message = userLoginDTO.getMessage();
                            MakeToast(mContext, Message);
                        } else if (status == 1) {
                            userLoginDTO.setPassword(password);
                            login = new ArrayList<>();
                            login.add(userLoginDTO);

                            if (login != null && login.size() > 0) {
                                setUser(login);
                                SaveSharedPreference.setConnectionType(mContext, "Connected");
                            } else {
                                MakeToast(mContext, " item null");
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                Log.d("Failed", "Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("device_id", device_id);
                params.put("companyId", companyId);


                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    public void setUser(final ArrayList<UserLoginDTO> itemlist) {
        databaseHelperAdmin.add_app_user(itemlist);

        if (RememberMe.isChecked()) {
            SaveSharedPreference.setStatus(mContext, "Login");
        }
        List<UserInfo> userInfo = new ArrayList<>();
        userInfo = databaseHelperUserInformation.getUserInfo(et_username.getText().toString().trim());
        String fname = userInfo.get(0).getFName();
        String lname = userInfo.get(0).getLName();
        String uname = fname + " " + lname;
        String uemail = userInfo.get(0).getEmail();
        SaveSharedPreference.setUserName(mContext, uname);
        SaveSharedPreference.setUserEmail(mContext, uemail);

        DataCalling();

        Intent i2 = new Intent(mContext, HomeActivity.class);
        startActivity(i2);
        finish();
    }


    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void verifyFromSQLite() {

        if (databaseHelper.checkUser(et_username.getText().toString().trim(), et_password.getText().toString().trim())) {


            List<UserInfo> userInfo = new ArrayList<>();
            userInfo = databaseHelperUserInformation.getUserInfo(et_username.getText().toString().trim());


            if (RememberMe.isChecked()) {
                SaveSharedPreference.setStatus(mContext, "Login");
            }

            String fname = userInfo.get(0).getFName();
            String lname = userInfo.get(0).getLName();
            String uname = fname + " " + lname;
            String uemail = userInfo.get(0).getEmail();
            SaveSharedPreference.setUserName(mContext, uname);
            SaveSharedPreference.setUserEmail(mContext, uemail);
            Intent i2 = new Intent(mContext, HomeActivity.class);
            startActivity(i2);
            finish();

        } else {


            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.app_name);
            builder.setMessage("User details not found offline. Please fetch data online. Make sure the device should be connected with the internet while fetching data online.")
                    .setCancelable(false)
                    .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }
                    })
                    .setNegativeButton("Check Online", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            if (isNetworkAvailable(Login.this) == true) {
                                //delete all the table and fetecg data again
                                databaseHelperDeleteTable.deleteTableData();
                                LoginApp(et_username.getText().toString().trim(), et_password.getText().toString().trim(), Mac,companyId);

                            } else {

                                MakeToast(mContext, "Please connect the device with the internet for fetching data online!");

                            }
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            // Toast.makeText(mContext, "Incorrect Email or Password or User data not!", Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchChecklistType(final String companyId) {
        showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_CHECKLIST_TYPE, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //      hideDialog();
                //   dismissProgress();
                String json = response.toString();

                try {


                    Gson gson = new Gson();
                    PChecklistTypeDTO pChecklistTypeDTOS = gson.fromJson(json, PChecklistTypeDTO.class);
                    int status = pChecklistTypeDTOS.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pChecklistTypeDTOS.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {


                        List<PChecklistTypeDTO.CType.ChecklistTypeData> checklistTypeData = pChecklistTypeDTOS.getData().getChecklistTypes();

                        if (checklistTypeData != null && checklistTypeData.size() > 0) {
                            setChecklistType(checklistTypeData);
                        }


                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchNationality(final String companyId) {
        //showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_NATIONALITY, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                // hideDialog();
                //  dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PNationalityDTO pNationalityDTO = gson.fromJson(json, PNationalityDTO.class);
                    int status = pNationalityDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pNationalityDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PNationalityDTO.NList.NationalityList> nationalityList = pNationalityDTO.getData().getNationalityList();

                        if (nationalityList != null && nationalityList.size() > 0) {
                            setNationalityList(nationalityList);
                        }

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }

    private void fetchDesignation(final String companyId) {
        //    showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_DESIGNATION, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //  hideDialog();
                // dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PDesignationListDTO pDesignationListDTO = gson.fromJson(json, PDesignationListDTO.class);
                    int status = pDesignationListDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pDesignationListDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PDesignationListDTO.DList.DesignationList> designationListData = pDesignationListDTO.getData().getDesignationList();
                        if (designationListData != null && designationListData.size() > 0) {
                            setDesignation(designationListData);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchChecklistCategory(final String companyId) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_CHECKLIST_CATEGORY, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                // hideDialog();
                //    dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PChecklistCategoryDTO pChecklistCategoryDTO = gson.fromJson(json, PChecklistCategoryDTO.class);
                    int status = pChecklistCategoryDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pChecklistCategoryDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PChecklistCategoryDTO.CCat.Checklistcategories> checklistcategories = pChecklistCategoryDTO.getData().getChecklistCategories();
                        if (checklistcategories != null && checklistcategories.size() > 0) {
                            setCategories(checklistcategories);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchVesselList(final String companyId) {
        // showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_VESSEL_LIST, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                // hideDialog();
                // dismissProgress();
                String json = response.toString();

                try {
                    Gson gson = new Gson();
                    PVesselListDTO pVesselListDTO = gson.fromJson(json, PVesselListDTO.class);
                    int status = pVesselListDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pVesselListDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PVesselListDTO.Datum.VesselList> vesselList = pVesselListDTO.getData().getVesselList();

                        if (vesselList != null && vesselList.size() > 0) {
                            setVesselList(vesselList);
                        }

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchPortList(final String companyId) {
        // showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_PORT_LIST, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                // hideDialog();
                // dismissProgress();

                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PortListDTO portListDTO = gson.fromJson(json, PortListDTO.class);
                    int status = portListDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = portListDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {
                        List<PortListDTO.PList.PortList> portList = portListDTO.getData().getPortList();

                        if (portList != null && portList.size() > 0) {
                            setPortList(portList);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchQuestions(final String companyId) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_CHECKLIST_QUESTIONS, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PChecklistQuestion pChecklistQuestion = gson.fromJson(json, PChecklistQuestion.class);
                    int status = pChecklistQuestion.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pChecklistQuestion.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PChecklistQuestion.CQData.ChecklistQuestions> checklistQuestions = pChecklistQuestion.getData().getChecklistQuestions();
                        if (checklistQuestions != null && checklistQuestions.size() > 0) {
                            setQuestions(checklistQuestions);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchShipSide(final String companyId) {
        // showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_SHIPSIDE, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //       hideDialog();
                //     dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    PShipSideDTO pShipSideDTO = gson.fromJson(json, PShipSideDTO.class);
                    int status = pShipSideDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = pShipSideDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<PShipSideDTO.SSide.ShipSide> shipSideList = pShipSideDTO.getData().getShipside();

                        if (shipSideList != null && shipSideList.size() > 0) {
                            setShipSide(shipSideList);
                        }

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchInspectionTypes(final String companyId) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_INSPECTION_TYPE, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    InspectionTypeDTO inspectionTypeDTO = gson.fromJson(json, InspectionTypeDTO.class);
                    int status = inspectionTypeDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = inspectionTypeDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<InspectionTypeDTO.InsData.InsTypeList> insTypeList = inspectionTypeDTO.getData().getInstypeList();
                        if (insTypeList != null && insTypeList.size() > 0) {
                            setInspectionType(insTypeList);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }


    private void fetchObservationTypes(final String companyId) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_OBSERVATION_TYPE, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    ObservationTypeDTO observationTypeDTO = gson.fromJson(json, ObservationTypeDTO.class);
                    int status = observationTypeDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = observationTypeDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<ObservationTypeDTO.ObsData.ObsTypeList> obsTypeList = observationTypeDTO.getData().getObstypeList();
                        if (obsTypeList != null && obsTypeList.size() > 0) {
                            setObservationType(obsTypeList);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }



    private void fetchObservationDepartment(final String companyId) {
        //  showProgress();
        String cancel_req_tag = "Loading Item Table";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServiceCall.URL_OBSERVATION_DEPARTMENT, new Response.Listener<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(final String response) {
                //hideDialog();
                //dismissProgress();
                String json = response.toString();

                try {

                    Gson gson = new Gson();
                    ObservationDepartmentDTO observationDepartmentDTO = gson.fromJson(json, ObservationDepartmentDTO.class);
                    int status = observationDepartmentDTO.getStatus();
                    Response = Response + status;
                    if (Response == 11) {
                        dismissProgress();
                    }
                    if (status == 0) {
                        String Message = observationDepartmentDTO.getMessage();
                        MakeToast(mContext, Message);
                    } else if (status == 1) {

                        List<ObservationDepartmentDTO.OData.ObservationDepartment> observationDepartmentList = observationDepartmentDTO.getData().getObservationDepartment();
                        if (observationDepartmentList != null && observationDepartmentList.size() > 0) {
                            setObservationDepartment(observationDepartmentList);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideDialog();
                dismissProgress();
                Log.d("Failed","Failed");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyId", companyId);
                return params;
            }
        };
        AppSingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(strReq, cancel_req_tag);
    }

    public void setObservationType(List<ObservationTypeDTO.ObsData.ObsTypeList> pDTO) {

        databaseHelperObservationType2.add_Observation_Type(pDTO);
    }


    public void setObservationDepartment(List<ObservationDepartmentDTO.OData.ObservationDepartment> pDTO) {

        databaseHelperObservationType2.add_Observation_Department(pDTO);
    }

    public void setInspectionType(List<InspectionTypeDTO.InsData.InsTypeList> pDTO) {

        databaseHelperInspectionType2.add_Inspection_Type(pDTO);
    }


    public void setShipSide(List<PShipSideDTO.SSide.ShipSide> pDTO) {

        databaseHelperAdminShipSide2.add_shipside(pDTO);
    }


    public void setPortList(List<PortListDTO.PList.PortList> pDTO) {

        databaseHelperAddPort.add_Port(pDTO);
    }


    public void setChecklistType(List<PChecklistTypeDTO.CType.ChecklistTypeData> pDTO) {

        databaseHelperCategory2.add_CType(pDTO);
    }

    public void setVesselList(List<PVesselListDTO.Datum.VesselList> pDTO) {

        databaseHelperVessel.add_Vessel(pDTO);
    }

    public void setNationalityList(List<PNationalityDTO.NList.NationalityList> pDTO) {

        databaseHelperVessel.add_Nationality(pDTO);
    }

    public void setDesignation(List<PDesignationListDTO.DList.DesignationList> pDTO) {

        databaseHelperVessel.add_Designation(pDTO);
    }


    public void setCategories(List<PChecklistCategoryDTO.CCat.Checklistcategories> pDTO) {

        databaseHelperCategory2.add_checklist_category(pDTO);
    }

    public void setQuestions(List<PChecklistQuestion.CQData.ChecklistQuestions> pDTO) {

        databaseHelperQuestions.add_Questions(pDTO);
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }


    private void showProgress() {
        if (progress == null) {
            progress = UtilsDialog.processDialog(mContext, "Loading...", true, face);
            progress.show();
        } else {
            progress.show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void dismissProgress() {
        if (!Login.this.isDestroyed() && progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

}
