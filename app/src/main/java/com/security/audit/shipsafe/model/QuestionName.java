package com.security.audit.shipsafe.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class QuestionName implements Parcelable {
    String info;
    int category__type__id;
    String category__type__name;
    String question;
    int category__id;
    String order;
    String category__name;
    int id;
    String remark;
    String select;
    String date;
    List<Uri> images;
    String questionCheckListId;

    public QuestionName(Parcel in) {
        info = in.readString();
        category__type__id = in.readInt();
        category__type__name = in.readString();
        question = in.readString();
        category__id = in.readInt();
        order = in.readString();
        category__name = in.readString();
        id = in.readInt();
        remark = in.readString();
        select = in.readString();
        date = in.readString();
    }

    public static final Creator<QuestionName> CREATOR = new Creator<QuestionName>() {
        @Override
        public QuestionName createFromParcel(Parcel in) {
            return new QuestionName(in);
        }

        @Override
        public QuestionName[] newArray(int size) {
            return new QuestionName[size];
        }
    };

    public QuestionName() {

    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getCategory__type__id() {
        return category__type__id;
    }

    public void setCategory__type__id(int category__type__id) {
        this.category__type__id = category__type__id;
    }

    public String getCategory__type__name() {
        return category__type__name;
    }

    public void setCategory__type__name(String category__type__name) {
        this.category__type__name = category__type__name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCategory__id() {
        return category__id;
    }

    public void setCategory__id(int category__id) {
        this.category__id = category__id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getCategory__name() {
        return category__name;
    }

    public void setCategory__name(String category__name) {
        this.category__name = category__name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImages(List<Uri> images) {
        if (this.images == null)
            this.images = images;
        else
            this.images.addAll(images);
    }

    public void setImage(Uri item) {
        if (images == null) {
            images = new ArrayList<>();
        }
        images.add(item);
    }

    public List<Uri> getImages() {
        return images;
    }

    public String getQuestionCheckListId() {
        return questionCheckListId;
    }

    public void setQuestionCheckListId(String questionCheckListId) {
        this.questionCheckListId = questionCheckListId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(info);
        dest.writeInt(category__type__id);
        dest.writeString(category__type__name);
        dest.writeString(question);
        dest.writeInt(category__id);
        dest.writeString(order);
        dest.writeString(category__name);
        dest.writeInt(id);
        dest.writeString(remark);
        dest.writeString(select);
        dest.writeString(date);
    }
}

