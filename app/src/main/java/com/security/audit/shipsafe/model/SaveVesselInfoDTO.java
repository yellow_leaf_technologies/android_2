package com.security.audit.shipsafe.model;

public class SaveVesselInfoDTO {

    String checklistid;
    String enddate;
    boolean isdeleted;
    String created_at;
    String startdate;
    int checklisttypeid;
    int fromportid;
    int toportid;
    String updatedon;
    String modified0n;
    int  vessel_id;


    public int getFromportid() {
        return fromportid;
    }

    public void setFromportid(int fromportid) {
        this.fromportid = fromportid;
    }

    public int getToportid() {
        return toportid;
    }

    public void setToportid(int toportid) {
        this.toportid = toportid;
    }

    public String getChecklistid() {
        return checklistid;
    }

    public void setChecklistid(String checklistid) {
        this.checklistid = checklistid;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public boolean isIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public int getChecklisttypeid() {
        return checklisttypeid;
    }

    public void setChecklisttypeid(int checklisttypeid) {
        this.checklisttypeid = checklisttypeid;
    }

    public String getUpdatedon() {
        return updatedon;
    }

    public void setUpdatedon(String updatedon) {
        this.updatedon = updatedon;
    }

    public String getModified0n() {
        return modified0n;
    }

    public void setModified0n(String modified0n) {
        this.modified0n = modified0n;
    }

    public int getVessel_id() {
        return vessel_id;
    }

    public void setVessel_id(int vessel_id) {
        this.vessel_id = vessel_id;
    }
}
