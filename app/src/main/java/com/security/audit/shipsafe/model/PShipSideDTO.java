package com.security.audit.shipsafe.model;

import java.io.Serializable;
import java.util.List;

public class PShipSideDTO implements Serializable {

    int status;
    String message;
    SSide data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SSide getData() {
        return data;
    }

    public void setData(SSide data) {
        this.data = data;
    }

    public class SSide {

        List<ShipSide> shipside;

        public List<ShipSide> getShipside() {
            return shipside;
        }

        public void setShipside(List<ShipSide> shipside) {
            this.shipside = shipside;
        }


        public class ShipSide {

            int checklist_type__id;
            int order;
            String checklist_type__name;
            int id;
            String name;

            public int getChecklist_type__id() {
                return checklist_type__id;
            }

            public void setChecklist_type__id(int checklist_type__id) {
                this.checklist_type__id = checklist_type__id;
            }

            public int getOrder() {
                return order;
            }

            public void setOrder(int order) {
                this.order = order;
            }

            public String getChecklist_type__name() {
                return checklist_type__name;
            }

            public void setChecklist_type__name(String checklist_type__name) {
                this.checklist_type__name = checklist_type__name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }


}
