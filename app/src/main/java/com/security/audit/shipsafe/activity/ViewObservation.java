package com.security.audit.shipsafe.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewObservation extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_view_observation);
        getSupportActionBar().setTitle("View Observation Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*// get the listview
       // expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);*/
    }

    /*
     * Preparing the list data
     */
   /* private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("7941DFD3-483B-4E4D-8434-B2B46D13EE79");
        listDataHeader.add("8041DFD3-483B-4E4D-8434-B2B46D13EE79");
        listDataHeader.add("8141DFD3-483B-4E4D-8434-B2B46D13EE79");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("Vessel Name");
        top250.add("Inspection Type");
        top250.add("Created Date");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("Vessel Name");
        nowShowing.add("Inspection Type");
        nowShowing.add("Created Date");

        List<String> comingSoon = new ArrayList<String>();

        comingSoon.add("Vessel Name");
        comingSoon.add("Inspection Type");
        comingSoon.add("Created Date");

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expListView.setIndicatorBounds(width-GetPixelFromDips(35), width-GetPixelFromDips(5));
        } else {
            expListView.setIndicatorBoundsRelative(width-GetPixelFromDips(35), width-GetPixelFromDips(5));
        }
    }
*/
    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
