package com.security.audit.shipsafe.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.PortSelectionAdapter;
import com.security.audit.shipsafe.model.ObservationInfoDTO;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class Is_Internal_Details extends AppCompatActivity {

    public static EditText SelectPortDetails;
    private EditText MeetingDate;
    private EditText MeetingTime;
    private EditText TBMeethingDate;
    private EditText TBMeethingTime;
    private EditText OMHeldAT;
    private MultiSelectSpinner OMAttendedBy;
    private TextView OMAttendedParticipents;
    private EditText CMHeldAt;
    private MultiSelectSpinner CMAttendedBy;
    private TextView CMAttendedParticipents;
    private Button btnContinueToNext;
    Context mContext;
    DatePickerDialog.OnDateSetListener MeetingDatePicker, TBMeethingDatepicker;
    Calendar myCalendar = Calendar.getInstance();
    DatabaseHelperAdmin databaseHelperAttendedBy;
    boolean clearMeetingDate = false;
    boolean clearMeetingTime = false;
    boolean clearTBMeetingDate = false;
    boolean clearTBMeetingTime = false;
    String ChecklistUUID;
    DatabaseHelperAdmin databaseHelperPortId;
    int VesselId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_is__internal__details);
        getSupportActionBar().setTitle("OC Meetings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        databaseHelperAttendedBy = new DatabaseHelperAdmin(mContext);
        databaseHelperPortId=new DatabaseHelperAdmin(mContext);
        initView();
        ChecklistUUID = getIntent().getStringExtra("Checklist_UUID");

       VesselId=getIntent().getIntExtra("V_ID",0);


        final String is_internal = getIntent().getStringExtra("Is_Internal");
        btnContinueToNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int portId= databaseHelperPortId.getPortId(SelectPortDetails.getText().toString());
                ObservationInfoDTO observationInfoDTO=new ObservationInfoDTO();


                observationInfoDTO.setVesselId(VesselId);
                observationInfoDTO.setPortPlace(portId);
                observationInfoDTO.setReportDate(MeetingDate.getText().toString());
                observationInfoDTO.setReportTime(MeetingTime.getText().toString());
                observationInfoDTO.setAuditorDate(TBMeethingDate.getText().toString());
                observationInfoDTO.setAuditorTime(TBMeethingTime.getText().toString());
                observationInfoDTO.setOpeningHeldAt(OMHeldAT.getText().toString());
                observationInfoDTO.setOpeningAttendedBy(OMAttendedParticipents.getText().toString());
                observationInfoDTO.setClosingHeldAt(CMHeldAt.getText().toString());
                observationInfoDTO.setClosingAttendedBy(CMAttendedParticipents.getText().toString());
                observationInfoDTO.setChecklistUUID(ChecklistUUID);

                databaseHelperAttendedBy.addSaveObservationInfo(observationInfoDTO);
                Intent i = new Intent(mContext, ObservationSecond.class);
                i.putExtra("Is_Internal", is_internal);
                i.putExtra("Checklist_UUID", ChecklistUUID);
                i.putExtra("V_ID", VesselId);
                startActivity(i);
            }
        });


        MeetingDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (clearMeetingDate == true) {
                    MeetingDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                    MeetingDate.setText("DD/MM/YYYY");
                    clearMeetingDate = false;
                } else {

                    new DatePickerDialog(mContext, MeetingDatePicker, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }

            }
        });
        MeetingDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
                clearMeetingDate = true;
                MeetingDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);

            }

        };
        TBMeethingDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (clearTBMeetingDate == true) {
                    TBMeethingDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.calendar_icon, 0);
                    TBMeethingDate.setText("DD/MM/YYYY");
                    clearTBMeetingDate = false;
                } else {

                    new DatePickerDialog(mContext, TBMeethingDatepicker, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        TBMeethingDatepicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();
                clearTBMeetingDate = true;
                TBMeethingDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);
            }

        };
        SelectPortDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, PortSelection.class);
                startActivity(i);
            }
        });
        MeetingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  int hour=0;
                int min=0;
                TimePickerDialog Tp = new TimePickerDialog(mContext,android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                         MeetingTime.setText(String.format("%02d:%02d", hourOfDay, minute));

                    }
                },hour,min,true);
                Tp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Tp.show();*/


                if (clearMeetingTime == true) {
                    MeetingTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.time_icon, 0);
                    MeetingTime.setText("HH:MM");
                    clearMeetingTime = false;
                } else {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            //    MeetingTime.setText( selectedHour + ":" + selectedMinute);
                            MeetingTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

                    clearMeetingTime = true;
                    MeetingTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);

                }


            }
        });
        TBMeethingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (clearTBMeetingTime == true) {
                    TBMeethingTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.time_icon, 0);
                    TBMeethingTime.setText("HH:MM");
                    clearTBMeetingTime = false;
                } else {

                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            //    MeetingTime.setText( selectedHour + ":" + selectedMinute);
                            TBMeethingTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

                    clearTBMeetingTime = true;
                    TBMeethingTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.date_clear, 0);

                }

            }
        });


        final ArrayList<String> OMParticipents = databaseHelperAttendedBy.getRank();

        OMAttendedBy.setItems(OMParticipents)

                .setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                    @Override
                    public void onItemsSelected(boolean[] selected) {

                        if (OMAttendedBy.getSelectedItem().equals("All")) {
                            String attendedby = "";

                            for (String s : OMParticipents) {
                                attendedby += s + ", ";
                            }
                            if (attendedby.endsWith(" ")) {
                                attendedby = attendedby.substring(0, attendedby.length() - 2);
                                OMAttendedParticipents.setText(attendedby);
                            } else {
                                OMAttendedParticipents.setText(attendedby);
                            }
                        } else {
                            OMAttendedParticipents.setText(OMAttendedBy.getSelectedItem().toString());
                        }

                    }
                })
                .setAllCheckedText("All")
                .setAllUncheckedText("None")
                .setSelectAll(false);


        final ArrayList<String> CMParticipents = databaseHelperAttendedBy.getRank();

        CMAttendedBy.setItems(CMParticipents)

                .setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                    @Override
                    public void onItemsSelected(boolean[] selected) {

                        if (CMAttendedBy.getSelectedItem().equals("All")) {
                            String attendedby = "";

                            for (String s : CMParticipents) {
                                attendedby += s + ", ";
                            }
                            if (attendedby.endsWith(" ")) {
                                attendedby = attendedby.substring(0, attendedby.length() - 2);
                                CMAttendedParticipents.setText(attendedby);
                            } else {
                                CMAttendedParticipents.setText(attendedby);
                            }
                        } else {
                            CMAttendedParticipents.setText(CMAttendedBy.getSelectedItem().toString());
                        }

                    }
                })
                .setAllCheckedText("All")
                .setAllUncheckedText("None")
                .setSelectAll(false);


    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        MeetingDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabel1() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        TBMeethingDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void initView() {
        SelectPortDetails = (EditText) findViewById(R.id.SelectPortDetails);
        MeetingDate = (EditText) findViewById(R.id.Meeting_Date);
        MeetingTime = (EditText) findViewById(R.id.Meeting_Time);
        TBMeethingDate = (EditText) findViewById(R.id.TB_Meething_Date);
        TBMeethingTime = (EditText) findViewById(R.id.TB_Meething_Time);
        OMHeldAT = (EditText) findViewById(R.id.OM_Held_AT);
        OMAttendedBy = (MultiSelectSpinner) findViewById(R.id.OM_Attended_By);
        OMAttendedParticipents = (TextView) findViewById(R.id.OM_Attended_Participents);
        CMHeldAt = (EditText) findViewById(R.id.CM_Held_At);
        CMAttendedBy = (MultiSelectSpinner) findViewById(R.id.CM_Attended_By);
        CMAttendedParticipents = (TextView) findViewById(R.id.CM_Attended_Participents);
        btnContinueToNext = (Button) findViewById(R.id.btnContinueToNext);
    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
