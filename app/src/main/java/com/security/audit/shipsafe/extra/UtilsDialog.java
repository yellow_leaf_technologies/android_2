package com.security.audit.shipsafe.extra;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.TextView;

import com.security.audit.shipsafe.R;

public class UtilsDialog {


    public static Dialog processDialog(Context mContext, String Text, boolean cancelable, Typeface face) {
        Dialog dialog = new Dialog(mContext);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        TextView tv = (TextView) dialog.findViewById(R.id.dialogText);
        if (face != null) {
            tv.setTypeface(face);
        }
        tv.setText(Text);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }


}
