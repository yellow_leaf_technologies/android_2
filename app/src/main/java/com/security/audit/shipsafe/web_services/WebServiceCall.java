package com.security.audit.shipsafe.web_services;

public class WebServiceCall {
    private static final String ROOT_URL = "https://shipsafe.topcubit.com/";


    //private static final String ROOT_URL = "http://192.168.1.162/";

    public static final String URL_APP_PASSWORD = ROOT_URL + "getAppPassword";
    public static final String URL_USER_LOGIN = ROOT_URL + "userLogin";
    public static final String URL_CHECKLIST_TYPE = ROOT_URL + "getChecklistTypes";
    public static final String URL_VESSEL_LIST = ROOT_URL + "getVesselList";
    public static final String URL_PORT_LIST = ROOT_URL + "getPortList";
    public static final String URL_NATIONALITY = ROOT_URL + "getNationalities";
    public static final String URL_DESIGNATION = ROOT_URL + "getDesignationList";
    public static final String URL_CHECKLIST_CATEGORY = ROOT_URL + "getChecklistCategories";
    public static final String URL_CHECKLIST_QUESTIONS = ROOT_URL + "getChecklistQuestions";
    public static final String URL_SHIPSIDE = ROOT_URL + "getShipsides";
    public static final String URL_INSPECTION_TYPE = ROOT_URL + "getInsTypes";
    public static final String URL_OBSERVATION_TYPE = ROOT_URL + "getObsTypes";
    public static final String URL_SAVE_VESSEL_INFO = ROOT_URL + "saveVesselInfo";
    public static final String URL_SAVE_STAFF_INFO = ROOT_URL + "saveStaffInfo";
    public static final String URL_OBSERVATION_DEPARTMENT = ROOT_URL + "getObservationDepartment";
    public static final String URL_SAVE_OBSERVATION = ROOT_URL + "saveObservation";
    public static final String URL_SAVE_OBSERVATION_INFO = ROOT_URL + "saveObservationInfo";
    public static final String URL_SAVE_PHOTO_REPORT = ROOT_URL + "savePhotoReport";

    public static final String URL_SAVE_QUESTIONS = ROOT_URL + "saveVesselChecklist";


}






