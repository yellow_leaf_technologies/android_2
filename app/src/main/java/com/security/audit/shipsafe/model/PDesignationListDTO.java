package com.security.audit.shipsafe.model;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class PDesignationListDTO implements Serializable {
    int status;
    String message;
    DList data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DList getData() {
        return data;
    }

    public void setData(DList data) {
        this.data = data;
    }


    public class DList {
        List<DesignationList> designationList;

        public List<DesignationList> getDesignationList() {
            return designationList;
        }

        public void setDesignationList(List<DesignationList> designationList) {
            this.designationList = designationList;
        }

        public class DesignationList {
            int id;
            String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }
}
