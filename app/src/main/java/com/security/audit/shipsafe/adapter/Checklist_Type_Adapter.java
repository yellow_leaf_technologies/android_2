package com.security.audit.shipsafe.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.activity.Checklist_All_Category;
import com.security.audit.shipsafe.activity.VesselInfo;
import com.security.audit.shipsafe.model.ChecklistTypeDTO;
import com.security.audit.shipsafe.model.ChecklistTypeDetails;

import java.util.List;

public class Checklist_Type_Adapter extends RecyclerView.Adapter<Checklist_Type_Adapter.ItemViewHolder> {
    android.support.v7.app.AlertDialog b = null;
    private Context mContext;
    private final List<ChecklistTypeDetails> dto;
    int lastPosition = -1;

    public Checklist_Type_Adapter(Context c, List<ChecklistTypeDetails> dto) {
        mContext = c;
        this.dto = dto;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView  typeName;

        public ItemViewHolder(View itemView) {
            super(itemView);
            typeName = (TextView) itemView.findViewById(R.id.checklist_type_name);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent i=new Intent(mContext, VesselInfo.class);
            i.putExtra("TypeObject", (Parcelable) dto.get(getAdapterPosition()));
            mContext.startActivity(i);
        }
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {

        lastPosition = position;
        String typename=dto.get(position).getName();
        itemViewHolder.typeName.setText(typename);


    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checklist_type_item_view, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public int getItemCount() {
        return dto.size();
    }

}
