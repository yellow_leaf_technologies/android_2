package com.security.audit.shipsafe.api.errors;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.security.audit.shipsafe.R;

public class WebError extends RuntimeException {
    public WebError(Throwable cause) {
        super(cause);
    }

    public WebError() {}

    @StringRes
    @Nullable
    public Integer getMessageTextResID() {
        return R.string.error_no_internet;
    }

    @Nullable
    public String getMessageText() {
        return null;
    }
}

