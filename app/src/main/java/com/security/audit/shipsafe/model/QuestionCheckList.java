package com.security.audit.shipsafe.model;

public class QuestionCheckList {
    private String id;
    private String questionId;
    private String checklistUUID;
    private String remark;
    private String select;
    private String date;

    public QuestionCheckList() {
    }

    public QuestionCheckList(String id, String questionId,String checklistUUID, String remark, String select, String date) {
        this.id = id;
        this.questionId = questionId;
        this.checklistUUID = checklistUUID;
        this.remark = remark;
        this.select = select;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getChecklistUUID() {
        return checklistUUID;
    }

    public void setChecklistUUID(String checklistUUID) {
        this.checklistUUID = checklistUUID;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
