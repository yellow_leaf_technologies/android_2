package com.security.audit.shipsafe.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.Checklist_Type_Adapter;
import com.security.audit.shipsafe.model.ChecklistTypeDetails;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.util.List;

public class checklist_type extends AppCompatActivity {

    Context mContext;
    RecyclerView customRecycler;
    List<ChecklistTypeDetails> Dto;
    DatabaseHelperAdmin databaseHelperAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_type);
        getSupportActionBar().setTitle("Checklist Type");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        customRecycler = (RecyclerView) findViewById(R.id.categorytyperecycler);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        customRecycler.setLayoutManager(mLayoutManager);   //For Tablet

        Dto = databaseHelperAdmin.getAllType();

        Checklist_Type_Adapter adapter = new Checklist_Type_Adapter(mContext, Dto);
        customRecycler.setAdapter(adapter);

    }

    public void MakeToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
