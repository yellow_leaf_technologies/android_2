package com.security.audit.shipsafe.adapter.rank;

import android.view.View;
import android.widget.TextView;

import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.core.BaseViewHolder;
import com.security.audit.shipsafe.adapter.core.OnItemClickListener;
import com.security.audit.shipsafe.model.SaveStaffinfo;

public class RankViewHolder extends BaseViewHolder<SaveStaffinfo> {
    private TextView tvRank;
    private TextView tvName;

    public RankViewHolder(View view, OnItemClickListener<SaveStaffinfo> onItemClickListener) {
        super(view, onItemClickListener);
    }

    @Override
    protected void initUI() {
        tvName = findViewById(R.id.tvName);
        tvRank = findViewById(R.id.tvRank);

    }

    @Override
    protected void bind(SaveStaffinfo item, int position) {
        tvName.setText(item.getName());
        tvRank.setText(item.getRank());
    }
}
