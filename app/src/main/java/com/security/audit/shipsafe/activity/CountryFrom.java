package com.security.audit.shipsafe.activity;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.security.audit.shipsafe.R;
import com.security.audit.shipsafe.adapter.AllCountriesFromAdapter;
import com.security.audit.shipsafe.adapter.DividerItemDecoration;
import com.security.audit.shipsafe.model.ConPortName;
import com.security.audit.shipsafe.model.PortListDeatils;
import com.security.audit.shipsafe.sqllite.DatabaseHelperAdmin;

import java.util.ArrayList;
import java.util.List;

public class CountryFrom extends AppCompatActivity implements AllCountriesFromAdapter.CallbackInterface {
    Context mContext;
    RecyclerView customerRecycler;
    List<PortListDeatils> Dto = null;
    DatabaseHelperAdmin databaseHelperAdmin;
    private SearchView searchView;
    AllCountriesFromAdapter adapter;

    private static final int MY_REQUEST = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        getSupportActionBar().setTitle("Select Port");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        databaseHelperAdmin = new DatabaseHelperAdmin(mContext);
        customerRecycler = (RecyclerView) findViewById(R.id.CountryRecycler);
        Dto = databaseHelperAdmin.getAllCountryWithPort();
        whiteNotificationBar(customerRecycler);

        adapter = new AllCountriesFromAdapter(mContext, Dto);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        customerRecycler.setLayoutManager(layoutManager);
        customerRecycler.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));
        customerRecycler.setItemAnimator(new DefaultItemAnimator());
        customerRecycler.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview_in_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
            getWindow().setNavigationBarColor(Color.WHITE);
        }
    }

    @Override
    public void onHandleSelection(int position, PortListDeatils name) {
        String Text = name.getFull_name();
        VesselInfo.PortFrom.setText(Text);
        finish();
    }
}

