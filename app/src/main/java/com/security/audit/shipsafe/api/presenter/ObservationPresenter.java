package com.security.audit.shipsafe.api.presenter;

import android.content.Context;
import android.net.Uri;

import com.security.audit.shipsafe.api.core.ApiUtils;
import com.security.audit.shipsafe.api.errors.WebError;
import com.security.audit.shipsafe.api.presenter.core.BasePresenter;
import com.security.audit.shipsafe.api.view.ViewPresenter;
import com.security.audit.shipsafe.model.SaveObservationDTO;
import com.security.audit.shipsafe.model.ShipSideCheckList;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObservationPresenter extends BasePresenter {

    public ObservationPresenter(Context context, ViewPresenter view) {
        super(context, view);
    }

    public void sendObservation(SaveObservationDTO item) {
        ApiUtils.getDataService(getContext())
                .sendObservation("vesselId",
                        item.getId(),
                        "portPlace",
                        item.getTargetDate(),
                        item.getClosedDate(),
                        "auditorTime",
                        "openingHeldAt",
                        "openingAttendedBy",
                        "closingHeldAt",
                        "closingAttendedBy",
                        item.getChecklistUUID()
                )
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        view.onAccountError(new WebError());
                    }
                });
    }
}
